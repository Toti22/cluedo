TEMPLATE = subdirs

SUBDIRS += \
    src

QMAKE_EXTRA_TARGETS += deploy

deploy.target = deploy
deploy.CONFIG = recursive
deploy.commands += $${QMAKE_COPY} \"$$(CLUEDODIR)/bin\" $$escape_expand(\\n\\t)

CONFIG(testcase){
    SUBDIRS += test
    test.depends = src
	
	QMAKE_EXTRA_TARGETS += prepare_check checks
	
	QTESTDIR=$$(CLUEDODIR)\test\results\qtestlib
                prepare_check.target=prepare_check
		prepare_check.commands += $(CHK_DIR_EXISTS) $$QTESTDIR $(MKDIR) $$QTESTDIR $$escape_expand(\\n\\t)
		prepare_check.commands += $(DEL_FILE) $$QTESTDIR\** $$escape_expand(\\n\\t)
	
	checks.target = checks
	checks.CONFIG = recursive
}

#include "ui/mainmenu.h"

#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QDebug>
#include <algorithm>

#include "ui/gameUI.h"
#include "ui/gameUiLogic.h"
#include "core/pathFinder.h"
#include "core/board/board.h"
#include "core/gameLogic/gamelogic.h"

#define MAP_X_SIZE 24
#define MAP_Y_SIZE 25


int main(int argc, char *argv[])
{
    int appResult = 0;
    QApplication a(argc, argv);

    QString serverModString("SERVER");
    QStringList arguments = a.arguments();
    auto iter = std::find(arguments.begin(), arguments.end(), serverModString);
    bool serveurMod = iter != arguments.end();

    if (serveurMod == true)
    {
        GameLogic gameLogic(&a, true);
        appResult = a.exec();
    }
    else
    {
        QTranslator translator;
        const QStringList uiLanguages = QLocale::system().uiLanguages();
        for (const QString &locale : uiLanguages) {
            const QString baseName = "Cluedo_" + QLocale(locale).name();
            if (translator.load(":/i18n/" + baseName)) {
                a.installTranslator(&translator);
                break;
            }
        }

        GameUi gameUi;

        GameLogic gameLogic(&a);
        GameUiLogic gameUiLogic(&gameLogic, &gameUi);
        gameUi.showMainMenu();

        appResult = a.exec();

        PathFinder::freePathFinderInstances();
        Board::freeBoardInstance();
    }

    return appResult;
}

/********************************************************************************
** Form generated from reading UI file 'mainmenu.ui'
**
** Created by: Qt User Interface Compiler version 6.5.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINMENU_H
#define UI_MAINMENU_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainMenu
{
public:
    QWidget *centralwidget;
    QGraphicsView *graphicsView;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox;
    QCheckBox *checkBox_4;
    QCheckBox *checkBox_3;
    QFrame *line;
    QSpacerItem *verticalSpacer;
    QLabel *label;
    QCheckBox *checkBox_8;
    QCheckBox *checkBox_7;
    QCheckBox *checkBox_6;
    QCheckBox *checkBox_5;
    QFrame *line_2;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_3;
    QCheckBox *checkBox_9;
    QCheckBox *checkBox_10;
    QCheckBox *checkBox_11;
    QCheckBox *checkBox_12;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainMenu)
    {
        if (MainMenu->objectName().isEmpty())
            MainMenu->setObjectName("MainMenu");
        MainMenu->resize(800, 600);
        centralwidget = new QWidget(MainMenu);
        centralwidget->setObjectName("centralwidget");
        graphicsView = new QGraphicsView(centralwidget);
        graphicsView->setObjectName("graphicsView");
        graphicsView->setGeometry(QRect(170, 210, 256, 192));
        verticalLayoutWidget = new QWidget(centralwidget);
        verticalLayoutWidget->setObjectName("verticalLayoutWidget");
        verticalLayoutWidget->setGeometry(QRect(460, 60, 271, 471));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName("verticalLayout");
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(verticalLayoutWidget);
        label_2->setObjectName("label_2");
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_2);

        checkBox_2 = new QCheckBox(verticalLayoutWidget);
        checkBox_2->setObjectName("checkBox_2");

        verticalLayout->addWidget(checkBox_2);

        checkBox = new QCheckBox(verticalLayoutWidget);
        checkBox->setObjectName("checkBox");

        verticalLayout->addWidget(checkBox);

        checkBox_4 = new QCheckBox(verticalLayoutWidget);
        checkBox_4->setObjectName("checkBox_4");

        verticalLayout->addWidget(checkBox_4);

        checkBox_3 = new QCheckBox(verticalLayoutWidget);
        checkBox_3->setObjectName("checkBox_3");

        verticalLayout->addWidget(checkBox_3);

        line = new QFrame(verticalLayoutWidget);
        line->setObjectName("line");
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        label = new QLabel(verticalLayoutWidget);
        label->setObjectName("label");
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        checkBox_8 = new QCheckBox(verticalLayoutWidget);
        checkBox_8->setObjectName("checkBox_8");

        verticalLayout->addWidget(checkBox_8);

        checkBox_7 = new QCheckBox(verticalLayoutWidget);
        checkBox_7->setObjectName("checkBox_7");

        verticalLayout->addWidget(checkBox_7);

        checkBox_6 = new QCheckBox(verticalLayoutWidget);
        checkBox_6->setObjectName("checkBox_6");

        verticalLayout->addWidget(checkBox_6);

        checkBox_5 = new QCheckBox(verticalLayoutWidget);
        checkBox_5->setObjectName("checkBox_5");

        verticalLayout->addWidget(checkBox_5);

        line_2 = new QFrame(verticalLayoutWidget);
        line_2->setObjectName("line_2");
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName("label_3");
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_3);

        checkBox_9 = new QCheckBox(verticalLayoutWidget);
        checkBox_9->setObjectName("checkBox_9");

        verticalLayout->addWidget(checkBox_9);

        checkBox_10 = new QCheckBox(verticalLayoutWidget);
        checkBox_10->setObjectName("checkBox_10");

        verticalLayout->addWidget(checkBox_10);

        checkBox_11 = new QCheckBox(verticalLayoutWidget);
        checkBox_11->setObjectName("checkBox_11");

        verticalLayout->addWidget(checkBox_11);

        checkBox_12 = new QCheckBox(verticalLayoutWidget);
        checkBox_12->setObjectName("checkBox_12");

        verticalLayout->addWidget(checkBox_12);

        MainMenu->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainMenu);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 800, 21));
        MainMenu->setMenuBar(menubar);
        statusbar = new QStatusBar(MainMenu);
        statusbar->setObjectName("statusbar");
        MainMenu->setStatusBar(statusbar);

        retranslateUi(MainMenu);

        QMetaObject::connectSlotsByName(MainMenu);
    } // setupUi

    void retranslateUi(QMainWindow *MainMenu)
    {
        MainMenu->setWindowTitle(QCoreApplication::translate("MainMenu", "MainMenu", nullptr));
        label_2->setText(QCoreApplication::translate("MainMenu", "Characters", nullptr));
        checkBox_2->setText(QCoreApplication::translate("MainMenu", "CheckBox", nullptr));
        checkBox->setText(QCoreApplication::translate("MainMenu", "CheckBox", nullptr));
        checkBox_4->setText(QCoreApplication::translate("MainMenu", "CheckBox", nullptr));
        checkBox_3->setText(QCoreApplication::translate("MainMenu", "CheckBox", nullptr));
        label->setText(QCoreApplication::translate("MainMenu", "Weapons", nullptr));
        checkBox_8->setText(QCoreApplication::translate("MainMenu", "CheckBox", nullptr));
        checkBox_7->setText(QCoreApplication::translate("MainMenu", "CheckBox", nullptr));
        checkBox_6->setText(QCoreApplication::translate("MainMenu", "CheckBox", nullptr));
        checkBox_5->setText(QCoreApplication::translate("MainMenu", "CheckBox", nullptr));
        label_3->setText(QCoreApplication::translate("MainMenu", "Rooms", nullptr));
        checkBox_9->setText(QCoreApplication::translate("MainMenu", "CheckBox", nullptr));
        checkBox_10->setText(QCoreApplication::translate("MainMenu", "CheckBox", nullptr));
        checkBox_11->setText(QCoreApplication::translate("MainMenu", "CheckBox", nullptr));
        checkBox_12->setText(QCoreApplication::translate("MainMenu", "CheckBox", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainMenu: public Ui_MainMenu {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINMENU_H

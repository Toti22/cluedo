#ifndef GAMEUISCENESUGGESTION_H
#define GAMEUISCENESUGGESTION_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QVector>
#include <QObject>
#include <QGraphicsProxyWidget>

#include "core/gameLogic/card.h"
#include "core/gameLogic/suggestion.h"
#include "core/gameLogic/gameElement/gameElementSuspect.h"
#include "selectionableCard.h"

class GameUiSceneSuggestion : public QObject
{
    Q_OBJECT
public:
    explicit GameUiSceneSuggestion(QGraphicsScene *scene);
    void requestShowSuggestionMenu(Card *roomCard);

    void clearScene();
protected:
    QVector<QGraphicsItem *> m_suggestionMenuItems;
    QGraphicsScene *m_scene;

    QVector<SelectionableCard *> m_selectableSuspectCards;
    QVector<SelectionableCard *> m_selectableWeaponsCards;

    Card *m_roomCardSelected;
    SelectionableCard *m_suspectCardSelected;
    SelectionableCard *m_weaponCardSelected;
    QGraphicsProxyWidget *m_validateSuggestionButton;

protected slots:
    void cardSelected(SelectionableCard *cardUi);
    void cardUnSelected(SelectionableCard *cardUi);

    void validateSuggestionSlot();

signals:
    void validateSuggestion(Suggestion);
};

#endif // GAMEUISCENESUGGESTION_H

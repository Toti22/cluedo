#ifndef GAMEUI_H
#define GAMEUI_H

#include <QWidget>
#include <QVector>
#include <QLabel>
#include <QWidgetItem>

#include "boardui.h"
#include "clueSheet.h"
#include "core/players/player.h"
#include "core/coordinate.h"
#include "playerCardsUi.h"
#include "portraitUi.h"
#include "selectionableCard.h"
#include "core/gameLogic/suggestion.h"
#include "gameUiSceneClueToShow.h"
#include "gameUiSceneResultSuggestion.h"
#include "gameUiSceneSuggestion.h"
#include "gameUiSceneAccusation.h"
#include "gameUiSceneWaitingForPlayerToAnswerSuggestion.h"
#include "mainMenu.h"
#include "gameLobbyUi.h"
#include "joinServerUi.h"

class GameUi : public QWidget
{
    Q_OBJECT

    enum class PlayerAction { NONE, MOVING, SUGGESTION, ACCUSATION};

public:
    explicit GameUi(QWidget *parent = nullptr);

    void enablePlayerMovementMod(int idCurrentPlayerPlaying, const Coordinate &playerLocation, const QVector<Coordinate> &coordinates);
    BoardUi *boardUi();

    void emitMoveToResult(const Coordinate &coordinate);

    void connectionSuccess(int suspectId);
    void connectionFailed();

    void aClientHasJoinServer(int suspectId);
    void aClientHasLeftServer(int suspectId);
    void clientList(QVector<int> suspectsIds);

    void connectionToServerLost();
    void gameEnded();

signals:
    void playerMovedTo(Coordinate);
    void playerMovedToResult(Coordinate);
    void playerSuggestion(Suggestion);
    void playerAccusation(Suggestion);
    void requestEndTurn();

private:
    PlayerAction m_currentAction;
    BoardUi m_boardUi;
    ClueSheet m_clueSheet;
    PlayerCardsUi m_playerCardsUi;
    MainMenu m_mainMenu;
    GameUiSceneSuggestion m_gameUiSceneSuggestion;
    GameUiSceneClueToShow m_gameUiSceneClueToShow;
    GameUiSceneResultSuggestion m_gameUiSceneResultSuggestion;
    GameUiSceneAccusation m_gameUiSceneAccusation;
    GameUiSceneWaitingForPLayerToAnswerSuggestion m_gameUiSceneWaitingForPLayerToAnswerSuggestion;
    GameLobbyUi m_gameLobbyUi;
    JoinServerUi m_joinServerUi;

    bool m_hostMod;


    // QWidget interface

public slots:
    void showMainMenu();
    void gameStarted();
    void playerStartTurn(int playerId);
    void playerEndTurn(int playerId);
    void playerMovedToSlot(const Coordinate &coordinate);
    void playerHasToShowClue(GameElementSuspect *suspectWhoMadeSuggestion, GameElementSuspect *suspectAnswering, const Suggestion &suggestion, QVector<Card *> cluesInPLayerPossession);
    void waitingForPlayerToShowClue(GameElementSuspect *suspectWhoMadeSuggestion, GameElementSuspect *suspectAnswering, const Suggestion &suggestion);
    void playerShowClue(GameElementSuspect *suspectWhoAnswered, GameElementSuspect *suspectWhoSuggest, const Suggestion &suggestion, Card * clue);
    void setPlayerPosition(const Coordinate &coord, int suspectId);
    void endSuggestion(GameElementSuspect *suspectWhoAnswered);
    void initPlayerUi(int playerId);

protected slots:
    void requestThrowDice();
    void requestTakeSecretPassage();
    void requestShowSuggestionMenu();
    void requestShowAccusationMenu();

    void clueToShowSelectedSlot(int clue);

    void validateSuggestion(const Suggestion &suggestion);
    void validateAccusation(const Suggestion &suggestion);
    void cancelAccusation();

    void createLocalGameSlot();
    void createRemoteGameSlot();
    void joinRemoteGameSlot(QString ip, int port);

    void cancelLobbySlot();
    void startGameSlot();

    void openJoinServerUi();
    void cancelJoinServer();
signals:
    void requestThrowDiceSignal();
    void requestTakeSecretPassageSignal();
    void requestShowSuggestionMenuSignal();
    void requestShowAccusationMenuSignal();
    void clueToShowSelected(int);

    void createLocalGame();
    void createRemoteGame();
    void joinRemoteGame(QString, int);

    void cancelLobby();
    void startGame();

    void playerStartTurnSignal(int playerId);
private:
    void clearInterfaces();

    int m_lastArrowKeyPressed;
    QGridLayout m_playerActionLayout;
    QVector<QPushButton *> m_playerActionButtons;
    PortraitUi m_playerPortrait;
    QGridLayout *m_layout;
    int m_currentPlayerId;
};

inline BoardUi *GameUi::boardUi()
{
    return &m_boardUi;
}

#endif // GAMEUI_H

#include "joinServerUi.h"

#include <QPainter>
#include <QLabel>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QBrush>
#include <QRect>
#include <QGraphicsProxyWidget>
#include <QPalette>
#include <QRandomGenerator>
#include <QHBoxLayout>

#include "core/board/BoardCaracteristics.h"

JoinServerUi::JoinServerUi(QGraphicsScene *scene)
    : m_scene(scene)
{
}

void JoinServerUi::clearScene()
{
    for (QGraphicsItem *item : m_items)
    {
        m_scene->removeItem(item);
    }
    m_items.clear();
}

void JoinServerUi::showMenu()
{
    createBackGroundItem();
    QGraphicsItem *item = m_scene->addWidget(createMenu());
    m_items.push_back(item);
    item->setZValue(1);
}

void JoinServerUi::joinServerSlot()
{
    clearScene();
    emit joinServer(m_lineEditIp->text(), m_spinBoxPort->value());
}

void JoinServerUi::cancelSlot()
{
    clearScene();
    emit cancel();
}

void JoinServerUi::createBackGroundItem()
{
    int xSize = BOARD_X_SIZE * TILE_SIZE;
    int ySize = BOARD_Y_SIZE * TILE_SIZE;

    QRect rect(0, 0, xSize, ySize);

    QBrush blackBrush(Qt::GlobalColor::black);

    QPixmap pixmap(":/onePixel.png");
    pixmap = pixmap.scaled(rect.width(), rect.height(), Qt::IgnoreAspectRatio);
    QPainter painter(&pixmap);
    painter.fillRect(0, 0, xSize, ySize, blackBrush);

    QGraphicsPixmapItem *itemBackground = m_scene->addPixmap(pixmap);
    m_items.push_back(itemBackground);
    itemBackground->setPos(0, 0);
    itemBackground->setOpacity(1);
    itemBackground->setZValue(0);
}

QWidget * JoinServerUi::createMenu()
{
    int xSize = BOARD_X_SIZE * TILE_SIZE;
    int ySize = BOARD_Y_SIZE * TILE_SIZE;
    QWidget *widget = new QWidget;

    widget->setFixedSize(xSize, ySize);
    widget->setAutoFillBackground(true);
    widget->setStyleSheet("font-size: 48px;background-color: rgba(255, 255, 255, 0);");

    QLabel *labelIp = new QLabel;
    labelIp->setAutoFillBackground(true);
    labelIp->setText(tr("Ip : "));
    labelIp->setStyleSheet("font-size: 48px;background-color: rgba(255, 255, 255, 255);");
    m_lineEditIp = new QLineEdit;
    m_lineEditIp->setText("127.0.0.1");
    m_lineEditIp->setAutoFillBackground(true);
    m_lineEditIp->setStyleSheet("font-size: 48px;background-color: rgba(255, 255, 255, 255);");

    QLabel *labelPort = new QLabel;
    labelPort->setAutoFillBackground(true);
    labelPort->setText(tr("Port : "));
    labelPort->setStyleSheet("font-size: 48px;background-color: rgba(255, 255, 255, 255);");
    m_spinBoxPort = new QSpinBox;
    m_spinBoxPort->setMaximum(65535);
    m_spinBoxPort->setValue(2332);
    m_spinBoxPort->setAutoFillBackground(true);
    m_spinBoxPort->setStyleSheet("font-size: 48px;background-color: rgba(255, 255, 255, 255);");

    QWidget *widgetButtons = new QWidget();
    QHBoxLayout *buttonsLayout = new QHBoxLayout();
    widgetButtons->setLayout(buttonsLayout);
    QPushButton *joinButton = new QPushButton();
    joinButton->setAutoFillBackground(true);
    joinButton->setStyleSheet("font-size: 36px;background-color: rgba(255, 255, 255, 255);");
    joinButton->setText("Join");
    QPushButton *cancelButton = new QPushButton();
    cancelButton->setAutoFillBackground(true);
    cancelButton->setStyleSheet("font-size: 36px;background-color: rgba(255, 255, 255, 255);");
    cancelButton->setText("Cancel");
    buttonsLayout->addWidget(joinButton);
    buttonsLayout->addWidget(cancelButton);

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(labelIp, 1, 1);
    layout->addWidget(m_lineEditIp, 1, 2);
    layout->addWidget(labelPort, 2, 1);
    layout->addWidget(m_spinBoxPort, 2, 2);
    layout->addWidget(widgetButtons, 3, 1, 1, 2, Qt::AlignBottom);

    QWidget *upperSpacer = new QWidget;
    QWidget *downSpacer = new QWidget;
    QWidget *leftSpacer = new QWidget;
    QWidget *rightSpacer = new QWidget;
    upperSpacer->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    downSpacer->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
    leftSpacer->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    rightSpacer->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    layout->addWidget(upperSpacer, 0, 0, 1, 4);
    layout->addWidget(downSpacer, 4, 0, 1, 4);
    layout->addWidget(leftSpacer, 1, 0, 3, 1);
    layout->addWidget(rightSpacer, 1, 3, 3, 1);

    widget->setLayout(layout);

    connect(joinButton, SIGNAL(pressed()), this, SLOT(joinServerSlot()));
    connect(cancelButton, SIGNAL(pressed()), this, SLOT(cancelSlot()));

    return widget;
}

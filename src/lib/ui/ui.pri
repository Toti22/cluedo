include($$(CLUEDODIR)/src/lib/core/core.pri)

LIBS += -L$$(CLUEDODIR)/lib
CONFIG(release, debug|release):LIBS += -lui
CONFIG(debug, debug|release):LIBS += -luid

INCLUDEPATH += \
    $$(CLUEDODIR)/src/lib/

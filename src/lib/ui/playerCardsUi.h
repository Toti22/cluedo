#ifndef PLAYERCARDSUI_H
#define PLAYERCARDSUI_H

#include <QGridLayout>
#include <QWidget>
#include <QSize>
#include "core/gameLogic/card.h"

class PlayerCardsUi : public QWidget
{
    Q_OBJECT
public:
    explicit PlayerCardsUi(const QSize &size, QWidget *parent = nullptr);

    void loadCards(QVector<Card *> cards);

private:
    QSize m_size;
    QGridLayout *m_layout;

    QVector<QWidget *> m_widgets;

signals:

};

#endif // PLAYERCARDSUI_H

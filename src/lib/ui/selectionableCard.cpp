#include "selectionableCard.h"

#include <QEnterEvent>
#include <QEvent>
#include <QPainter>

SelectionableCard::SelectionableCard(int cardId, const QPixmap &pixmap) :
    m_cardId(cardId), m_defaultPixmap(pixmap), m_hoverPixmap(pixmap), m_selectedPixmap(pixmap), m_isSelected(false)
{
    setPixmap(m_defaultPixmap);
    QBrush yellowBrush(Qt::GlobalColor::yellow);
    QRect rect(0, 0, pixmap.size().width(), pixmap.size().height());
    QPainter painterHover(&m_hoverPixmap);
    painterHover.setOpacity(0.3);
    painterHover.fillRect(rect, yellowBrush);

    int thicknessWall = 8;

    QBrush blackBrush(Qt::GlobalColor::blue);

    QPainter painter(&m_selectedPixmap);
    painter.fillRect(0, 0, thicknessWall, rect.height(), blackBrush);
    painter.fillRect(0, 0, rect.width(), thicknessWall, blackBrush);
    painter.fillRect(rect.width() - thicknessWall, 0, rect.width(), rect.height(), blackBrush);
    painter.fillRect(0,rect.height() - thicknessWall, rect.width(), rect.height(), blackBrush);

}

void SelectionableCard::setSelectected(bool selected)
{
    m_isSelected = selected;
    if (m_isSelected == true)
    {
        setPixmap(m_selectedPixmap);
    }
    else
    {
        if (m_mouseIsOver == true)
        {
            setPixmap(m_hoverPixmap);
        }
        else
        {
            setPixmap(m_defaultPixmap);
        }
    }
}



void SelectionableCard::mousePressEvent(QMouseEvent *)
{
    if (m_isSelected == true)
    {
        emit unSelected(this);
    }
    else
    {
        emit selected(this);
    }
    setSelectected(!m_isSelected);
}

void SelectionableCard::enterEvent(QEnterEvent *event)
{
    m_mouseIsOver = true;
    event->accept();

    if (m_isSelected == false)
    {
        setPixmap(m_hoverPixmap);
    }
}

void SelectionableCard::leaveEvent(QEvent *event)
{
    m_mouseIsOver = false;
    event->accept();

    if (m_isSelected == false)
    {
        setPixmap(m_defaultPixmap);
    }
}

#ifndef PLAYERDATAFORUI_H
#define PLAYERDATAFORUI_H

#include <QVector>
#include <QMap>

#include "core/coordinate.h"
#include "core/players/clueSheetData.h"
#include "core/gameLogic/card.h"
#include "core/players/player.h"

class PlayerDataForUi
{
public:
    static QMap<int, PlayerDataForUi*> s_playersData;

    PlayerDataForUi(GameElementSuspect *suspect);
    PlayerDataForUi(Player *player);

    bool m_localHumanPlayer;

    int m_characterId;
    QVector<Card *> m_cards;
    Coordinate m_position;
    ClueSheetData *m_clueSheetData;
};

#endif // PLAYERDATAFORUI_H

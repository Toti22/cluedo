#include "clueSheet.h"

#include <QLabel>
#include <QSpacerItem>
#include <algorithm>

#include "core/gameLogic/gameElement/gameElementFactory.h"
#include "core/gameLogic/gameElement/gameElementRoom.h"

ClueSheet::ClueSheet(QWidget *parent)
    : QWidget{parent}
{
    init();
}

void ClueSheet::init()
{
    m_layout = new QVBoxLayout;

    QVector<GameElementRoom *> rooms = GameElementFactory::getRooms();
    QVector<GameElementSuspect *> suspects = GameElementFactory::getSuspects();
    QVector<GameElementWeapon *> weapons = GameElementFactory::getWeapons();

    QLabel *label = nullptr;
    QCheckBox *checkBox = nullptr;

    // SUSPECTS

    label = new QLabel(tr("SUSPECTS"));
    label->setAlignment(Qt::AlignHCenter);

    m_layout->addWidget(label);
    m_layout->addWidget(createLineSeparator());

    for (GameElementSuspect *suspect : suspects)
    {
        checkBox = new QCheckBox(suspect->getVisibleName());
        checkBox->setAccessibleName(suspect->getName());
        connect(checkBox, SIGNAL(clicked(bool)), this, SLOT(elementChecked(bool)));
        m_layout->addWidget(checkBox);
        m_checkBox.push_back(checkBox);
    }

    m_layout->addWidget(createLineSeparator());

    // WEAPONS

    QWidget *spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    m_layout->addWidget(spacer);

    label = new QLabel(tr("WEAPONS"));
    label->setAlignment(Qt::AlignHCenter);

    m_layout->addWidget(label);
    m_layout->addWidget(createLineSeparator());

    for (GameElementWeapon *weapon : weapons)
    {
        checkBox = new QCheckBox(weapon->getVisibleName());
        checkBox->setAccessibleName(weapon->getName());
        connect(checkBox, SIGNAL(clicked(bool)), this, SLOT(elementChecked(bool)));
        m_layout->addWidget(checkBox);
        m_checkBox.push_back(checkBox);
    }

    m_layout->addWidget(createLineSeparator());

    // ROOMS

    spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    m_layout->addWidget(spacer);

    label = new QLabel(tr("ROOMS"));
    label->setAlignment(Qt::AlignHCenter);

    m_layout->addWidget(label);
    m_layout->addWidget(createLineSeparator());

    for (GameElementRoom *room : rooms)
    {
        checkBox = new QCheckBox(room->getVisibleName());
        checkBox->setAccessibleName(room->getName());
        connect(checkBox, SIGNAL(clicked(bool)), this, SLOT(elementChecked(bool)));
        m_layout->addWidget(checkBox);
        m_checkBox.push_back(checkBox);
    }
}

void ClueSheet::loadClueSheetData(ClueSheetData *clueSheetData)
{
    m_clueSheetData = clueSheetData;
    QVector<GameElement *> gameElements = GameElementFactory::getAllGameElements();

    for (GameElement *gameElement : gameElements)
    {
        if (clueSheetData->gameElementsCheck.contains(gameElement->getId()) == true)
        {
            checkClue(gameElement->getId(), clueSheetData->gameElementsCheck[gameElement->getId()]);
        }
    }
}

void ClueSheet::checkClue(int clueId, bool checked)
{
    GameElement *gameElement = GameElementFactory::getGameElementById(clueId);
    if (gameElement == nullptr)
    {
        return;
    }
    auto iter = std::find_if(m_checkBox.begin(), m_checkBox.end(), [gameElement](QCheckBox *checkbox)
        {
            return checkbox->accessibleName() == gameElement->getName();
        });

    if(iter == m_checkBox.end())
    {
        return;
    }
    QCheckBox *checkBox = *iter;
    checkBox->setChecked(checked);
    m_clueSheetData->gameElementsCheck[clueId] = checked;
}

QFrame * ClueSheet::createLineSeparator()
{
    QFrame *line;
    line = new QFrame();
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);

    return  line;
}

void ClueSheet::elementChecked(bool checked)
{
    QCheckBox *checkBox = dynamic_cast<QCheckBox *>(sender());
    GameElement *elementFound = nullptr;
    QVector<GameElement *> elements = GameElementFactory::getAllGameElements();
    for (GameElement *element : elements)
    {
        if (element->getName() == checkBox->accessibleName())
        {
            elementFound = element;
            break;
        }
    }
    if (elementFound != nullptr)
    {
        checkClue(elementFound->getId(), checked);
    }
}

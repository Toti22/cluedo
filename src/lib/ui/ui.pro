include($$(CLUEDODIR)/src/lib/core/core.pri)

include($$(CLUEDODIR)/src/lib/lib.pri)

QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += \
    boardui.cpp \
    clueSheet.cpp \
    gameLobbyUi.cpp \
    gameUI.cpp \
    gameUiLogic.cpp \
    gameUiSceneClueToShow.cpp \
    gameUiSceneResultSuggestion.cpp \
    gameUiSceneSuggestion.cpp \
    gameUiSceneAccusation.cpp \
    gameUiSceneWaitingForPlayerToAnswerSuggestion.cpp \
    joinServerUi.cpp \
    mainMenu.cpp \
    playerCardsUi.cpp \
    playerDataforUi.cpp \
    portraitUi.cpp \
    selectionableCard.cpp

HEADERS += \
    boardui.h \
    clueSheet.h \
    gameLobbyUi.h \
    gameUiLogic.h \
    gameUiSceneClueToShow.h \
    gameUiSceneResultSuggestion.h \
    gameUiSceneSuggestion.h \
    gameUiSceneAccusation.h \
    gameUiSceneWaitingForPlayerToAnswerSuggestion.h \
    gameUI.h \
    joinServerUi.h \
    mainMenu.h \
    playerCardsUi.h \
    playerDataForUi.h \
    portraitUi.h \
    selectionableCard.h

FORMS +=

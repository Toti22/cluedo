#ifndef GAMEUILOGIC_H
#define GAMEUILOGIC_H

#include <QObject>
#include <QMap>

#include "core/gameLogic/gamelogic.h"
#include "core/players/player.h"
#include "gameUI.h"
#include "core/gameLogic/gameElement/gameElementFactory.h"
#include "playerDataForUi.h"

class GameUiLogic : public QObject
{
    Q_OBJECT
public:
    explicit GameUiLogic(GameLogic *gameLogic, GameUi *gameUi, QObject *parent = nullptr);


public slots:
    addLocalPlayer(Player * player);

private slots:
    void localGameStarted(QVector<Player *> players);
    void gameStarted();
    void remoteGameStarted(Player *);
    void playerStartTurn(int playerId);
    void playerEndTurn(int playerId);
    void suggestionEnd(int idPlayerWhoAnswered);

    void createLocalGameSlot();
    void createRemoteGameSlot();
    void joinRemoteGameSlot(const QString &ipAdresse, int port);

    void connectionSuccess(int suspectId);
    void connectionFailed();
    void connectionServerLost();

    void startGameSlot();
    void cancelLobbySlot();

    void aClientHasJoinServerSlot(int suspectId);
    void aClientHasLeftServerSlot(int suspectId);
    void clientListSlot(QVector<int> suspectsIds);

    void playerThrowDiceResult(int result);
    void playerMoveToResult(const Coordinate &coord);
    void playerHasToAnswerSuggestion(int playerIdWhoMadeSuggestion, const Suggestion &suggestion, int playerIdWhoShouldAnswer, QVector<Card *> clues);
    void playerShowClue(const Suggestion &suggestion, int clue, int playerWhoAnswered);
    void playerMakeSuggestion(const Suggestion &suggestion);
    void playerMakeAccusation(bool heIsRight, const Suggestion &suggestion);
    void emitSuggestion(const Suggestion &suggestion);
    void emitAccusation(const Suggestion &suggestion);
    void emitThrowDice();
    void emitEndTurn();
    void emitMoveTo(const Coordinate &);
    void emitShowClue(int);

private:
    void disconnectToCurrentPlayerSignal();
    void connectToCurrentPlayerSignal();
    //Use for remote games
    void connectToLocalPlayerSignal(Player *player);
    //Use for local games
    void connectToGameLogic();
    void gameEnded(const Suggestion &suggestion, int playerIdWhoWon);

    int m_currentPlayerId;
    Suggestion m_currentSuggestion;
    int m_playerHasToShowClueId;
    GameLogic *m_gameLogic;
    GameUi *m_gameUi;
    QVector<Player *> m_localPlayers;
    QVector<int> m_charactersPlaying;

    Player *findLocalPlayer(int playerId);

};

#endif // GAMEUILOGIC_H

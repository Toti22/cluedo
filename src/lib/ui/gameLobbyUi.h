#ifndef GAMELOBBYUI_H
#define GAMELOBBYUI_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QMap>
#include <QVector>

class GameLobbyUi : public QWidget
{
    Q_OBJECT
public:
    explicit GameLobbyUi(QGraphicsScene *scene);
    void clearScene();
    void showMenu(bool hostMod);

    void addSuspect(int id);
    void addSuspects(QVector<int> ids);
    void removeSuspect(int id);

protected slots:
    void startGameSlot();
    void cancelSlot();

protected:
    QGridLayout *m_layout;
    QGraphicsScene *m_scene;
    QVector<QGraphicsItem *> m_items;
    QMap<int, QGraphicsItem *> m_portraitsItems;
    QVector<QLabel *> m_portraits;

    void createBackGroundItem();
    QWidget *createMenu(bool hostMod);

signals:
    void startGame();
    void cancelLobby();
};

#endif // GAMELOBBYUI_H

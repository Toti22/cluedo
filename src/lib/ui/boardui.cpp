#include "boardui.h"

#include <QGraphicsProxyWidget>
#include <QBrush>
#include <QPixmap>

#include "core/gameLogic/gameElement/gameElementSuspect.h"
#include "core/gameLogic/gameElement/gameElementFactory.h"
#include "core/board/BoardCaracteristics.h"

BoardUi::BoardUi():
    m_lastCoordUsedToDrawArrow(-1, -1)
{
    m_graphicsView = new QGraphicsView(this);
    setSceneRect(0, 0, TILE_SIZE * BOARD_X_SIZE, TILE_SIZE * BOARD_Y_SIZE);
}

void BoardUi::clearBoard()
{
    disablePlayerMovementMod();
    for(QGraphicsProxyWidget *item : m_suspectsPawns.values())
    {
        item->deleteLater();
    }
    m_suspectsPawns.clear();
}

void BoardUi::playerStartTurn(int idCurrentPlayerPlaying)
{
    m_currentPlayerPlaying = idCurrentPlayerPlaying;
}

void BoardUi::enablePlayerMovementMod(int, const Coordinate &playerLocation, const QVector<Coordinate> &coordinates)
{
    m_currentPlayerPossiblePosition.push_back(playerLocation);
    QBrush blueBrush(Qt::GlobalColor::blue);

    QRect rect(0, 0, TILE_SIZE, TILE_SIZE);
    QPixmap pixmap(":/onePixel.png");
    pixmap = pixmap.scaled(rect.width(), rect.height());
    QPainter painter(&pixmap);
    painter.fillRect(rect, blueBrush);

    for (int i = 0 ; i < coordinates.size() ; ++i)
    {
        int xPos = coordinates.at(i).x * TILE_SIZE;
        int yPos = coordinates.at(i).y * TILE_SIZE;

        if (m_highlightItems.contains(coordinates.at(i)) == false)
        {
            QLabel *label = new QLabel();
            label->setPixmap(pixmap);
            QGraphicsProxyWidget *graphicsItem = this->addWidget(label);
            graphicsItem->setOpacity(0.35);
            graphicsItem->setPos(xPos, yPos);
            m_highlightItems.insert(coordinates.at(i), graphicsItem);
        }
    }
}

void BoardUi::disablePlayerMovementMod()
{
    for (auto iter = m_highlightItems.begin() ; iter != m_highlightItems.end() ; ++iter)
    {
        iter.value()->deleteLater();
    }
    for (QGraphicsProxyWidget *item : m_arrowItems)
    {
        item->deleteLater();
    }
    m_arrowItems.clear();
    m_highlightItems.clear();
    m_currentPlayerPossiblePosition.clear();
    m_lastCoordUsedToDrawArrow = Coordinate(-1, -1);
}

void BoardUi::playerMoveToResult(const Coordinate &coord)
{
    setPlayerPosition(coord, m_currentPlayerPlaying);
}

void BoardUi::init()
{
    m_board = Board::getBoardInstance();
    QVector<QVector<std::shared_ptr<Tile> > > boardData = m_board->getBoard();

    for (int x = 0 ; x < boardData.size() ; ++x)
    {
        for (int y = 0 ; y < boardData.at(x).size() ; ++y)
        {
            QLabel *label = new QLabel;
            label->setPixmap(boardData.at(x).at(y)->getPixamp());

            QGraphicsProxyWidget *graphicsItem = this->addWidget(label);
            graphicsItem->setPos(x * TILE_SIZE, y * TILE_SIZE);
        }
    }

    for (Room *room : m_board->getRooms())
    {
        QLabel *label = new QLabel();
        label->setText(room->getRoomText());
        Coordinate roomCoordinate = room->getLabelCoordinate();

        int xPos = roomCoordinate.x * TILE_SIZE;
        int yPos = roomCoordinate.y * TILE_SIZE;

        QGraphicsProxyWidget *graphicsItem = this->addWidget(label);
        graphicsItem->setPos(xPos, yPos);
    }

    QRect rect(0, 0, TILE_SIZE, TILE_SIZE);

    QVector<GameElementSuspect *> suspects = GameElementFactory::getSuspects();
    for (GameElementSuspect *suspect : suspects)
    {
        QPixmap pixmap;
        if (suspect->getId() == static_cast<int>(SuspectsEnum::Green))
        {
            pixmap = QPixmap(":/pawnGreen.png");
        }
        else if (suspect->getId() == static_cast<int>(SuspectsEnum::Mustard))
        {
            pixmap = QPixmap(":/pawnMustard.png");
        }
        else if (suspect->getId() == static_cast<int>(SuspectsEnum::Peacock))
        {
            pixmap = QPixmap(":/pawnPeacock.png");
        }
        else if (suspect->getId() == static_cast<int>(SuspectsEnum::Plum))
        {
            pixmap = QPixmap(":/pawnPlum.png");
        }
        else if (suspect->getId() == static_cast<int>(SuspectsEnum::Scarlet))
        {
            pixmap = QPixmap(":/pawnScarlet.png");
        }
        else if (suspect->getId() == static_cast<int>(SuspectsEnum::White))
        {
            pixmap = QPixmap(":/pawnWhite.png");
        }
        pixmap = pixmap.scaled(rect.height(), rect.width());

        QLabel *label = new QLabel();
        label->setStyleSheet("background-color:transparent");

        label->setPixmap(pixmap);

        int xPos = suspect->getPosition().x * TILE_SIZE;
        int yPos = suspect->getPosition().y * TILE_SIZE;

        QGraphicsProxyWidget *graphicsItem = this->addWidget(label);
        graphicsItem->setZValue(99);
        graphicsItem->setPos(xPos, yPos);
        m_suspectsPawns.insert(suspect->getId(), graphicsItem);
    }

}

void BoardUi::playerEndTurn()
{
    disablePlayerMovementMod();
}

void BoardUi::setPlayerPosition(const Coordinate &coord, int suspectId)
{
    QGraphicsProxyWidget *player = m_suspectsPawns[suspectId];
    player->setPos(coord.x * TILE_SIZE, coord.y * TILE_SIZE);
}

void BoardUi::drawPathToCursor(const Coordinate &startingPos, QVector<Direction> directions)
{
    for (QGraphicsProxyWidget *graphicsItem : m_arrowItems)
    {
        graphicsItem->deleteLater();
    }
    m_arrowItems.clear();

    Direction lastDirection = Direction::NONE;
    Coordinate currentPos = startingPos;
    for (int i = 0 ; i < directions.size() ; ++i)
    {
        Direction currentDirection = directions.at(i);
        QLabel *labelArrow = new QLabel();
        QPixmap arrow;

        if (currentDirection == Direction::UP)
        {
            if (lastDirection == Direction::NONE)
            {
                arrow.load(":/base_up.png");
            }
            else if (lastDirection == Direction::UP)
            {
                arrow.load(":/straight_vertical.png");
            }
            else if (lastDirection == Direction::RIGHT)
            {
                arrow.load(":/turn_right_up.png");
            }
            else if (lastDirection == Direction::LEFT)
            {
                arrow.load(":/turn_down_right.png");
            }
        }
        else if (currentDirection == Direction::RIGHT)
        {
            if (i == 0)
            {
                arrow.load(":/base_right.png");
            }
            else if (lastDirection == Direction::UP)
            {
                arrow.load(":/turn_up_right.png");
            }
            else if (lastDirection == Direction::RIGHT)
            {
                arrow.load(":/straight_horizontal.png");
            }
            else if (lastDirection == Direction::DOWN)
            {
                arrow.load(":/turn_down_right.png");
            }
        }
        else if (currentDirection == Direction::DOWN)
        {
            if (i == 0)
            {
                arrow.load(":/base_down.png");
            }
            else if (lastDirection == Direction::LEFT)
            {
                arrow.load(":/turn_up_right.png");
            }
            else if (lastDirection == Direction::RIGHT)
            {
                arrow.load(":/turn_up_left.png");
            }
            else if (lastDirection == Direction::DOWN)
            {
                arrow.load(":/straight_vertical.png");
            }
        }
        else if (currentDirection == Direction::LEFT)
        {
            if (i == 0)
            {
                arrow.load(":/base_left.png");
            }
            else if (lastDirection == Direction::LEFT)
            {
                arrow.load(":/straight_horizontal.png");
            }
            else if (lastDirection == Direction::UP)
            {
                arrow.load(":/turn_up_left.png");
            }
            else if (lastDirection == Direction::DOWN)
            {
                arrow.load(":/turn_right_up.png");
            }
        }

        arrow = arrow.scaled(TILE_SIZE, TILE_SIZE);
        labelArrow->setPixmap(arrow);
        labelArrow->setStyleSheet("background-color:transparent");

        QGraphicsProxyWidget *graphicsItem = addWidget(labelArrow);
        graphicsItem->setPos(currentPos.x * TILE_SIZE, currentPos.y * TILE_SIZE);

        m_arrowItems.push_back(graphicsItem);

        if (currentDirection == Direction::UP)
        {
            currentPos.y--;
        }
        else if (currentDirection == Direction::RIGHT)
        {
            currentPos.x++;
        }
        else if (currentDirection == Direction::DOWN)
        {
            currentPos.y++;
        }
        else if (currentDirection == Direction::LEFT)
        {
            currentPos.x--;
        }
        lastDirection = currentDirection;
    }

    QPixmap arrow;

    if (lastDirection == Direction::UP)
    {
        arrow.load(":/arrow_up.png");
    }
    else if (lastDirection == Direction::RIGHT)
    {
        arrow.load(":/arrow_right.png");
    }
    else if (lastDirection == Direction::DOWN)
    {
        arrow.load(":/arrow_down.png");
    }
    else if (lastDirection == Direction::LEFT)
    {
        arrow.load(":/arrow_left.png");
    }

    if (lastDirection != Direction::NONE)
    {
        QLabel *labelArrow = new QLabel();

        arrow = arrow.scaled(TILE_SIZE, TILE_SIZE);
        labelArrow->setPixmap(arrow);
        labelArrow->setStyleSheet("background-color:transparent");

        QGraphicsProxyWidget *graphicsItem = addWidget(labelArrow);
        graphicsItem->setPos(currentPos.x * TILE_SIZE, currentPos.y * TILE_SIZE);

        m_arrowItems.push_back(graphicsItem);
    }
}

void BoardUi::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    // Are we in PlayerMovementMod
    if (m_highlightItems.size() > 0)
    {
        int indexX = event->scenePos().x() / TILE_SIZE;
        int indexY = event->scenePos().y() / TILE_SIZE;
        Coordinate coord(indexX, indexY);

        auto iter = m_highlightItems.find(coord);
        if (iter != m_highlightItems.end() && coord != m_lastCoordUsedToDrawArrow)
        {
            m_lastCoordUsedToDrawArrow = coord;

            PathFinder *pathFinder = PathFinder::getPathFinderInstance(m_board);
            QVector<Direction> shortestDirections = pathFinder->findShortestPath(m_currentPlayerPossiblePosition.at(0), Coordinate(indexX, indexY));
            Coordinate shortestStartingPos = m_currentPlayerPossiblePosition.at(0);
            for (int i = 1 ; i < m_currentPlayerPossiblePosition.size() ; ++i)
            {
                QVector<Direction> directions = pathFinder->findShortestPath(m_currentPlayerPossiblePosition.at(i), Coordinate(indexX, indexY));
                if (directions.size() < shortestDirections.size())
                {
                    shortestDirections = directions;
                    shortestStartingPos = m_currentPlayerPossiblePosition.at(i);
                }
            }

            drawPathToCursor(shortestStartingPos, shortestDirections);
        }
    }

    QGraphicsScene::mouseMoveEvent(event);
}

void BoardUi::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    // Are we in PlayerMovementMod
    if (m_highlightItems.size() > 0)
    {
        int indexX = event->scenePos().x() / TILE_SIZE;
        int indexY = event->scenePos().y() / TILE_SIZE;
        Coordinate coord(indexX, indexY);

        auto iter = m_highlightItems.find(coord);
        if (iter != m_highlightItems.end())
        {
            disablePlayerMovementMod();
            emit playerMovedTo(coord);
        }
        event->accept();
    }
    else
    {
        QGraphicsScene::mousePressEvent(event);
    }
}





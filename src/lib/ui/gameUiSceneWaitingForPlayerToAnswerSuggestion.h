#ifndef GAMEUISCENEWAITINGFORPLAYERTOANSWERSUGGESTION_H
#define GAMEUISCENEWAITINGFORPLAYERTOANSWERSUGGESTION_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QVector>
#include <QObject>

#include "core/gameLogic/card.h"
#include "core/gameLogic/suggestion.h"
#include "core/gameLogic/gameElement/gameElementSuspect.h"

class GameUiSceneWaitingForPLayerToAnswerSuggestion : public QObject
{
    Q_OBJECT
public:
    explicit GameUiSceneWaitingForPLayerToAnswerSuggestion(QGraphicsScene *scene);

    void clearScene();
    void showScene(GameElementSuspect *suspectWhoMadeSuggestion, GameElementSuspect *suspectAnswering, const Suggestion &suggestion);

protected:
    QVector<QGraphicsItem *> m_items;
    QGraphicsScene *m_scene;

    void createBackGroundItem();
};

#endif // GAMEUISCENEWAITINGFORPLAYERTOANSWERSUGGESTION_H

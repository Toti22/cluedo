#ifndef JOINSERVERUI_H
#define JOINSERVERUI_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGridLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QSpinBox>

class JoinServerUi : public QWidget
{
    Q_OBJECT
public:
    explicit JoinServerUi(QGraphicsScene *scene);

    void clearScene();
    void showMenu();

protected slots:
    void joinServerSlot();
    void cancelSlot();

protected:
    QGraphicsScene *m_scene;
    QVector<QGraphicsItem *> m_items;
    QLineEdit *m_lineEditIp;
    QSpinBox *m_spinBoxPort;

    void createBackGroundItem();
    QWidget *createMenu();

signals:
    void joinServer(QString, int);
    void cancel();
};

#endif // JOINSERVERUI_H

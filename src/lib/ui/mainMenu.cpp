#include "mainMenu.h"

#include <QPainter>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QBrush>
#include <QRect>
#include <QGraphicsProxyWidget>
#include <QPalette>
#include <QRandomGenerator>

#include "core/board/BoardCaracteristics.h"

MainMenu::MainMenu(QGraphicsScene *scene)
    : m_scene(scene)
{
}

void MainMenu::clearScene()
{
    for (QGraphicsItem *item : m_items)
    {
        m_scene->removeItem(item);
    }
    m_items.clear();
}

void MainMenu::showMenu()
{
    createBackGroundItem();
    QGraphicsItem *item = m_scene->addWidget(createMenu());
    m_items.push_back(item);
    item->setZValue(1);
}

void MainMenu::createLocalGameSlot()
{
    clearScene();
    emit createLocalGame();

}

void MainMenu::createHostGameSlot()
{
    clearScene();
    emit createHostGame();
}

void MainMenu::joinGameSlot()
{
    clearScene();
    emit joinGame();
}

void MainMenu::createBackGroundItem()
{
    int xSize = BOARD_X_SIZE * TILE_SIZE;
    int ySize = BOARD_Y_SIZE * TILE_SIZE;
    int randomMenu = QRandomGenerator::global()->bounded(0, 10);

    QRect rect(0, 0, xSize, ySize);
    QPixmap pixmap;

    if (randomMenu == 0)
    {
        pixmap = QPixmap(":/menuRyanReynolds.png");
    }
    else
    {
        pixmap = QPixmap(":/menu.png");
    }

    pixmap = pixmap.scaled(rect.width(), rect.height(), Qt::IgnoreAspectRatio);

    QGraphicsPixmapItem *itemBackground = m_scene->addPixmap(pixmap);
    m_items.push_back(itemBackground);
    itemBackground->setPos(0, 0);
    itemBackground->setOpacity(1);
    itemBackground->setZValue(0);
}

QWidget * MainMenu::createMenu()
{
    int xSize = BOARD_X_SIZE * TILE_SIZE;
    int ySize = BOARD_Y_SIZE * TILE_SIZE;
    QWidget *widget = new QWidget;

    widget->setFixedSize(xSize, ySize);
    widget->setAutoFillBackground(true);
    widget->setStyleSheet("font-size: 48px;background-color: rgba(255, 255, 255, 0);");

    QPushButton *buttonCreateLocalGame = new QPushButton;
    buttonCreateLocalGame->setAutoFillBackground(true);
    buttonCreateLocalGame->setText(tr("Create Local Game"));
    buttonCreateLocalGame->setStyleSheet("font-size: 48px;background-color: rgba(255, 255, 255, 255);");

    QPushButton *buttonHostGame = new QPushButton;
    buttonHostGame->setAutoFillBackground(true);
    buttonHostGame->setText(tr("Host Game"));
    buttonHostGame->setStyleSheet("font-size: 48px;background-color: rgba(255, 255, 255, 255);");

    QPushButton *buttonJoinGame = new QPushButton;
    buttonJoinGame->setAutoFillBackground(true);
    buttonJoinGame->setText(tr("Join Game"));
    buttonJoinGame->setStyleSheet("font-size: 48px;background-color: rgba(255, 255, 255, 255);");

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(buttonCreateLocalGame, 1, 1);
    layout->addWidget(buttonHostGame, 2, 1);
    layout->addWidget(buttonJoinGame, 3, 1);

    QWidget *upperSpacer = new QWidget;
    QWidget *downSpacer = new QWidget;
    QWidget *leftSpacer = new QWidget;
    QWidget *rightSpacer = new QWidget;
    upperSpacer->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    downSpacer->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    leftSpacer->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    rightSpacer->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    layout->addWidget(upperSpacer, 0, 0, 1, 3);
    layout->addWidget(downSpacer, 4, 0, 1, 3);
    layout->addWidget(leftSpacer, 1, 0, 3, 1);
    layout->addWidget(rightSpacer, 1, 2, 3, 1);

    widget->setLayout(layout);

    connect(buttonCreateLocalGame, SIGNAL(pressed()), this, SLOT(createLocalGameSlot()));
    connect(buttonHostGame, SIGNAL(pressed()), this, SLOT(createHostGameSlot()));
    connect(buttonJoinGame, SIGNAL(pressed()), this, SLOT(joinGameSlot()));

    return widget;
}

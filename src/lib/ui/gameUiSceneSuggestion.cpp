#include "gameUiSceneSuggestion.h"

#include <QPainter>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QLabel>
#include <QPushButton>

#include "core/board/BoardCaracteristics.h"


GameUiSceneSuggestion::GameUiSceneSuggestion(QGraphicsScene *scene) : m_scene(scene), m_roomCardSelected(nullptr),
    m_suspectCardSelected(nullptr), m_weaponCardSelected(nullptr)
{
    QPushButton *pushButton = new QPushButton;
    pushButton->setText("OK");
    m_validateSuggestionButton = m_scene->addWidget(pushButton);
    m_validateSuggestionButton->setZValue(101);
    m_validateSuggestionButton->setVisible(false);

    connect(pushButton, SIGNAL(pressed()), this, SLOT(validateSuggestionSlot()));
}

void GameUiSceneSuggestion::requestShowSuggestionMenu(Card *roomCard)
{
    int xSize = BOARD_X_SIZE * TILE_SIZE;
    int ySize = BOARD_Y_SIZE * TILE_SIZE;

    QRect rect(0, 0, xSize, ySize);

    QBrush blackBrush(Qt::GlobalColor::black);

    QPixmap pixmap(":/onePixel.png");
    pixmap = pixmap.scaled(rect.width(), rect.height(), Qt::IgnoreAspectRatio);
    QPainter painter(&pixmap);
    painter.fillRect(0, 0, xSize, ySize, blackBrush);

    QGraphicsPixmapItem *itemBackground = m_scene->addPixmap(pixmap);
    m_suggestionMenuItems.push_back(itemBackground);
    itemBackground->setPos(0, 0);
    itemBackground->setOpacity(0.6);
    itemBackground->setZValue(100);

    int yPos = ySize * 0.15;

    // Rooms
    QVector<Card *> roomCards = Card::getAllRoomCards();
    int xAvialableSpaceForCards = xSize * 0.85;
    int xCardSize = xAvialableSpaceForCards / roomCards.size();
    int yCardSize = xCardSize * 2;
    int xPos = xSize * 0.01;

    QLabel *labelRoom = new QLabel("Room");

    QGraphicsProxyWidget *item = m_scene->addWidget(labelRoom);
    m_suggestionMenuItems.push_back(item);
    item->setPos((xSize / 2) - (item->size().width() / 2), yPos - 20);
    item->setZValue(101);

    Card *card = roomCard;
    m_roomCardSelected = card;
    QLabel *labelRoomCard = new QLabel();
    labelRoomCard->setPixmap(card->getPixmap().scaled(xCardSize, yCardSize));
    QGraphicsProxyWidget *roomItem = m_scene->addWidget(labelRoomCard);
    m_suggestionMenuItems.push_back(roomItem);
    roomItem->setPos(xPos, yPos);
    roomItem->setZValue(101);

    xPos += xCardSize + 10;

    // Suspects
    yPos = ySize * 0.38;
    xPos = xSize * 0.01;

    labelRoom = new QLabel("Suspects");
    item = m_scene->addWidget(labelRoom);
    m_suggestionMenuItems.push_back(item);
    item->setPos((xSize / 2) - (item->size().width() / 2), yPos - 20);
    item->setZValue(101);

    QVector<Card *> suspectCards = Card::getAllSuspectCards();
    for (int i = 0 ; i < suspectCards.size() ; ++i)
    {
        Card *card = suspectCards.at(i);
        SelectionableCard *label = new SelectionableCard(card->getElementId(), card->getPixmap().scaled(xCardSize, yCardSize));
        connect(label, SIGNAL(selected(SelectionableCard*)), this, SLOT(cardSelected(SelectionableCard*)));
        connect(label, SIGNAL(unSelected(SelectionableCard*)), this, SLOT(cardUnSelected(SelectionableCard*)));
        m_selectableSuspectCards.push_back(label);
        QGraphicsProxyWidget *roomItem = m_scene->addWidget(label);
        m_suggestionMenuItems.push_back(roomItem);
        roomItem->setPos(xPos, yPos);
        roomItem->setZValue(101);

        xPos += xCardSize + 10;
    }

    // Weapons
    yPos = ySize * 0.61;
    xPos = xSize * 0.01;

    labelRoom = new QLabel("Weapons");
    item = m_scene->addWidget(labelRoom);
    m_suggestionMenuItems.push_back(item);
    item->setPos((xSize / 2) - (item->size().width() / 2), yPos - 20);
    item->setZValue(101);

    QVector<Card *> weaponCards = Card::getAllWeaponCards();
    for (int i = 0 ; i < weaponCards.size() ; ++i)
    {
        Card *card = weaponCards.at(i);
        SelectionableCard *label = new SelectionableCard(card->getElementId(), card->getPixmap().scaled(xCardSize, yCardSize));
        connect(label, SIGNAL(selected(SelectionableCard*)), this, SLOT(cardSelected(SelectionableCard*)));
        connect(label, SIGNAL(unSelected(SelectionableCard*)), this, SLOT(cardUnSelected(SelectionableCard*)));
        m_selectableWeaponsCards.push_back(label);

        QGraphicsProxyWidget *roomItem = m_scene->addWidget(label);
        m_suggestionMenuItems.push_back(roomItem);
        roomItem->setPos(xPos, yPos);
        roomItem->setZValue(101);

        xPos += xCardSize + 10;
    }

    // PushButton Pos
    yPos = ySize * 0.90;

    m_validateSuggestionButton->setVisible(true);
    m_validateSuggestionButton->setPos((xSize / 2) - (m_validateSuggestionButton->size().width() / 2), yPos);
}

void GameUiSceneSuggestion::clearScene()
{
    for (QGraphicsItem *item : m_suggestionMenuItems)
    {
        m_scene->removeItem(item);
    }
    m_suggestionMenuItems.clear();
    m_validateSuggestionButton->setVisible(false);
}

void GameUiSceneSuggestion::cardSelected(SelectionableCard *cardUi)
{
    Card *card = Card::getCard(cardUi->getCardId());
    if (Card::isWeaponCard(card) == true)
    {
        if (m_weaponCardSelected != nullptr && m_weaponCardSelected != cardUi)
        {
            m_weaponCardSelected->setSelectected(false);
        }
        m_weaponCardSelected = cardUi;
    }
    else if (Card::isSuspectCard(card) == true)
    {
        if (m_suspectCardSelected != nullptr && m_suspectCardSelected != cardUi)
        {
            m_suspectCardSelected->setSelectected(false);
        }
        m_suspectCardSelected = cardUi;
    }
}

void GameUiSceneSuggestion::cardUnSelected(SelectionableCard *cardUi)
{
    Card *card = Card::getCard(cardUi->getCardId());
    if (Card::isWeaponCard(card) == true)
    {
        m_weaponCardSelected = nullptr;
    }
    else if (Card::isSuspectCard(card) == true)
    {
        m_suspectCardSelected = nullptr;
    }
}

void GameUiSceneSuggestion::validateSuggestionSlot()
{
    if (m_roomCardSelected != nullptr && m_weaponCardSelected != nullptr && m_suspectCardSelected != nullptr)
    {
        Suggestion suggestion;
        suggestion.murderer = m_suspectCardSelected->getCardId();
        suggestion.room = m_roomCardSelected->getElementId();
        suggestion.weapon = m_weaponCardSelected->getCardId();
        emit validateSuggestion(suggestion);
        m_roomCardSelected = nullptr;
        m_weaponCardSelected = nullptr;
        m_suspectCardSelected = nullptr;
        clearScene();
    }
}

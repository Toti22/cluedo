#include "gameUiSceneResultSuggestion.h"

#include <QPainter>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QGraphicsProxyWidget>
#include <QLabel>

#include "core/board/BoardCaracteristics.h"

GameUiSceneResultSuggestion::GameUiSceneResultSuggestion(QGraphicsScene *scene) : m_scene(scene)
{
}

void GameUiSceneResultSuggestion::clearScene()
{
    for (QGraphicsItem *item : m_items)
    {
        m_scene->removeItem(item);
    }
    m_items.clear();
}

void GameUiSceneResultSuggestion::showSuggestion(GameElementSuspect *suspectWhoAnswered, GameElementSuspect *suspectWhoAsked, const Suggestion &suggestion, Card * clueShown)
{
    createBackGroundItem();

    int xSize = BOARD_X_SIZE * TILE_SIZE;
    int ySize = BOARD_Y_SIZE * TILE_SIZE;

    int yPos = ySize * 0.05;

    // Suggestion player Portrait
    QPixmap portaitPlayerSuggestion = suspectWhoAnswered->getPortrait().scaled(ySize * 0.12, ySize * 0.12);
    QGraphicsPixmapItem *portaitPlayerSuggestionItem = m_scene->addPixmap(portaitPlayerSuggestion);
    m_items.push_back(portaitPlayerSuggestionItem);
    portaitPlayerSuggestionItem->setZValue(101);
    portaitPlayerSuggestionItem->setPos((xSize / 2) - (portaitPlayerSuggestionItem->boundingRect().width() / 2), yPos);

    yPos = ySize * 0.20;

    // Insert Label "Answered to :"
    QLabel *labelIsAsking = new QLabel("Answered to : ");

    QGraphicsProxyWidget *item = m_scene->addWidget(labelIsAsking);
    m_items.push_back(item);
    item->setPos((xSize / 2) - (item->size().width() / 2), yPos - 20);
    item->setZValue(101);

    // Answering player Portrait
    QPixmap portaitPlayerAnswering = suspectWhoAsked->getPortrait().scaled(ySize * 0.12, ySize * 0.12);
    QGraphicsPixmapItem *portaitPlayerAnsweringItem = m_scene->addPixmap(portaitPlayerAnswering);
    m_items.push_back(portaitPlayerAnsweringItem);
    portaitPlayerAnsweringItem->setZValue(101);
    portaitPlayerAnsweringItem->setPos((xSize / 2) - (portaitPlayerAnsweringItem->boundingRect().width() / 2), yPos);

    yPos = ySize * 0.35;

    // Insert Label "For :"
    QLabel *labelFor = new QLabel("For : ");

    item = m_scene->addWidget(labelFor);
    m_items.push_back(item);
    item->setPos((xSize / 2) - (item->size().width() / 2), yPos - 20);
    item->setZValue(101);

    //Suggestion
    Card *suspectCard = Card::getCard(suggestion.murderer);
    Card *weaponCard = Card::getCard(suggestion.weapon);
    Card *roomCard = Card::getCard(suggestion.room);
    QVector<Card*> cards;
    cards.push_back(suspectCard);
    cards.push_back(weaponCard);
    cards.push_back(roomCard);

    int xAvialableSpaceForCards = xSize * 0.86;
    int xCardSize = xAvialableSpaceForCards / Card::getAllRoomCards().size();
    int yCardSize = xCardSize * 2;
    int xPos = xSize * 0.07;

    for (Card* card : cards)
    {
        QLabel *label = new QLabel();
        label->setPixmap(card->getPixmap().scaled(xCardSize, yCardSize));
        QGraphicsProxyWidget *cardItem = m_scene->addWidget(label);
        m_items.push_back(cardItem);
        cardItem->setPos(xPos, yPos);
        cardItem->setZValue(101);

        xPos += xAvialableSpaceForCards / cards.size();
    }

    yPos = ySize * 0.65;

    // Insert Label "With This Clue :"
    QLabel *labelClues = new QLabel("With This Clue : ");

    item = m_scene->addWidget(labelClues);
    m_items.push_back(item);
    item->setPos((xSize / 2) - (item->size().width() / 2), yPos - 20);
    item->setZValue(101);

    //Clues
    xPos = xSize * 0.07;
    QLabel *label = new QLabel();
    label->setPixmap(clueShown->getPixmap().scaled(xCardSize, yCardSize));
    QGraphicsProxyWidget *cardItem = m_scene->addWidget(label);
    m_items.push_back(cardItem);
    cardItem->setPos(xPos, yPos);
    cardItem->setZValue(101);

    yPos = ySize * 0.85;

    //PushButton Ok
    QPushButton *pushButton = new QPushButton();
    pushButton->setText("OK");
    connect(pushButton, SIGNAL(pressed()), this, SLOT(acceptedSlot()));
    QGraphicsProxyWidget *pushButtonItem = m_scene->addWidget(pushButton);
    m_items.push_back(pushButtonItem);
    pushButtonItem->setPos((xSize / 2) - (pushButtonItem->size().width() / 2), yPos);
    pushButtonItem->setZValue(101);
    pushButtonItem->setVisible(true);
}

void GameUiSceneResultSuggestion::createBackGroundItem()
{
    int xSize = BOARD_X_SIZE * TILE_SIZE;
    int ySize = BOARD_Y_SIZE * TILE_SIZE;

    QRect rect(0, 0, xSize, ySize);

    QBrush blackBrush(Qt::GlobalColor::black);

    QPixmap pixmap(":/onePixel.png");
    pixmap = pixmap.scaled(rect.width(), rect.height(), Qt::IgnoreAspectRatio);
    QPainter painter(&pixmap);
    painter.fillRect(0, 0, xSize, ySize, blackBrush);

    QGraphicsPixmapItem *itemBackground = m_scene->addPixmap(pixmap);
    m_items.push_back(itemBackground);
    itemBackground->setPos(0, 0);
    itemBackground->setOpacity(0.6);
    itemBackground->setZValue(100);
}

void GameUiSceneResultSuggestion::acceptedSlot()
{
    clearScene();
    emit accepted();
}

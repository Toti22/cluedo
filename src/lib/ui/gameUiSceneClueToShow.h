#ifndef GAMEUISCENECLUETOSHOW_H
#define GAMEUISCENECLUETOSHOW_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QVector>
#include <QObject>

#include "core/gameLogic/card.h"
#include "core/gameLogic/suggestion.h"
#include "core/gameLogic/gameElement/gameElementSuspect.h"
#include "selectionableCard.h"

class GameUiSceneClueToShow : public QObject
{
    Q_OBJECT
public:
    GameUiSceneClueToShow(QGraphicsScene *scene);

    void clearScene();
    void showSuggestion(GameElementSuspect *suspectWhoMadeSuggestion, GameElementSuspect *suspectAnswering, const Suggestion &suggestion, QVector<Card *> cluesInPLayerPossession);

signals:
    void clueSelected(int);
protected:
    QVector<QGraphicsItem *> m_items;
    QGraphicsScene *m_scene;

    void createBackGroundItem();

protected slots:
    void selectedCardSlot(SelectionableCard* card);

};

#endif // GAMEUISCENECLUETOSHOW_H

#ifndef BOARDUI_H
#define BOARDUI_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsProxyWidget>
#include <QGraphicsSceneMouseEvent>
#include <QMap>
#include <QVector>

#include "core/board/board.h"
#include "core/coordinate.h"
#include "core/pathFinder.h"

class BoardUi : public QGraphicsScene
{
    Q_OBJECT
public:
    BoardUi();

    QGraphicsView *getGraphicsView();

signals:
    void playerMovedTo(Coordinate);

public slots:
    void clearBoard();
    void playerStartTurn(int idCurrentPlayerPlaying);
    void enablePlayerMovementMod(int idCurrentPlayerPlaying, const Coordinate &playerLocation, const QVector<Coordinate> &coordinates);
    void disablePlayerMovementMod();
    void playerMoveToResult(const Coordinate &coord);
    void init();
    void playerEndTurn();
    void setPlayerPosition(const Coordinate &coord, int suspectId);

private:
    void drawPathToCursor(const Coordinate &startingPos, QVector<Direction> directions);

    Board * m_board;
    QVector<Coordinate> m_currentPlayerPossiblePosition;
    Coordinate m_lastCoordUsedToDrawArrow;
    int m_currentPlayerPlaying;
    QGraphicsView *m_graphicsView;
    QMap<Coordinate, QGraphicsProxyWidget *> m_highlightItems;
    QMap<int, QGraphicsProxyWidget *> m_suspectsPawns;
    QVector<QGraphicsProxyWidget *> m_arrowItems;


    // QGraphicsScene interface
protected:
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
};

inline QGraphicsView *BoardUi::getGraphicsView()
{
    return m_graphicsView;
}

#endif // BOARDUI_H

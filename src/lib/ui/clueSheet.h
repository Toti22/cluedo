#ifndef CLUESHEET_H
#define CLUESHEET_H

#include "qframe.h"
#include <QWidget>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QVector>
#include <QString>
#include <QFrame>

#include "core/players/clueSheetData.h"

class ClueSheet : public QWidget
{
    Q_OBJECT
public:
    explicit ClueSheet(QWidget *parent = nullptr);

    QVBoxLayout *getLayout() const;

public slots:
    void init();

    void loadClueSheetData(ClueSheetData *clueSheetData);
    void checkClue(int clueId, bool checked);

private:
    QVBoxLayout *m_layout;
    QVector<QCheckBox *> m_checkBox;
    QFrame *createLineSeparator();
    ClueSheetData *m_clueSheetData;

protected slots:
    void elementChecked(bool checked);

signals:

};

inline QVBoxLayout *ClueSheet::getLayout() const
{
    return m_layout;
}

#endif // CLUESHEET_H

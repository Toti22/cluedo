#include "GameUiSceneAccusation.h"

#include <QPainter>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QLabel>

#include "core/board/BoardCaracteristics.h"


GameUiSceneAccusation::GameUiSceneAccusation(QGraphicsScene *scene) : m_scene(scene), m_roomCardSelected(nullptr),
    m_suspectCardSelected(nullptr), m_weaponCardSelected(nullptr)
{
    /*
    QPushButton *pushButton = new QPushButton;
    pushButton->setText("OK");
    m_validateAccusationButton = m_scene->addWidget(pushButton);
    m_validateAccusationButton->setZValue(101);
    m_validateAccusationButton->setVisible(false);

    connect(pushButton, SIGNAL(pressed()), this, SLOT(validateAccusationSlot()));

    */

    m_pushButtonOk.setText("OK");
    m_validateAccusationButton = m_scene->addWidget(&m_pushButtonOk);
    m_validateAccusationButton->setZValue(101);
    m_validateAccusationButton->setVisible(false);

    connect(&m_pushButtonOk, SIGNAL(pressed()), this, SLOT(validateAccusationSlot()));


    QPushButton *pushButtonCancel = new QPushButton;
    pushButtonCancel->setText("Cancel");
    m_cancelAccusationButton = m_scene->addWidget(pushButtonCancel);
    m_cancelAccusationButton->setZValue(101);
    m_cancelAccusationButton->setVisible(false);

    connect(pushButtonCancel, SIGNAL(pressed()), this, SLOT(cancelAccusationSlot()));
}

void GameUiSceneAccusation::requestShowAccusationMenu()
{
    int xSize = BOARD_X_SIZE * TILE_SIZE;
    int ySize = BOARD_Y_SIZE * TILE_SIZE;

    QRect rect(0, 0, xSize, ySize);

    QBrush blackBrush(Qt::GlobalColor::black);

    QPixmap pixmap(":/onePixel.png");
    pixmap = pixmap.scaled(rect.width(), rect.height(), Qt::IgnoreAspectRatio);
    QPainter painter(&pixmap);
    painter.fillRect(0, 0, xSize, ySize, blackBrush);

    QGraphicsPixmapItem *itemBackground = m_scene->addPixmap(pixmap);
    m_accusationMenuItems.push_back(itemBackground);
    itemBackground->setPos(0, 0);
    itemBackground->setOpacity(0.6);
    itemBackground->setZValue(100);

    int yPos = ySize * 0.15;

    // Rooms
    QVector<Card *> roomCards = Card::getAllRoomCards();
    int xAvialableSpaceForCards = xSize * 0.85;
    int xCardSize = xAvialableSpaceForCards / roomCards.size();
    int yCardSize = xCardSize * 2;
    int xPos = xSize * 0.01;

    QLabel *labelRoom = new QLabel("Room");

    QGraphicsProxyWidget *item = m_scene->addWidget(labelRoom);
    m_accusationMenuItems.push_back(item);
    item->setPos((xSize / 2) - (item->size().width() / 2), yPos - 20);
    item->setZValue(101);

    for (int i = 0 ; i < roomCards.size() ; ++i)
    {
        Card *card = roomCards.at(i);
        SelectionableCard *label = new SelectionableCard(card->getElementId(), card->getPixmap().scaled(xCardSize, yCardSize));
        connect(label, SIGNAL(selected(SelectionableCard*)), this, SLOT(cardSelected(SelectionableCard*)));
        connect(label, SIGNAL(unSelected(SelectionableCard*)), this, SLOT(cardUnSelected(SelectionableCard*)));
        m_selectableRoomCards.push_back(label);
        QGraphicsProxyWidget *roomItem = m_scene->addWidget(label);
        m_accusationMenuItems.push_back(roomItem);
        roomItem->setPos(xPos, yPos);
        roomItem->setZValue(101);

        xPos += xCardSize + 10;
    }

    // Suspects
    yPos = ySize * 0.38;
    xPos = xSize * 0.01;

    labelRoom = new QLabel("Suspects");
    item = m_scene->addWidget(labelRoom);
    m_accusationMenuItems.push_back(item);
    item->setPos((xSize / 2) - (item->size().width() / 2), yPos - 20);
    item->setZValue(101);

    QVector<Card *> suspectCards = Card::getAllSuspectCards();
    for (int i = 0 ; i < suspectCards.size() ; ++i)
    {
        Card *card = suspectCards.at(i);
        SelectionableCard *label = new SelectionableCard(card->getElementId(), card->getPixmap().scaled(xCardSize, yCardSize));
        connect(label, SIGNAL(selected(SelectionableCard*)), this, SLOT(cardSelected(SelectionableCard*)));
        connect(label, SIGNAL(unSelected(SelectionableCard*)), this, SLOT(cardUnSelected(SelectionableCard*)));
        m_selectableSuspectCards.push_back(label);
        QGraphicsProxyWidget *roomItem = m_scene->addWidget(label);
        m_accusationMenuItems.push_back(roomItem);
        roomItem->setPos(xPos, yPos);
        roomItem->setZValue(101);

        xPos += xCardSize + 10;
    }

    // Weapons
    yPos = ySize * 0.61;
    xPos = xSize * 0.01;

    labelRoom = new QLabel("Weapons");
    item = m_scene->addWidget(labelRoom);
    m_accusationMenuItems.push_back(item);
    item->setPos((xSize / 2) - (item->size().width() / 2), yPos - 20);
    item->setZValue(101);

    QVector<Card *> weaponCards = Card::getAllWeaponCards();
    for (int i = 0 ; i < weaponCards.size() ; ++i)
    {
        Card *card = weaponCards.at(i);
        SelectionableCard *label = new SelectionableCard(card->getElementId(), card->getPixmap().scaled(xCardSize, yCardSize));
        connect(label, SIGNAL(selected(SelectionableCard*)), this, SLOT(cardSelected(SelectionableCard*)));
        connect(label, SIGNAL(unSelected(SelectionableCard*)), this, SLOT(cardUnSelected(SelectionableCard*)));
        m_selectableWeaponsCards.push_back(label);

        QGraphicsProxyWidget *roomItem = m_scene->addWidget(label);
        m_accusationMenuItems.push_back(roomItem);
        roomItem->setPos(xPos, yPos);
        roomItem->setZValue(101);

        xPos += xCardSize + 10;
    }

    // PushButton Pos
    yPos = ySize * 0.90;

    m_validateAccusationButton->setVisible(true);
    m_validateAccusationButton->setPos((xSize / 2) - ((m_validateAccusationButton->size().width() + m_cancelAccusationButton->size().width()) / 2), yPos);

    m_cancelAccusationButton->setVisible(true);
    m_cancelAccusationButton->setPos((xSize / 2) + ((m_validateAccusationButton->size().width() + m_cancelAccusationButton->size().width()) / 2), yPos);
}

void GameUiSceneAccusation::clearScene()
{
    for (QGraphicsItem *item : m_accusationMenuItems)
    {
        m_scene->removeItem(item);
    }
    m_accusationMenuItems.clear();

    m_selectableSuspectCards.clear();
    m_selectableWeaponsCards.clear();
    m_selectableRoomCards.clear();

    m_validateAccusationButton->setVisible(false);
    m_cancelAccusationButton->setVisible(false);
}

void GameUiSceneAccusation::cardSelected(SelectionableCard *cardUi)
{
    Card *card = Card::getCard(cardUi->getCardId());
    if (Card::isWeaponCard(card) == true)
    {
        if (m_weaponCardSelected != nullptr && m_weaponCardSelected != cardUi)
        {
            m_weaponCardSelected->setSelectected(false);
        }
        m_weaponCardSelected = cardUi;
    }
    else if (Card::isSuspectCard(card) == true)
    {
        if (m_suspectCardSelected != nullptr && m_suspectCardSelected != cardUi)
        {
            m_suspectCardSelected->setSelectected(false);
        }
        m_suspectCardSelected = cardUi;
    }
    else if (Card::isRoomCard(card) == true)
    {
        if (m_roomCardSelected != nullptr && m_roomCardSelected != cardUi)
        {
            m_roomCardSelected->setSelectected(false);
        }
        m_roomCardSelected = cardUi;
    }
}

void GameUiSceneAccusation::cardUnSelected(SelectionableCard *cardUi)
{
    Card *card = Card::getCard(cardUi->getCardId());
    if (Card::isWeaponCard(card) == true)
    {
        m_weaponCardSelected = nullptr;
    }
    else if (Card::isSuspectCard(card) == true)
    {
        m_suspectCardSelected = nullptr;
    }
    else if (Card::isRoomCard(card) == true)
    {
        m_roomCardSelected = nullptr;
    }
}

void GameUiSceneAccusation::validateAccusationSlot()
{
    if (m_roomCardSelected != nullptr && m_weaponCardSelected != nullptr && m_suspectCardSelected != nullptr)
    {
        Suggestion accusation;
        accusation.murderer = m_suspectCardSelected->getCardId();
        accusation.room = m_roomCardSelected->getCardId();
        accusation.weapon = m_weaponCardSelected->getCardId();

        m_roomCardSelected = nullptr;
        m_weaponCardSelected = nullptr;
        m_suspectCardSelected = nullptr;
        clearScene();

        emit validateAccusation(accusation);
    }
}

void GameUiSceneAccusation::cancelAccusationSlot()
{
    m_roomCardSelected = nullptr;
    m_weaponCardSelected = nullptr;
    m_suspectCardSelected = nullptr;

    clearScene();
    emit cancelSignal();
}

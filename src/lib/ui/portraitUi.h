#ifndef PORTRAITUI_H
#define PORTRAITUI_H

#include <QWidget>
#include <QLabel>
#include <QPixmap>

class PortraitUi : public QWidget
{
    Q_OBJECT
public:
    explicit PortraitUi(const QSize &size, QWidget *parent = nullptr);
    void setPortrait(const QPixmap &pixmap);

private:
    QLabel m_label;
    QSize m_size;

signals:

};

#endif // PORTRAITUI_H

#include "gameUiLogic.h"

#include <QDialog>
#include <QVBoxLayout>
#include <QDebug>
#include <QStringBuilder>

#include "core/pathFinder.h"
#include "core/gameLogic/suggestion.h"

GameUiLogic::GameUiLogic(GameLogic *gameLogic, GameUi *gameUi, QObject *parent)
    : QObject{parent}, m_gameUi(gameUi), m_gameLogic(gameLogic)
{
    connect(m_gameLogic, SIGNAL(connectionSuccess(int)), this, SLOT(connectionSuccess(int)));
    connect(m_gameLogic, SIGNAL(connectionFailed()), this, SLOT(connectionFailed()));
    connect(m_gameLogic, SIGNAL(connectionToServerLost()), this, SLOT(connectionServerLost()));

    connect(m_gameLogic, SIGNAL(aClientHasJoinServer(int)), this, SLOT(aClientHasJoinServerSlot(int)));
    connect(m_gameLogic, SIGNAL(aClientHasLeftServer(int)), this, SLOT(aClientHasLeftServerSlot(int)));
    connect(m_gameLogic, SIGNAL(clientList(QVector<int>)), this, SLOT(clientListSlot(QVector<int>)));
    connect(m_gameLogic, SIGNAL(remoteGameStarted(Player*)), this, SLOT(remoteGameStarted(Player*)));
    connect(m_gameLogic, SIGNAL(hasToAnswerSuggestion(int,Suggestion,int, QVector<Card*>)), this, SLOT(playerHasToAnswerSuggestion(int,Suggestion,int, QVector<Card*>)));

    connect(m_gameUi, SIGNAL(createLocalGame()), this, SLOT(createLocalGameSlot()));
    connect(m_gameUi, SIGNAL(createRemoteGame()), this, SLOT(createRemoteGameSlot()));
    connect(m_gameUi, SIGNAL(joinRemoteGame(QString, int)), this, SLOT(joinRemoteGameSlot(QString, int)));

    connect(m_gameUi, SIGNAL(cancelLobby()), this, SLOT(cancelLobbySlot()));
    connect(m_gameUi, SIGNAL(startGame()), this, SLOT(startGameSlot()));
    connectToGameLogic();
    connectToCurrentPlayerSignal();
}

GameUiLogic::addLocalPlayer(Player *player)
{
    m_localPlayers.push_back(player);
}

void GameUiLogic::localGameStarted(QVector<Player *> players)
{
    m_localPlayers = players;
    QVector<int> charactersPlaying;
    for (Player *player : players)
    {
        charactersPlaying.push_back(player->getCharacterId());
    }
    gameStarted();
}

void GameUiLogic::gameStarted()
{
    QVector<GameElementSuspect *> suspects = GameElementFactory::getSuspects();
    QVector<int> suspectsPlaying;
    for (GameElementSuspect *suspect : suspects)
    {
        suspectsPlaying.push_back(suspect->getId());
    }
    PlayerDataForUi::s_playersData.clear();
    for (Player *player : m_localPlayers)
    {
        PlayerDataForUi *data = new PlayerDataForUi(player);
        PlayerDataForUi::s_playersData.insert(data->m_characterId, data);
    }
    for (int characterId : suspectsPlaying)
    {
        if (findLocalPlayer(characterId) == nullptr)
        {
            GameElementSuspect *suspect = GameElementFactory::getSuspectById(characterId);
            PlayerDataForUi *data = new PlayerDataForUi(suspect);
            PlayerDataForUi::s_playersData.insert(data->m_characterId, data);
        }
    }
    m_gameUi->gameStarted();
}

void GameUiLogic::remoteGameStarted(Player *localPlayer)
{
    m_localPlayers.push_back(localPlayer);
    gameStarted();
    m_gameUi->initPlayerUi(localPlayer->getCharacterId());
}

void GameUiLogic::playerStartTurn(int playerId)
{
    m_currentPlayerId = playerId;

    QDialog *dialog = new QDialog();
    dialog->setModal(true);
    QLabel *label = new QLabel("Player start his turn : " + GameElementFactory::getSuspectById(m_currentPlayerId)->getVisibleName());
    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(label);
    dialog->setLayout(layout);
    dialog->exec();
    dialog->deleteLater();
    layout->deleteLater();

    m_gameUi->playerStartTurn(playerId);
}

void GameUiLogic::playerEndTurn(int playerId)
{
    m_gameUi->playerEndTurn(playerId);
}

void GameUiLogic::suggestionEnd(int idPlayerWhoAnswered)
{
    if (idPlayerWhoAnswered < 0)
    {
        gameEnded(m_currentSuggestion, m_currentPlayerId);
    }
    else
    {
        GameElementSuspect *suspectWhoAnswered = GameElementFactory::getSuspectById(idPlayerWhoAnswered);
        m_gameUi->endSuggestion(suspectWhoAnswered);
    }
}

void GameUiLogic::createLocalGameSlot()
{
    m_gameLogic->startLocalGame();
}

void GameUiLogic::createRemoteGameSlot()
{
    m_gameLogic->createServer();
}

void GameUiLogic::joinRemoteGameSlot(const QString &ipAdresse, int port)
{
    m_gameLogic->connectToServeur(ipAdresse, port);
}

void GameUiLogic::connectionSuccess(int suspectId)
{
    m_charactersPlaying.clear();
    m_gameUi->connectionSuccess(suspectId);
    m_charactersPlaying.push_back(suspectId);
}

void GameUiLogic::connectionFailed()
{
    m_gameUi->connectionFailed();
}

void GameUiLogic::connectionServerLost()
{
    m_gameUi->connectionToServerLost();
}

void GameUiLogic::startGameSlot()
{
    m_gameLogic->requestStartGame();
}

void GameUiLogic::cancelLobbySlot()
{
    m_gameLogic->cancelLobby();
}

void GameUiLogic::aClientHasJoinServerSlot(int suspectId)
{
    m_gameUi->aClientHasJoinServer(suspectId);
    m_charactersPlaying.push_back(suspectId);
}

void GameUiLogic::aClientHasLeftServerSlot(int suspectId)
{
    m_gameUi->aClientHasLeftServer(suspectId);
    m_charactersPlaying.removeOne(suspectId);
}

void GameUiLogic::clientListSlot(QVector<int> suspectsIds)
{
    m_gameUi->clientList(suspectsIds);
    for (int suspectId : suspectsIds)
    {
        m_charactersPlaying.push_back(suspectId);
    }
}

void GameUiLogic::playerThrowDiceResult(int result)
{
    PathFinder *pathFinder = PathFinder::getPathFinderInstance(Board::getBoardInstance());

    GameElementRoom *roomCurrentlyIn = nullptr;
    QVector<GameElementRoom *> rooms = GameElementFactory::getRooms();
    Coordinate playerPos = PlayerDataForUi::s_playersData[m_currentPlayerId]->m_position;
    for (GameElementRoom *room : rooms)
    {
        if (room->getPosition() == playerPos)
        {
            roomCurrentlyIn = room;
        }
    }

    if (roomCurrentlyIn == nullptr)
    {
        QVector<Coordinate> reachableTile = pathFinder->getReachableCoordinates(playerPos, result);
        m_gameUi->enablePlayerMovementMod(m_currentPlayerId, playerPos, reachableTile);
    }
    else
    {
        for (const GameElementDoor &door : roomCurrentlyIn->getDoors())
        {
            QVector<Coordinate> reachableTile = pathFinder->getReachableCoordinates(door.m_position, result);
            m_gameUi->enablePlayerMovementMod(m_currentPlayerId, door.m_position, reachableTile);
        }
    }
}

void GameUiLogic::playerMoveToResult(const Coordinate &coord)
{
    PlayerDataForUi::s_playersData[m_currentPlayerId]->m_position = coord;
    QVector<GameElementRoom *> rooms = GameElementFactory::getRooms();
    Coordinate coordToMoveTo = coord;
    for (GameElementRoom *room : rooms)
    {
        if (room->getPosition() == coord)
        {
            coordToMoveTo = room->getSpot(m_currentPlayerId);
        }
    }
    m_gameUi->emitMoveToResult(coordToMoveTo);
}

void GameUiLogic::playerHasToAnswerSuggestion(int playerIdWhoMadeSuggestion, const Suggestion &suggestion, int playerIdWhoShouldAnswer, QVector<Card *> clues)
{
    GameElementSuspect *suspectWhoMadeSuggestion = GameElementFactory::getSuspectById(playerIdWhoMadeSuggestion);
    GameElementSuspect *suspectAnswering = GameElementFactory::getSuspectById(playerIdWhoShouldAnswer);

    GameElementRoom *room = GameElementFactory::getRoomById(suggestion.room);
    PlayerDataForUi::s_playersData[suggestion.murderer]->m_position = room->getPosition();
    m_gameUi->setPlayerPosition(room->getSpot(suggestion.murderer), suggestion.murderer);

    Player *localPlayer = findLocalPlayer(playerIdWhoShouldAnswer);
    m_playerHasToShowClueId = playerIdWhoShouldAnswer;

    if (localPlayer == nullptr)
    {
        m_gameUi->waitingForPlayerToShowClue(suspectWhoMadeSuggestion, suspectAnswering, suggestion);
    }
    else
    {
        m_gameUi->playerHasToShowClue(suspectWhoMadeSuggestion, suspectAnswering, suggestion, clues);
        connect(m_gameUi, SIGNAL(clueToShowSelected(int)), this, SLOT(emitShowClue(int)));
    }
}

void GameUiLogic::playerShowClue(const Suggestion &suggestion, int clue, int playerWhoAnswered)
{
    GameElementSuspect *suspectWhoMadeSuggestion = GameElementFactory::getSuspectById(m_currentPlayerId);
    GameElementSuspect *suspectWhoAnswered = GameElementFactory::getSuspectById(playerWhoAnswered);

    Card *card = Card::getCard(clue);

    m_gameUi->playerShowClue(suspectWhoAnswered, suspectWhoMadeSuggestion, suggestion, card);
}

void GameUiLogic::playerMakeSuggestion(const Suggestion &suggestion)
{
    m_currentSuggestion = suggestion;
}

void GameUiLogic::playerMakeAccusation(bool heIsRight, const Suggestion &suggestion)
{
    if (heIsRight == true)
    {
        gameEnded(suggestion, m_currentPlayerId);
    }
    else
    {
        QDialog *dialog = new QDialog();
        QString message;
        message.append("Player Made Accusation and Failed : " + GameElementFactory::getSuspectById(m_currentPlayerId)->getVisibleName());
        message.append("\n - Murderer : " + GameElementFactory::getSuspectById(suggestion.murderer)->getVisibleName());
        message.append("\n - Weapon : " + GameElementFactory::getWeaponById(suggestion.weapon)->getVisibleName());
        message.append("\n - Room : " + GameElementFactory::getRoomById(suggestion.room)->getVisibleName());
        dialog->setModal(true);
        QLabel *label = new QLabel(message);
        QVBoxLayout *layout = new QVBoxLayout();
        layout->addWidget(label);
        dialog->setLayout(layout);
        dialog->exec();
        dialog->deleteLater();
        layout->deleteLater();
    }
}

void GameUiLogic::emitSuggestion(const Suggestion &suggestion)
{
    Player *player = findLocalPlayer(m_currentPlayerId);
    if (player != nullptr)
    {
        GameElementRoom *room = GameElementFactory::getRoomById(suggestion.room);
        PlayerDataForUi::s_playersData[suggestion.murderer]->m_position = room->getPosition();
        m_gameUi->setPlayerPosition(room->getSpot(suggestion.murderer), suggestion.murderer);
        player->emitSuggestion(suggestion);
    }
}

void GameUiLogic::emitAccusation(const Suggestion &suggestion)
{
    Player *player = findLocalPlayer(m_currentPlayerId);
    if (player != nullptr)
    {
        player->emitAccusation(suggestion);
    }
}

void GameUiLogic::emitThrowDice()
{
    Player *player = findLocalPlayer(m_currentPlayerId);
    if (player != nullptr)
    {
        player->emitThrowDice();
    }
}

void GameUiLogic::emitEndTurn()
{
    Player *player = findLocalPlayer(m_currentPlayerId);
    if (player != nullptr)
    {
        player->emitEndTurn();
    }
}

void GameUiLogic::emitMoveTo(const Coordinate &coord)
{
    Player *player = findLocalPlayer(m_currentPlayerId);
    if (player != nullptr)
    {
        player->emitMoveTo(coord);
    }
}

void GameUiLogic::emitShowClue(int clueId)
{
    Player *player = findLocalPlayer(m_playerHasToShowClueId);
    if (player != nullptr)
    {
        disconnect(m_gameUi, SIGNAL(clueToShowSelected(int)), this, SLOT(emitShowClue(int)));
        player->emitShowClue(clueId);
    }
}

void GameUiLogic::disconnectToCurrentPlayerSignal()
{
    Player *player = findLocalPlayer(m_currentPlayerId);
    if (player != nullptr)
    {
        disconnect(m_gameUi, nullptr, player, nullptr);
    }
}

void GameUiLogic::connectToCurrentPlayerSignal()
{
    connect(m_gameUi, SIGNAL(requestThrowDiceSignal()), this, SLOT(emitThrowDice()));
    connect(m_gameUi, SIGNAL(requestEndTurn()), this, SLOT(emitEndTurn()));
    connect(m_gameUi, SIGNAL(playerMovedTo(Coordinate)), this, SLOT(emitMoveTo(Coordinate)));
    connect(m_gameUi, SIGNAL(playerSuggestion(Suggestion)), this, SLOT(emitSuggestion(Suggestion)));
    connect(m_gameUi, SIGNAL(playerAccusation(Suggestion)), this, SLOT(emitAccusation(Suggestion)));
    connect(m_gameUi, SIGNAL(clueToShowSelected(int)), this, SLOT(emitShowClue(int)));
}

void GameUiLogic::connectToLocalPlayerSignal(Player *player)
{
    connect(player, SIGNAL(playerStartedTurnSignal(int)), this, SLOT(playerStartTurn(int)));
    connect(player, SIGNAL(playerEndTurnSignal(int)), this, SLOT(playerEndTurn(int)));
    connect(player, SIGNAL(playerThrownDiceSignal(int)), this, SLOT(playerThrowDiceResult(int)));
    connect(player, SIGNAL(playerMoveToResultSignal(Coordinate)), this, SLOT(playerMoveToResult(Coordinate)));
    connect(player, SIGNAL(playerShownClue(Suggestion, int, int)), this, SLOT(playerShowClue(Suggestion, int, int)));
}

void GameUiLogic::connectToGameLogic()
{
    connect(m_gameLogic, SIGNAL(playerStartedTurnSignal(int)), this, SLOT(playerStartTurn(int)));
    connect(m_gameLogic, SIGNAL(playerEndTurnSignal(int)), this, SLOT(playerEndTurn(int)));
    connect(m_gameLogic, SIGNAL(localGameStarted(QVector<Player *>)), this, SLOT(localGameStarted(QVector<Player *>)));
    connect(m_gameLogic, SIGNAL(playerThrownDiceSignal(int)), this, SLOT(playerThrowDiceResult(int)));
    connect(m_gameLogic, SIGNAL(playerMoveToResultSignal(Coordinate)), this, SLOT(playerMoveToResult(Coordinate)));
    connect(m_gameLogic, SIGNAL(playerShownClue(Suggestion, int, int)), this, SLOT(playerShowClue(Suggestion, int, int)));
    connect(m_gameLogic, SIGNAL(suggestionEnd(int)), this, SLOT(suggestionEnd(int)));
    connect(m_gameLogic, SIGNAL(hasToAnswerSuggestion(int,Suggestion,int,QVector<Card *>)), this, SLOT(playerHasToAnswerSuggestion(int,Suggestion,int,QVector<Card *>)));
    connect(m_gameLogic, SIGNAL(playerMakeSuggestionSignal(Suggestion)), this, SLOT(playerMakeSuggestion(Suggestion)));
    connect(m_gameLogic, SIGNAL(playerMakeAccusationSignal(bool, Suggestion)), this, SLOT(playerMakeAccusation(bool, Suggestion)));
}

void GameUiLogic::gameEnded(const Suggestion &suggestion, int playerIdWhoWon)
{
    QDialog *dialog = new QDialog();
    QString message;
    message.append("Player Won : " + GameElementFactory::getSuspectById(playerIdWhoWon)->getVisibleName());
    message.append("\n - Murderer : " + GameElementFactory::getSuspectById(suggestion.murderer)->getVisibleName());
    message.append("\n - Weapon : " + GameElementFactory::getWeaponById(suggestion.weapon)->getVisibleName());
    message.append("\n - Room : " + GameElementFactory::getRoomById(suggestion.room)->getVisibleName());
    dialog->setModal(true);
    QLabel *label = new QLabel(message);
    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(label);
    dialog->setLayout(layout);
    dialog->exec();
    dialog->deleteLater();
    layout->deleteLater();

    m_gameUi->gameEnded();

    m_localPlayers.clear();
    m_charactersPlaying.clear();
}

Player *GameUiLogic::findLocalPlayer(int playerId)
{
    Player *player = nullptr;
    auto iter = std::find_if(m_localPlayers.begin(), m_localPlayers.end(), [playerId](Player *player) { return playerId == player->getCharacterId() ;});
    if (iter != m_localPlayers.end())
    {
        player = *iter;
    }

    return player;
}

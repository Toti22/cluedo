#include "portraitUi.h"

#include <QGridLayout>

PortraitUi::PortraitUi(const QSize &size, QWidget *parent)
    : m_size(size), QWidget{parent}
{
    this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    m_label.setMaximumSize(size);
    m_label.setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    QGridLayout *layout = new QGridLayout();
    QWidget *spacerRight = new QWidget();
    spacerRight->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    QWidget *spacerDown = new QWidget();
    spacerDown->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    QWidget *spacerLeft = new QWidget();
    spacerLeft->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    layout->addWidget(spacerLeft, 0, 0, 1, 1);
    layout->addWidget(&m_label, 0, 1, 1, 1);
    layout->addWidget(spacerRight, 0, 2, 1, 1);
    //layout->addWidget(spacerDown, 1, 0, 1, 3);

    m_label.setPixmap(QPixmap(":/onePixel.png").scaled(m_size));
    setLayout(layout);

}

void PortraitUi::setPortrait(const QPixmap &pixmap)
{
    m_label.setPixmap(pixmap.scaled(m_size));
}

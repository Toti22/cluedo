#ifndef GameUiSceneAccusation_H
#define GameUiSceneAccusation_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QVector>
#include <QObject>
#include <QGraphicsProxyWidget>
#include <QPushButton>

#include "core/gameLogic/card.h"
#include "core/gameLogic/suggestion.h"
#include "core/gameLogic/gameElement/gameElementSuspect.h"
#include "selectionableCard.h"

class GameUiSceneAccusation : public QObject
{
    Q_OBJECT
public:
    explicit GameUiSceneAccusation(QGraphicsScene *scene);
    void requestShowAccusationMenu();

    void clearScene();
protected:
    QVector<QGraphicsItem *> m_accusationMenuItems;
    QGraphicsScene *m_scene;

    QVector<SelectionableCard *> m_selectableSuspectCards;
    QVector<SelectionableCard *> m_selectableWeaponsCards;
    QVector<SelectionableCard *> m_selectableRoomCards;

    SelectionableCard *m_suspectCardSelected;
    SelectionableCard *m_weaponCardSelected;
    SelectionableCard *m_roomCardSelected;
    QGraphicsProxyWidget *m_validateAccusationButton;
    QGraphicsProxyWidget *m_cancelAccusationButton;

    QPushButton m_pushButtonOk;

protected slots:
    void cardSelected(SelectionableCard *cardUi);
    void cardUnSelected(SelectionableCard *cardUi);

    void validateAccusationSlot();
    void cancelAccusationSlot();

signals:
    void validateAccusation(Suggestion);
    void cancelSignal();
};

#endif // GameUiSceneAccusation_H

#ifndef GAMEUISCENERESULTSUGGESTION_H
#define GAMEUISCENERESULTSUGGESTION_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QVector>
#include <QObject>
#include <QPushButton>

#include "core/gameLogic/card.h"
#include "core/gameLogic/suggestion.h"
#include "core/gameLogic/gameElement/gameElementSuspect.h"
#include "selectionableCard.h"

class GameUiSceneResultSuggestion : public QObject
{
    Q_OBJECT
public:
    GameUiSceneResultSuggestion(QGraphicsScene *scene);

    void clearScene();
    void showSuggestion(GameElementSuspect *suspectWhoAnswered, GameElementSuspect *suspectWhoAsked, const Suggestion &suggestion, Card *clueShown);

signals:
    void accepted();
protected:
    QVector<QGraphicsItem *> m_items;
    QGraphicsScene *m_scene;

    void createBackGroundItem();

protected slots:
    void acceptedSlot();
};

#endif // GAMEUISCENERESULTSUGGESTION_H

#ifndef MAINMENU_H
#define MAINMENU_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGridLayout>
#include <QPushButton>

class MainMenu : public QWidget
{
    Q_OBJECT
public:
    explicit MainMenu(QGraphicsScene *scene);

    void clearScene();
    void showMenu();

protected slots:
    void createLocalGameSlot();
    void createHostGameSlot();
    void joinGameSlot();

protected:
    QGraphicsScene *m_scene;
    QVector<QGraphicsItem *> m_items;

    void createBackGroundItem();
    QWidget *createMenu();

signals:
    void createLocalGame();
    void createHostGame();
    void joinGame();
};

#endif // MAINMENU_H

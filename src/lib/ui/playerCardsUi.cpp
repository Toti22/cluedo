#include "playerCardsUi.h"

#include <QLabel>

PlayerCardsUi::PlayerCardsUi(const QSize &size, QWidget *parent)
    : m_size(size), QWidget{parent}, m_layout(nullptr)
{
    setMinimumHeight(320);
    m_layout = new QGridLayout();
    setLayout(m_layout);
}

void PlayerCardsUi::loadCards(QVector<Card *> cards)
{
    QLayoutItem *item;
    while ((item = m_layout->takeAt(0)) != 0)
    {
        m_layout->removeItem (item);
    }
    for (QWidget *widget : m_widgets)
    {
        widget->deleteLater();
    }
    m_widgets.clear();

    int nbWidgetPerRow = 3;

    QWidget *spacer = new QWidget;
    m_widgets.push_back(spacer);
    spacer->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    //layout->addWidget(spacer, 0, 0 , 1, nbWidgetPerRow);

    for (int i = 0 ; i < cards.size() ; ++i)
    {
        Card *card = cards.at(i);
        QLabel *label = new QLabel();
        m_widgets.push_back(label);
        label->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        label->setPixmap(card->getPixmap().scaled(m_size));
        m_layout->addWidget(label, (i / nbWidgetPerRow) + 1, i % nbWidgetPerRow, Qt::AlignCenter);
    }

    spacer = new QWidget;
    m_widgets.push_back(spacer);
    spacer->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    //layout->addWidget(spacer, cards.size() + 1, 0, 1, nbWidgetPerRow);
}

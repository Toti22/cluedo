#ifndef SELECTIONABLECARD_H
#define SELECTIONABLECARD_H

#include <QLabel>
#include <QPixmap>

class SelectionableCard : public QLabel
{
    Q_OBJECT
public:
    SelectionableCard(int cardId, const QPixmap &pixmap);

    void setSelectected(bool selected);

    // QWidget interface
    int getCardId() const;

    bool isSelected() const;

protected:
    int m_cardId;
    bool m_isSelected;
    bool m_mouseIsOver;
    QPixmap m_defaultPixmap;
    QPixmap m_hoverPixmap;
    QPixmap m_selectedPixmap;

    void mousePressEvent(QMouseEvent *event);
    void enterEvent(QEnterEvent *event);
    void leaveEvent(QEvent *event);

signals:
    void selected(SelectionableCard *);
    void unSelected(SelectionableCard *);
};

inline int SelectionableCard::getCardId() const
{
    return m_cardId;
}

inline bool SelectionableCard::isSelected() const
{
    return m_isSelected;
}

#endif // SELECTIONABLECARD_H

#include "playerDataForUi.h"

QMap<int, PlayerDataForUi*> PlayerDataForUi::s_playersData;

PlayerDataForUi::PlayerDataForUi(GameElementSuspect *suspect)
    : m_localHumanPlayer(false), m_characterId(suspect->getId()),
    m_position(suspect->getPosition()), m_clueSheetData(nullptr)
{

}

PlayerDataForUi::PlayerDataForUi(Player *player)
    : m_localHumanPlayer(player->isLocalHumanPlayer()), m_characterId(player->getCharacterId()),
    m_cards(player->getCards()), m_position(player->getPosition()), m_clueSheetData(player->getClueSheetData())
{
}

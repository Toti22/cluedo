#include "gameUI.h"

#include <QGridLayout>
#include <QKeyEvent>
#include <QGraphicsProxyWidget>
#include <QGraphicsPixmapItem>
#include <QTransform>
#include <QPushButton>
#include <QPushButton>

#include "../core/board/BoardCaracteristics.h"
#include "core/players/player.h"
#include "core/pathFinder.h"
#include "boardui.h"
#include "selectionableCard.h"
#include "core/gameLogic/gameElement/gameElementFactory.h"
#include "playerDataForUi.h"

GameUi::GameUi(QWidget *parent)
    : QWidget{parent}, m_lastArrowKeyPressed(0), m_playerPortrait(QSize(200, 200)), m_playerCardsUi(QSize(100, 150)),
    m_currentAction(PlayerAction::NONE), m_gameUiSceneClueToShow(&m_boardUi),
    m_gameUiSceneResultSuggestion(&m_boardUi), m_gameUiSceneSuggestion(&m_boardUi), m_gameUiSceneAccusation(&m_boardUi),
    m_mainMenu(&m_boardUi), m_gameUiSceneWaitingForPLayerToAnswerSuggestion(&m_boardUi),
    m_gameLobbyUi(&m_boardUi), m_joinServerUi(&m_boardUi)
{
    m_layout = new QGridLayout;
    m_layout->addWidget(&m_playerPortrait, 0, 0);

    QWidget *spacer = new QWidget;
    spacer->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    m_layout->addWidget(spacer, 1, 0);

    m_layout->addWidget(&m_playerCardsUi, 2, 0);
    m_playerCardsUi.setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);

    spacer = new QWidget;
    spacer->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    m_layout->addWidget(spacer, 3, 0);
    m_layout->addWidget(m_boardUi.getGraphicsView(), 0, 1, 5, 1);
    m_boardUi.getGraphicsView()->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_layout->addLayout(m_clueSheet.getLayout(), 0, 2);

    QPushButton *pushButtonDice = new QPushButton("Throw Dice");
    pushButtonDice->setStyleSheet("font-size:20px");
    pushButtonDice->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    QPushButton *pushButtonPassage = new QPushButton("Take secret passage");
    pushButtonPassage->setStyleSheet("font-size:20px");
    pushButtonPassage->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    QPushButton *pushButtonSuggestion = new QPushButton("Make suggestion");
    pushButtonSuggestion->setStyleSheet("font-size:20px");
    pushButtonSuggestion->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    QPushButton *pushButtonAccusation = new QPushButton("Make Accusation");
    pushButtonAccusation->setStyleSheet("font-size:20px");
    pushButtonAccusation->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    QPushButton *pushButtonEndTurn = new QPushButton("End Turn");
    pushButtonEndTurn->setStyleSheet("font-size:20px");
    pushButtonEndTurn->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    m_playerActionLayout.addWidget(pushButtonDice, 0, 0);
    m_playerActionLayout.addWidget(pushButtonPassage, 0, 1);
    m_playerActionLayout.addWidget(pushButtonSuggestion, 1, 0);
    m_playerActionLayout.addWidget(pushButtonAccusation, 1, 1);
    m_playerActionLayout.addWidget(pushButtonEndTurn, 2, 0, 1, 2);

    m_playerActionButtons.push_back(pushButtonDice);
    m_playerActionButtons.push_back(pushButtonPassage);
    m_playerActionButtons.push_back(pushButtonSuggestion);
    m_playerActionButtons.push_back(pushButtonAccusation);
    m_playerActionButtons.push_back(pushButtonEndTurn);

    m_layout->addLayout(&m_playerActionLayout, 4, 0);


    connect(pushButtonDice, SIGNAL(pressed()), this, SLOT(requestThrowDice()));
    connect(&m_boardUi, SIGNAL(playerMovedTo(Coordinate)), this, SLOT(playerMovedToSlot(Coordinate)));
    connect(this, SIGNAL(playerMovedToResult(Coordinate)), &m_boardUi, SLOT(playerMoveToResult(Coordinate)));
    connect(this, SIGNAL(playerStartTurnSignal(int)), &m_boardUi, SLOT(playerStartTurn(int)));
    connect(pushButtonEndTurn, SIGNAL(pressed()), this, SIGNAL(requestEndTurn()));
    connect(pushButtonSuggestion, SIGNAL(pressed()), this, SLOT(requestShowSuggestionMenu()));
    connect(pushButtonAccusation, SIGNAL(pressed()), this, SLOT(requestShowAccusationMenu()));
    connect(&m_gameUiSceneClueToShow, SIGNAL(clueSelected(int)), this, SLOT(clueToShowSelectedSlot(int)));
    connect(&m_gameUiSceneSuggestion, SIGNAL(validateSuggestion(Suggestion)), this, SLOT(validateSuggestion(Suggestion)));
    connect(&m_gameUiSceneAccusation, SIGNAL(validateAccusation(Suggestion)), this, SLOT(validateAccusation(Suggestion)));
    connect(&m_gameUiSceneAccusation, SIGNAL(cancelSignal()), this, SLOT(cancelAccusation()));

    connect(&m_gameLobbyUi, SIGNAL(cancelLobby()), this, SLOT(cancelLobbySlot()));
    connect(&m_gameLobbyUi, SIGNAL(startGame()), this, SLOT(startGameSlot()));

    connect(&m_mainMenu, SIGNAL(createLocalGame()), this, SLOT(createLocalGameSlot()));
    connect(&m_mainMenu, SIGNAL(createHostGame()), this, SLOT(createRemoteGameSlot()));
    connect(&m_mainMenu, SIGNAL(joinGame()), this, SLOT(openJoinServerUi()));

    connect(&m_joinServerUi, SIGNAL(joinServer(QString,int)), this, SLOT(joinRemoteGameSlot(QString,int)));
    connect(&m_joinServerUi, SIGNAL(cancel()), this, SLOT(cancelJoinServer()));

    setLayout(m_layout);
    setFocus();
}

void GameUi::enablePlayerMovementMod(int idCurrentPlayerPlaying, const Coordinate &playerLocation, const QVector<Coordinate> &coordinates)
{
    m_currentAction = PlayerAction::MOVING;
    m_boardUi.enablePlayerMovementMod(idCurrentPlayerPlaying, playerLocation, coordinates);
}

void GameUi::emitMoveToResult(const Coordinate &coordinate)
{
    emit playerMovedToResult(coordinate);
}

void GameUi::connectionSuccess(int suspectId)
{
    m_gameLobbyUi.showMenu(m_hostMod);
    m_gameLobbyUi.addSuspect(suspectId);
}

void GameUi::connectionFailed()
{
    m_mainMenu.showMenu();
}

void GameUi::aClientHasJoinServer(int suspectId)
{
    m_gameLobbyUi.addSuspect(suspectId);
}

void GameUi::aClientHasLeftServer(int suspectId)
{
    m_gameLobbyUi.removeSuspect(suspectId);
}

void GameUi::clientList(QVector<int> suspectsIds)
{
    m_gameLobbyUi.addSuspects(suspectsIds);
}

void GameUi::connectionToServerLost()
{
    clearInterfaces();
    m_mainMenu.showMenu();
}

void GameUi::gameEnded()
{
    clearInterfaces();
    m_boardUi.clearBoard();
    showMainMenu();
}

void GameUi::showMainMenu()
{
    m_mainMenu.showMenu();
    this->showMaximized();
}

void GameUi::gameStarted()
{
    clearInterfaces();

    m_clueSheet.init();
    m_boardUi.init();
    this->showMaximized();
}

void GameUi::playerStartTurn(int playerId)
{
    emit playerStartTurnSignal(playerId);
    clearInterfaces();
    m_currentPlayerId = playerId;
    initPlayerUi(playerId);
}

void GameUi::playerEndTurn(int playerId)
{
    clearInterfaces();
    m_boardUi.playerEndTurn();

    m_currentAction = PlayerAction::NONE;
}

void GameUi::playerMovedToSlot(const Coordinate &coordinate)
{
    m_currentAction = PlayerAction::NONE;
    emit playerMovedTo(coordinate);
}

void GameUi::playerHasToShowClue(GameElementSuspect *suspectWhoMadeSuggestion, GameElementSuspect *suspectAnswering, const Suggestion &suggestion, QVector<Card *> cluesInPLayerPossession)
{
    clearInterfaces();

    m_gameUiSceneClueToShow.showSuggestion(suspectWhoMadeSuggestion, suspectAnswering, suggestion, cluesInPLayerPossession);
}

void GameUi::waitingForPlayerToShowClue(GameElementSuspect *suspectWhoMadeSuggestion, GameElementSuspect *suspectAnswering, const Suggestion &suggestion)
{
    clearInterfaces();
    m_gameUiSceneWaitingForPLayerToAnswerSuggestion.showScene(suspectWhoMadeSuggestion, suspectAnswering, suggestion);
}

void GameUi::playerShowClue(GameElementSuspect *suspectWhoAnswered, GameElementSuspect *suspectWhoSuggest, const Suggestion &suggestion, Card *clue)
{
    clearInterfaces();

    m_gameUiSceneResultSuggestion.showSuggestion(suspectWhoAnswered, suspectWhoSuggest, suggestion, clue);
}

void GameUi::setPlayerPosition(const Coordinate &coord, int suspectId)
{
    m_boardUi.setPlayerPosition(coord, suspectId);
}

void GameUi::endSuggestion(GameElementSuspect *suspectWhoAnswered)
{
    clearInterfaces();
}

void GameUi::requestThrowDice()
{
    if (m_currentAction == PlayerAction::NONE)
    {
        emit requestThrowDiceSignal();
    }
}

void GameUi::requestTakeSecretPassage()
{

}

void GameUi::requestShowSuggestionMenu()
{
    if (m_currentAction != PlayerAction::NONE)
    {
        return;
    }

    QVector<GameElementRoom *> rooms = GameElementFactory::getRooms();
    GameElementRoom *roomPlayerIn = nullptr;
    for (GameElementRoom *room : rooms)
    {
        PlayerDataForUi *data = PlayerDataForUi::s_playersData[m_currentPlayerId];
        if (data->m_position == room->getPosition())
        {
            roomPlayerIn = room;
        }
    }
    if (roomPlayerIn == nullptr)
    {
        return;
    }
    m_currentAction = PlayerAction::SUGGESTION;

    m_gameUiSceneSuggestion.requestShowSuggestionMenu(Card::getCard(roomPlayerIn->getId()));
}

void GameUi::requestShowAccusationMenu()
{
    m_currentAction = PlayerAction::ACCUSATION;
    m_gameUiSceneAccusation.requestShowAccusationMenu();
}

void GameUi::clueToShowSelectedSlot(int clue)
{
    emit clueToShowSelected(clue);
}

void GameUi::validateSuggestion(const Suggestion &suggestion)
{
    m_currentAction = PlayerAction::NONE;
    m_gameUiSceneSuggestion.clearScene();
    emit playerSuggestion(suggestion);
}

void GameUi::validateAccusation(const Suggestion &suggestion)
{
    m_currentAction = PlayerAction::NONE;
    m_gameUiSceneAccusation.clearScene();
    emit playerAccusation(suggestion);
}

void GameUi::cancelAccusation()
{
    m_currentAction = PlayerAction::NONE;
    m_gameUiSceneAccusation.clearScene();
}

void GameUi::createLocalGameSlot()
{
    emit createLocalGame();
}

void GameUi::createRemoteGameSlot()
{
    m_hostMod = true;
    emit createRemoteGame();
}

void GameUi::joinRemoteGameSlot(QString ip, int port)
{
    m_hostMod = false;
    emit joinRemoteGame(ip, port);
}

void GameUi::cancelLobbySlot()
{
    emit cancelLobby();
    m_mainMenu.showMenu();
}

void GameUi::startGameSlot()
{
    emit startGame();
}

void GameUi::openJoinServerUi()
{
    clearInterfaces();
    m_joinServerUi.showMenu();
}

void GameUi::cancelJoinServer()
{
    clearInterfaces();
    m_mainMenu.showMenu();
}

void GameUi::initPlayerUi(int playerId)
{
    PlayerDataForUi *player = PlayerDataForUi::s_playersData[playerId];

    if (player->m_localHumanPlayer == true)
    {
        m_clueSheet.loadClueSheetData(player->m_clueSheetData);
        m_playerCardsUi.loadCards(player->m_cards);
        for (QPushButton *button : m_playerActionButtons)
        {
            button->show();
        }
        GameElementSuspect *suspect = GameElementFactory::getSuspectById(player->m_characterId);
        m_playerPortrait.setPortrait(suspect->getPortrait());
    }
    else
    {
        for (QPushButton *button : m_playerActionButtons)
        {
            button->hide();
        }
    }

}

void GameUi::clearInterfaces()
{
    m_gameUiSceneSuggestion.clearScene();
    m_gameUiSceneResultSuggestion.clearScene();
    m_gameUiSceneClueToShow.clearScene();
    m_gameUiSceneAccusation.clearScene();
    m_gameUiSceneWaitingForPLayerToAnswerSuggestion.clearScene();
}

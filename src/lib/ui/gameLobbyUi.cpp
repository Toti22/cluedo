#include "gameLobbyUi.h"

#include <QPainter>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QBrush>
#include <QRect>
#include <QGraphicsProxyWidget>
#include <QPalette>

#include "core/board/BoardCaracteristics.h"
#include "core/gameLogic/gameElement/gameElementFactory.h"

GameLobbyUi::GameLobbyUi(QGraphicsScene *scene)
    : m_scene(scene), m_layout(nullptr)
{
}

void GameLobbyUi::clearScene()
{
    for (QGraphicsItem *item : m_items)
    {
        m_scene->removeItem(item);
    }
    m_items.clear();
    m_portraitsItems.clear();
    m_portraits.clear();
}

void GameLobbyUi::showMenu(bool hostMod)
{
    clearScene();
    createBackGroundItem();
    m_items.push_back(m_scene->addWidget(createMenu(hostMod)));
}

void GameLobbyUi::addSuspect(int id)
{
    GameElementSuspect *suspect = GameElementFactory::getSuspectById(id);
    if (suspect == nullptr)
    {
        return;
    }

    if (m_layout == nullptr)
    {
        m_layout = new QGridLayout();
    }

    bool suspectAdded = false;
    for (int column = 1 ; column < 4 ; ++column)
    {
        for (int row = 1 ; row < 3 ; ++row)
        {
            if (m_layout->itemAtPosition(row, column) == nullptr)
            {
                suspectAdded = true;
                QLabel *label = new QLabel();
                m_layout->addWidget(label, row, column);
                m_portraits.push_back(label);
                label->setAccessibleName(suspect->getName());
                label->setPixmap(suspect->getPortrait().scaled(100, 100));

                QGraphicsProxyWidget *item = m_scene->addWidget(label);
                m_items.push_back(item);
                m_portraitsItems.insert(suspect->getId(), item);
                break;
            }
        }

        if (suspectAdded == true)
        {
            break;
        }
    }
}

void GameLobbyUi::addSuspects(QVector<int> ids)
{
    for (int id : ids)
    {
        addSuspect(id);
    }
}

void GameLobbyUi::removeSuspect(int id)
{
    if (m_layout == nullptr)
    {
        m_layout = new QGridLayout();
    }

    GameElementSuspect *suspect = GameElementFactory::getSuspectById(id);
    if (suspect == nullptr)
    {
        return;
    }

    for (int i = 0 ; i < m_portraits.size() ; ++i)
    {
        QLabel *label = m_portraits.at(i);
        if (label->accessibleName() == suspect->getName())
        {
            m_portraits.removeAt(i);
            m_layout->removeWidget(label);
            label->hide();
        }
    }
}

void GameLobbyUi::startGameSlot()
{
    clearScene();
    emit startGame();
}

void GameLobbyUi::cancelSlot()
{
    clearScene();
    emit cancelLobby();
}

void GameLobbyUi::createBackGroundItem()
{
    int xSize = BOARD_X_SIZE * TILE_SIZE;
    int ySize = BOARD_Y_SIZE * TILE_SIZE;

    QRect rect(0, 0, xSize, ySize);

    QBrush blackBrush(Qt::GlobalColor::black);

    QPixmap pixmap(":/onePixel.png");
    pixmap = pixmap.scaled(rect.width(), rect.height(), Qt::IgnoreAspectRatio);
    QPainter painter(&pixmap);
    painter.fillRect(0, 0, xSize, ySize, blackBrush);

    QGraphicsPixmapItem *itemBackground = m_scene->addPixmap(pixmap);
    m_items.push_back(itemBackground);
    itemBackground->setPos(0, 0);
    itemBackground->setOpacity(1);
    itemBackground->setZValue(0);
}

QWidget *GameLobbyUi::createMenu(bool hostMod)
{
    int xSize = BOARD_X_SIZE * TILE_SIZE;
    int ySize = BOARD_Y_SIZE * TILE_SIZE;

    QWidget *widget = new QWidget();
    widget->setFixedSize(xSize, ySize);

    widget->setAutoFillBackground(true);
    widget->setStyleSheet("font-size: 48px;background-color: rgba(255, 255, 255, 0);");

    QPushButton *buttonStartGame = new QPushButton();
    buttonStartGame->setAutoFillBackground(true);
    buttonStartGame->setText(tr("Start Game"));
    buttonStartGame->setStyleSheet("font-size: 48px;background-color: rgba(255, 255, 255, 255);");

    QPushButton *buttonCancel = new QPushButton();
    buttonCancel->setAutoFillBackground(true);
    buttonCancel->setText(tr("Cancel"));
    buttonCancel->setStyleSheet("font-size: 48px;background-color: rgba(255, 255, 255, 255);");

    QWidget *upperSpacer = new QWidget();
    QWidget *downSpacer = new QWidget();
    QWidget *leftSpacer = new QWidget();
    QWidget *rightSpacer = new QWidget();
    upperSpacer->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    downSpacer->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    leftSpacer->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    rightSpacer->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    m_layout = new QGridLayout();
    m_layout->addWidget(upperSpacer, 0, 0, 1, 5);
    m_layout->addWidget(downSpacer, 3, 0, 1, 5);
    m_layout->addWidget(leftSpacer, 1, 0, 3, 1);
    m_layout->addWidget(rightSpacer, 1, 4, 3, 1);

    if (hostMod)
    {
        m_layout->addWidget(buttonStartGame, 4, 1, 1, 1);
        m_layout->addWidget(buttonCancel, 4, 3, 1, 1);
    }
    else
    {
        buttonStartGame->hide();
        m_layout->addWidget(buttonCancel, 4, 2, 1, 1);
    }

    widget->setLayout(m_layout);

    connect(buttonStartGame, SIGNAL(pressed()), this, SLOT(startGameSlot()));
    connect(buttonCancel, SIGNAL(pressed()), this, SLOT(cancelSlot()));

    return widget;

}

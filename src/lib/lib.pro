TEMPLATE = subdirs

SUBDIRS += \
    core \
    ui

ui.depends = core

QMAKE_EXTRA_TARGETS += deploy
deploy.target = deploy

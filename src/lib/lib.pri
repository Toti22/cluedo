TEMPLATE = lib
DESTDIR = $$(CLUEDODIR)/lib
DEFINES += QT_DEPRECATED_WARNINGS
CONFIG += c++17 staticlib

CONFIG(debug, debug|release):TARGET = $$join(TARGET,,,d)

CONFIG(testcase) {
	CONFIG -= testcase
}

include($$(CLUEDODIR)/deploy.pri)
#include "pathFinder.h"

#include <QDebug>
#include <QTextStream>
#include <QPair>

#include "board/BoardCaracteristics.h"

QMap<Board *, PathFinder *> PathFinder::s_pathFinders;

PathFinder::PathFinder(Board *board)
    : m_board(board)
{

}

PathFinder *PathFinder::getPathFinderInstance(Board *board)
{
    auto iter = s_pathFinders.find(board);
    PathFinder * pathFinder = nullptr;
    if (iter == s_pathFinders.end())
    {
        pathFinder = new PathFinder(board);
        s_pathFinders.insert(board, pathFinder);
    }
    else
    {
        pathFinder = iter.value();
    }
    return pathFinder;
}

void PathFinder::freePathFinderInstances()
{
    for (auto iter = s_pathFinders.begin() ; iter != s_pathFinders.end() ; ++iter)
    {
        if (iter.value() != nullptr)
        {
            delete iter.value();
        }
    }
    s_pathFinders.clear();
}

QVector<Direction> PathFinder::findShortestPath(const Coordinate &startingPosition, const Coordinate &endingPosition)
{
    QVector<Direction> fatestPathFound;
    QVector<QVector<int>> eachTileWeight = getEachTileWeight(startingPosition);

    recursiveFastestSearch(*m_board, startingPosition, endingPosition, eachTileWeight, QVector<Direction>(), fatestPathFound);

    return fatestPathFound;
}

QVector<QVector<int> > PathFinder::getEachTileWeight(const Coordinate &position)
{
    if (m_eachTileWeightMap.contains(position))
    {
        return m_eachTileWeightMap[position];
    }

    QVector<QVector<int>> eachTileWeight;
    for (int x = 0 ; x < m_board->getBoard().size() ; ++x)
    {
        QVector<int> line;
        for (int y = 0 ; y < m_board->getBoard().at(x).size() ; ++y)
        {
            line.push_back(INT_MAX);
        }
        eachTileWeight.push_back(line);
    }
    eachTileWeight[position.x][position.y] = 0;

    QVector<QPair<Coordinate, int>> coordinatesToCheck;

    if (position.y > 0 && !m_board->getBoard().at(position.x).at(position.y)->isUpColissionOn())
    {
        coordinatesToCheck.push_back(QPair<Coordinate, int>(Coordinate(position.x, position.y - 1), 1));
        eachTileWeight[position.x][position.y - 1] = 1;
    }
    if (position.x < BOARD_X_SIZE && !m_board->getBoard().at(position.x).at(position.y)->isRightColissionOn())
    {
        coordinatesToCheck.push_back(QPair<Coordinate, int>(Coordinate(position.x + 1, position.y), 1));
        eachTileWeight[position.x + 1][position.y] = 1;
    }
    if (position.y < BOARD_Y_SIZE && !m_board->getBoard().at(position.x).at(position.y)->isDownColissionOn())
    {
        coordinatesToCheck.push_back(QPair<Coordinate, int>(Coordinate(position.x, position.y + 1), 1));
        eachTileWeight[position.x][position.y + 1] = 1;
    }
    if (position.x > 0 && !m_board->getBoard().at(position.x).at(position.y)->isLeftColissionOn())
    {
        coordinatesToCheck.push_back(QPair<Coordinate, int>(Coordinate(position.x - 1, position.y), 1));
        eachTileWeight[position.x - 1][position.y] = 1;
    }

    while (!coordinatesToCheck.empty())
    {
        QPair<Coordinate, int> pair = coordinatesToCheck.first();
        Coordinate coordinateToCheck = pair.first;
        int weight = pair.second + 1;
        coordinatesToCheck.pop_front();

        if (coordinateToCheck.y > 0 && !m_board->getBoard().at(coordinateToCheck.x).at(coordinateToCheck.y)->isUpColissionOn() &&
            eachTileWeight[coordinateToCheck.x][coordinateToCheck.y - 1] > weight
            )
        {
            coordinatesToCheck.push_back(QPair<Coordinate, int>(Coordinate(coordinateToCheck.x, coordinateToCheck.y - 1), weight));
            eachTileWeight[coordinateToCheck.x][coordinateToCheck.y - 1] = weight;
        }
        if (coordinateToCheck.x < BOARD_X_SIZE && !m_board->getBoard().at(coordinateToCheck.x).at(coordinateToCheck.y)->isRightColissionOn() &&
            eachTileWeight[coordinateToCheck.x + 1][coordinateToCheck.y] > weight
            )
        {
            coordinatesToCheck.push_back(QPair<Coordinate, int>(Coordinate(coordinateToCheck.x + 1, coordinateToCheck.y), weight));
            eachTileWeight[coordinateToCheck.x + 1][coordinateToCheck.y] = weight;
        }
        if (coordinateToCheck.y < BOARD_Y_SIZE && !m_board->getBoard().at(coordinateToCheck.x).at(coordinateToCheck.y)->isDownColissionOn() &&
            eachTileWeight[coordinateToCheck.x][coordinateToCheck.y + 1] > weight
            )
        {
            coordinatesToCheck.push_back(QPair<Coordinate, int>(Coordinate(coordinateToCheck.x, coordinateToCheck.y + 1), weight));
            eachTileWeight[coordinateToCheck.x][coordinateToCheck.y + 1] = weight;
        }
        if (coordinateToCheck.x > 0 && !m_board->getBoard().at(coordinateToCheck.x).at(coordinateToCheck.y)->isLeftColissionOn() &&
            eachTileWeight[coordinateToCheck.x - 1][coordinateToCheck.y] > weight
            )
        {
            coordinatesToCheck.push_back(QPair<Coordinate, int>(Coordinate(coordinateToCheck.x - 1, coordinateToCheck.y), weight));
            eachTileWeight[coordinateToCheck.x - 1][coordinateToCheck.y] = weight;
        }
    }

    m_eachTileWeightMap.insert(position, eachTileWeight);
    return eachTileWeight;
}

QVector<Coordinate> PathFinder::getReachableCoordinates(const Coordinate &position, int depth)
{
    QVector<Coordinate> reachableCoordinates;

    QVector<QVector<int>> eachTileWeight = getEachTileWeight(position);
    for (int x = 0 ; x < eachTileWeight.size() ; ++x)
    {
        for (int y = 0 ; y < eachTileWeight.at(x).size() ; ++y)
        {
            if(eachTileWeight.at(x).at(y) <= depth)
            {
                reachableCoordinates.push_back(Coordinate(x, y));
            }
        }
    }
    return reachableCoordinates;
}

void PathFinder::recursiveFastestSearch(const Board &board, Coordinate currentPosition, const Coordinate &endingPosition, const QVector<QVector<int> > &eachTileWeight,
                                        QVector<Direction> currentPath, QVector<Direction> &foundPath)
{
    if (currentPosition == endingPosition)
    {
        foundPath = currentPath;
        return;
    }
    if (!foundPath.empty())
    {
        return;
    }

    if (currentPosition.y > 0 && !board.getBoard().at(currentPosition.x).at(currentPosition.y)->isUpColissionOn() &&
        eachTileWeight[currentPosition.x][currentPosition.y - 1] == 1 + currentPath.size()
        )
    {
        Coordinate nextPosition(currentPosition.x, currentPosition.y - 1);
        QVector<Direction> nextPath = currentPath;
        nextPath.push_back(Direction::UP);
        recursiveFastestSearch(board, nextPosition, endingPosition, eachTileWeight, nextPath, foundPath);
    }
    if (currentPosition.x < BOARD_X_SIZE && !board.getBoard().at(currentPosition.x).at(currentPosition.y)->isRightColissionOn() &&
        eachTileWeight[currentPosition.x + 1][currentPosition.y] == 1 + currentPath.size()
        )
    {
        Coordinate nextPosition(currentPosition.x + 1, currentPosition.y);
        QVector<Direction> nextPath = currentPath;
        nextPath.push_back(Direction::RIGHT);
        recursiveFastestSearch(board, nextPosition, endingPosition, eachTileWeight, nextPath, foundPath);
    }
    if (currentPosition.y < BOARD_Y_SIZE && !board.getBoard().at(currentPosition.x).at(currentPosition.y)->isDownColissionOn() &&
        eachTileWeight[currentPosition.x][currentPosition.y + 1] == 1 + currentPath.size()
        )
    {
        Coordinate nextPosition(currentPosition.x, currentPosition.y + 1);
        QVector<Direction> nextPath = currentPath;
        nextPath.push_back(Direction::DOWN);
        recursiveFastestSearch(board, nextPosition, endingPosition, eachTileWeight, nextPath, foundPath);
    }
    if (currentPosition.x > 0 && !board.getBoard().at(currentPosition.x).at(currentPosition.y)->isLeftColissionOn() &&
        eachTileWeight[currentPosition.x - 1][currentPosition.y] == 1 + currentPath.size()
        )
    {
        Coordinate nextPosition(currentPosition.x - 1, currentPosition.y);
        QVector<Direction> nextPath = currentPath;
        nextPath.push_back(Direction::LEFT);
        recursiveFastestSearch(board, nextPosition, endingPosition, eachTileWeight, nextPath, foundPath);
    }
}

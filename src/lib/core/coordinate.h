#ifndef COORDINATE_H
#define COORDINATE_H

struct Coordinate
{
    int x;
    int y;

    Coordinate(int xToSet, int yToSet)
    {
        this->x = xToSet;
        this->y = yToSet;
    }
    Coordinate(const Coordinate &coordinateToCopy)
        : Coordinate::Coordinate(coordinateToCopy.x, coordinateToCopy.y)
    {
    }

    Coordinate operator=(const Coordinate &coordinateToCopy);
    bool operator==(const Coordinate &compareTo) const;
    bool operator<(const Coordinate &compareTo) const;
    bool operator!=(const Coordinate &compareTo) const;
    bool equalTo(const Coordinate &compareTo) const;
    int distanceFrom(const Coordinate &compareTo) const;
};

#endif // COORDINATE_H

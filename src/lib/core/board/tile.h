#ifndef TILE_H
#define TILE_H

#define WALL_UP_COLLISION 1
#define WALL_RIGHT_COLLISION 2
#define WALL_DOWN_COLLISION 4
#define WALL_LEFT_COLLISION 8

#include <QLabel>

class Tile
{
public:
    explicit Tile(char collision);
    explicit Tile(char collision, QPixmap pixmap);
    Tile(const Tile &tileToCopy);
    void copy(const Tile &tileToCopy);


    char getCollision() const;
    void setCollision(char collision);
    bool isUpColissionOn() const;
    bool isRightColissionOn() const;
    bool isDownColissionOn() const;
    bool isLeftColissionOn() const;

    QPixmap getPixamp() const;
    void setPixmap(const QPixmap &pixmap);

    static char generateCollision(bool upCollision, bool rightCollision, bool downCollision, bool leftCollision);


private:
    char m_collision;
    QPixmap m_pixmap;
};

inline char Tile::getCollision() const
{
    return m_collision;
}

inline void Tile::setCollision(char collision)
{
    m_collision = collision;
}

inline QPixmap Tile::getPixamp() const
{
    return m_pixmap;
}

inline void Tile::setPixmap(const QPixmap &pixmap)
{
    m_pixmap = pixmap;
}

#endif // TILE_H

#ifndef MAPELEMENT_H
#define MAPELEMENT_H

#include <QLabel>

#include "tile.h"

class MapElement
{
public:
    MapElement();
    virtual std::shared_ptr<Tile> tileAtPos(int x, int y) const = 0;
};

#endif // MAPELEMENT_H

#ifndef FLOOR_H
#define FLOOR_H

#include <QMap>

#include "mapelement.h"
#include "tile.h"

class Floor : public MapElement
{
public:
    Floor();

    // MapElement interface
public:
    virtual std::shared_ptr<Tile> tileAtPos(int x, int y) const override;
protected:
    static QPixmap getPixmap(char collision);

private:
    static void initPixmaps();

    static QMap<char, QPixmap> s_pixmaps;
};

#endif // FLOOR_H

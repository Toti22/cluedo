#include "board.h"

#include <QBrush>
#include <QPixmap>
#include <QPainter>

#include "rooms/ballRoom.h"
#include "rooms/billiardRoom.h"
#include "rooms/diningRoom.h"
#include "rooms/conservatory.h"
#include "rooms/hall.h"
#include "rooms/kitchen.h"
#include "rooms/library.h"
#include "rooms/lounge.h"
#include "rooms/study.h"
#include "boardCaracteristics.h"

Board * Board::s_boardInstance = nullptr;

Board *Board::getBoardInstance()
{
    if (s_boardInstance == nullptr)
    {
        s_boardInstance = new Board();
    }
    return s_boardInstance;
}

void Board::freeBoardInstance()
{
    if (s_boardInstance != nullptr)
    {
        delete s_boardInstance;
        s_boardInstance = nullptr;
    }
}

Board::Board()
{
    m_floor = new Floor();
    m_mapElements.append(m_floor);

    BallRoom *ballRoom = new BallRoom();
    BilliardRoom *billiardRoom = new BilliardRoom();
    DiningRoom *diningRoom = new DiningRoom();
    Conservatory *conservatory = new Conservatory();
    Hall *hall = new Hall();
    Kitchen *kitchen = new Kitchen();
    Library *library = new Library();
    Lounge *lounge = new Lounge();
    Study *study = new Study();

    m_rooms.append(ballRoom);
    m_rooms.append(billiardRoom);
    m_rooms.append(diningRoom);
    m_rooms.append(conservatory);
    m_rooms.append(hall);
    m_rooms.append(kitchen);
    m_rooms.append(library);
    m_rooms.append(lounge);
    m_rooms.append(study);

    m_mapElements.append(ballRoom);
    m_mapElements.append(billiardRoom);
    m_mapElements.append(diningRoom);
    m_mapElements.append(conservatory);
    m_mapElements.append(hall);
    m_mapElements.append(kitchen);
    m_mapElements.append(library);
    m_mapElements.append(lounge);
    m_mapElements.append(study);

    initBoard();

}

void Board::initBoard()
{
    QRect rect(0, 0, TILE_SIZE, TILE_SIZE);
    QBrush darkGreenBrush(Qt::GlobalColor::darkGreen);

    QPixmap pixmap(":/onePixel.png");
    pixmap = pixmap.scaled(rect.height(), rect.width());
    QPainter painter(&pixmap);
    painter.fillRect(rect, darkGreenBrush);

    Tile defaultTile(Tile::generateCollision(true, true, true, true), pixmap);

    std::shared_ptr<Tile> currentTile = nullptr;
    for (int x = 0 ; x < BOARD_X_SIZE ; ++x)
    {
        QVector<std::shared_ptr<Tile>> yVector;
        for (int y = 0 ; y < BOARD_Y_SIZE ; ++y)
        {
            for (auto iterMapElement : m_mapElements)
            {
                currentTile = iterMapElement->tileAtPos(x, y);
                if (currentTile != nullptr)
                {
                    yVector.push_back(currentTile);
                    break;
                }
            }
            if (currentTile == nullptr)
            {
                yVector.push_back(std::make_shared<Tile>(defaultTile));
            }
            currentTile = nullptr;
        }
        m_board.push_back(yVector);
    }
}



#ifndef LOUNGE_H
#define LOUNGE_H

#include "room.h"

class Lounge : public Room
{
public:
    Lounge();

    // MapElement interface
public:
    virtual std::shared_ptr<Tile> tileAtPos(int x, int y) const override;
    virtual Coordinate getLabelCoordinate() const override;
    virtual QString getRoomText() const override;
};

#endif // LOUNGE_H

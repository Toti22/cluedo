#include "billiardRoom.h"

BilliardRoom::BilliardRoom()
{

}

std::shared_ptr<Tile> BilliardRoom::tileAtPos(int x, int y) const
{
    char collision = 0;
    if (x >= 18 && x <= 23 && y >= 8 && y <= 12)
    {
        // Door
        if (x == 18 && y == 9)
        {
            collision = Tile::generateCollision(true, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }
        // Door
        else if (x == 22 && y == 12)
        {
            collision = Tile::generateCollision(true, true, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }
        else if (x == 18 && y == 8)
        {
            collision = Tile::generateCollision(true, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 18 && y == 12)
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 23 && y == 8)
        {
            collision = Tile::generateCollision(true, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 23 && y == 12)
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 18)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 23)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 8)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 12)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
    }

    return nullptr;
}

Coordinate BilliardRoom::getLabelCoordinate() const
{
    return Coordinate(20, 9);
}

QString BilliardRoom::getRoomText() const
{
    return "Billiard\nRoom";
}

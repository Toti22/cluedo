#include "lounge.h"

Lounge::Lounge()
{

}

std::shared_ptr<Tile> Lounge::tileAtPos(int x, int y) const
{
    char collision = 0;
    if (x >= 0 && x <= 6 && y >= 19 && y <= 24)
    {
        // Exclude unused tile
        if (x == 6 && y == 24)
        {
            return nullptr;
        }
        // Door
        if (x == 6 && y == 19)
        {
            collision = Tile::generateCollision(false, true, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, true, false, false))));
        }
        else if (x == 0 && y == 19)
        {
            collision = Tile::generateCollision(true, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 0 && y == 24)
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 6 && y == 19)
        {
            collision = Tile::generateCollision(true, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if ((x == 5 && y == 24) || (x == 6 && y == 23))
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 0)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 6)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 19)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 24)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
    }

    return nullptr;
}

Coordinate Lounge::getLabelCoordinate() const
{
    return Coordinate(3, 20);
}

QString Lounge::getRoomText() const
{
    return "Lounge";
}

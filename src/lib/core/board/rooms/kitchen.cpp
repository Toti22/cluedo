#include "kitchen.h"

Kitchen::Kitchen()
{

}

std::shared_ptr<Tile> Kitchen::tileAtPos(int x, int y) const
{
    char collision = 0;
    if (x >= 0 && x < 6 && y >= 1 && y < 7)
    {
        // Door Location
        if (x == 4 && y == 6)
        {
            collision = Tile::generateCollision(true, true, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }

        // Upper left Wall
        if (x == 0 && y == 1)
        {
            collision = Tile::generateCollision(true, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        // Upper right Wall
        else if (x == 5 && y == 1)
        {
            collision = Tile::generateCollision(true, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        // Left Walls
        else if (x == 0 && y < 5)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        // Upper Walls
        else if (y == 1)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if ((x == 0 && y == 5) || (x == 1 && y == 6))
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 5 && y == 6)
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x > 0 && y == 6)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 5)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        // Neutral Tile. We exclude tile where x = 0 and y = 6
        else if (!(x == 0 && y == 6))
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else
        {
            return nullptr;
        }
    }
    else
    {
        return nullptr;
    }
}

Coordinate Kitchen::getLabelCoordinate() const
{
    return Coordinate(2, 2);
}

QString Kitchen::getRoomText() const
{
    return "Kitchen";
}

#include "diningRoom.h"

DiningRoom::DiningRoom()
{

}

std::shared_ptr<Tile> DiningRoom::tileAtPos(int x, int y) const
{
    char collision = 0;
    if (x >= 0 && x <= 7 && y >= 9 && y <= 15)
    {
        // Exclude unused tile
        if ((x == 5 || x == 6 || x == 7) && y == 9)
        {
            return nullptr;
        }
        // Door
        if (x == 7 && y == 12)
        {
            collision = Tile::generateCollision(true, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }
        // Door
        else if (x == 6 && y == 15)
        {
            collision = Tile::generateCollision(true, true, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }
        else if (x == 0 && y == 9)
        {
            collision = Tile::generateCollision(true, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 0 && y == 15)
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if ((x == 4 && y == 9) || (x == 7 && y == 10))
        {
            collision = Tile::generateCollision(true, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if ((x == 7 && y == 15))
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 0)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 7)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 9 || (y == 10 && x >= 5 && x <= 7))
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 15)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
    }

    return nullptr;
}

Coordinate DiningRoom::getLabelCoordinate() const
{
    return Coordinate(3, 11);
}

QString DiningRoom::getRoomText() const
{
    return "Dining\nRoom";
}

#include "hall.h"

Hall::Hall()
{

}

std::shared_ptr<Tile> Hall::tileAtPos(int x, int y) const
{
    char collision = 0;
    if (x >= 9 && x <= 14 && y >= 18 && y <= 24)
    {
        // Doors
        if ((x == 11 || x == 12) && y == 18)
        {
            collision = Tile::generateCollision(false, true, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }
        // Door
        else if (x == 14 && y == 20)
        {
            collision = Tile::generateCollision(true, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }
        else if (x == 9 && y == 18)
        {
            collision = Tile::generateCollision(true, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 9 && y == 24)
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 14 && y == 18)
        {
            collision = Tile::generateCollision(true, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 14 && y == 24)
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 9)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 14)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 18)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 24)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
    }

    return nullptr;
}

Coordinate Hall::getLabelCoordinate() const
{
    return Coordinate(11, 20);
}

QString Hall::getRoomText() const
{
    return "Hall";
}

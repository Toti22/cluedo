#ifndef HALL_H
#define HALL_H

#include "room.h"

class Hall : public Room
{
public:
    Hall();

    // MapElement interface
public:
    virtual std::shared_ptr<Tile> tileAtPos(int x, int y) const override;
    virtual Coordinate getLabelCoordinate() const override;
    virtual QString getRoomText() const override;
};

#endif // HALL_H

#ifndef LIBRARY_H
#define LIBRARY_H

#include "room.h"

class Library : public Room
{
public:
    Library();

    // MapElement interface
public:
    virtual std::shared_ptr<Tile> tileAtPos(int x, int y) const override;
    virtual Coordinate getLabelCoordinate() const override;
    virtual QString getRoomText() const override;
};

#endif // LIBRARY_H

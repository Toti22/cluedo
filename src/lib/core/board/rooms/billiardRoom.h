#ifndef BILLIARDROOM_H
#define BILLIARDROOM_H

#include "room.h"

class BilliardRoom : public Room
{
public:
    BilliardRoom();

    // MapElement interface
public:
    virtual std::shared_ptr<Tile> tileAtPos(int x, int y) const override;
    virtual Coordinate getLabelCoordinate() const override;
    virtual QString getRoomText() const override;
};

#endif // BILLIARDROOM_H

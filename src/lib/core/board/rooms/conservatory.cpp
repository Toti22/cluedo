#include "conservatory.h"

Conservatory::Conservatory()
{

}

std::shared_ptr<Tile> Conservatory::tileAtPos(int x, int y) const
{
    char collision = 0;
    if (x >= 18 && x <= 23 && y >= 1 && y <= 5)
    {
        // Exclude unused tile
        if ((x == 18 && y == 5) || (x == 23 && y == 5))
        {
            return nullptr;
        }
        // Door
        if (x == 18 && y == 4)
        {
            collision = Tile::generateCollision(true, true, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, true))));
        }
        else if (x == 18 && y == 1)
        {
            collision = Tile::generateCollision(true, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 19 && y == 5)
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 23 && y == 1)
        {
            collision = Tile::generateCollision(true, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if ((x == 22 && y == 5) || (x == 23 && y == 4))
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 18)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 23)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 1)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 5)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
    }

    return nullptr;
}

Coordinate Conservatory::getLabelCoordinate() const
{
    return Coordinate(20, 2);
}

QString Conservatory::getRoomText() const
{
    return "Conservatory";
}

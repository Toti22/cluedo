#include "ballRoom.h"

BallRoom::BallRoom()
{

}

std::shared_ptr<Tile> BallRoom::tileAtPos(int x, int y) const
{
    char collision = 0;
    if (x >= 10 && x <= 13 && y == 1)
    {
        if (x == 10)
        {
            collision = Tile::generateCollision(true, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 13)
        {
            collision = Tile::generateCollision(true, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
    }
    else if (x >= 8 && x <= 15 && y>=2 && y <= 7)
    {
        // Door
        if (x == 8 && y == 5)
        {
            collision = Tile::generateCollision(true, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }
        // Door
        else if (x == 15 && y == 5)
        {
            collision = Tile::generateCollision(true, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }
        // Door
        else if (x == 9 && y == 7)
        {
            collision = Tile::generateCollision(true, true, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }
        // Door
        else if (x == 14 && y == 7)
        {
            collision = Tile::generateCollision(true, true, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }
        else if (x == 8 && y == 2)
        {
            collision = Tile::generateCollision(true, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 8 && y == 7)
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 15 && y == 2)
        {
            collision = Tile::generateCollision(true, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 15 && y == 7)
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 8)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 15)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 2 && (x == 9 || x == 14))
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 7)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
    }

    return nullptr;
}

Coordinate BallRoom::getLabelCoordinate() const
{
    return Coordinate(11, 2);

}

QString BallRoom::getRoomText() const
{
    return "Ballroom";
}

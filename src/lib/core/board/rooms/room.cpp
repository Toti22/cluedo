#include "room.h"

#include <QPainter>

#include "../boardCaracteristics.h"

QMap<char, QPixmap> Room::m_pixmaps;

Room::Room()
{
    if (m_pixmaps.empty())
    {
        initPixmaps();
    }
}

QPixmap Room::getPixmap(char collision)
{
    if (m_pixmaps.find(collision) != m_pixmaps.end())
    {
        return *m_pixmaps.find(collision);
    }
    return QPixmap();
}

void Room::initPixmaps()
{
    char collision = 0;
    QRect rect(0, 0, TILE_SIZE, TILE_SIZE);
    int thickness = TILE_SIZE / 8;

    QBrush greenBrush(Qt::GlobalColor::green);
    QBrush redBrush(Qt::GlobalColor::red);

    QPixmap pixmap(":/onePixel.png");
    pixmap = pixmap.scaled(rect.height(), rect.width());
    QPainter painter(&pixmap);
    painter.fillRect(rect, greenBrush);

    m_pixmaps.insert(collision, pixmap);


    // LEFT AND UP
    {
        collision = Tile::generateCollision(true, false, false, true);
        QPixmap pixmapLeftAndUp(pixmap);
        QPainter painterLeftAndUp(&pixmapLeftAndUp);
        painterLeftAndUp.fillRect(0, 0, thickness, rect.height(), redBrush);
        painterLeftAndUp.fillRect(0, 0, rect.height(), thickness, redBrush);
        painterLeftAndUp.end();

        m_pixmaps.insert(collision, pixmapLeftAndUp);
    }
    // UP
    {
        collision = Tile::generateCollision(true, false, false, false);
        QPixmap pixmapUp(pixmap);
        QPainter painterUp(&pixmapUp);
        painterUp.fillRect(0, 0, rect.height(), thickness, redBrush);
        painterUp.end();

        m_pixmaps.insert(collision, pixmapUp);
    }
    // UP AND RIGHT
    {
        collision = Tile::generateCollision(true, true, false, false);
        QPixmap pixmapUpAndRight(pixmap);
        QPainter painterUpAndRight(&pixmapUpAndRight);
        painterUpAndRight.fillRect(0, 0, rect.height(), thickness, redBrush);
        painterUpAndRight.fillRect(rect.height() - thickness, 0, rect.height(), rect.height(), redBrush);
        painterUpAndRight.end();

        m_pixmaps.insert(collision, pixmapUpAndRight);
    }
    // RIGHT
    {
        collision = Tile::generateCollision(false, true, false, false);
        QPixmap pixmapRight(pixmap);
        QPainter painterRight(&pixmapRight);
        painterRight.fillRect(rect.height() - thickness, 0, rect.height(), rect.height(), redBrush);
        painterRight.end();

        m_pixmaps.insert(collision, pixmapRight);
    }
    // RIGHT AND DOWN
    {
        collision = Tile::generateCollision(false, true, true, false);
        QPixmap pixmapRightAndDown(pixmap);
        QPainter painterRightAndDown(&pixmapRightAndDown);
        painterRightAndDown.fillRect(rect.height() - thickness, 0, rect.height(), rect.height(), redBrush);
        painterRightAndDown.fillRect(0,rect.height() - thickness, rect.height(), rect.height(), redBrush);
        painterRightAndDown.end();

        m_pixmaps.insert(collision, pixmapRightAndDown);
    }
    // DOWN
    {
        collision = Tile::generateCollision(false, false, true, false);
        QPixmap pixmapDown(pixmap);
        QPainter painterDown(&pixmapDown);
        painterDown.fillRect(0,rect.height() - thickness, rect.height(), rect.height(), redBrush);
        painterDown.end();

        m_pixmaps.insert(collision, pixmapDown);
    }
    // DOWN AND LEFT
    {
        collision = Tile::generateCollision(false, false, true, true);
        QPixmap pixmapDownAndLeft(pixmap);
        QPainter painterDownAndLeft(&pixmapDownAndLeft);
        painterDownAndLeft.fillRect(0, 0, thickness, rect.height(), redBrush);
        painterDownAndLeft.fillRect(0,rect.height() - thickness, rect.height(), rect.height(), redBrush);
        painterDownAndLeft.end();

        m_pixmaps.insert(collision, pixmapDownAndLeft);
    }
    // LEFT
    {
        collision = Tile::generateCollision(false, false, false, true);
        QPixmap pixmapLeft(pixmap);
        QPainter painterLeft(&pixmapLeft);
        painterLeft.fillRect(0, 0, thickness, rect.height(), redBrush);
        painterLeft.end();

        m_pixmaps.insert(collision, pixmapLeft);
    }

}

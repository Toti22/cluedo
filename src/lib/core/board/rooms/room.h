#ifndef ROOM_H
#define ROOM_H

#include <QString>
#include <QPixmap>
#include <QMap>

#include "../mapelement.h"
#include "../../coordinate.h"
#include "../tile.h"

class Room : public MapElement
{
public:
    Room();
    virtual Coordinate getLabelCoordinate() const = 0;
    virtual QString getRoomText() const = 0;

protected:
    static QPixmap getPixmap(char collision);

private:
    static void initPixmaps();

    static QMap<char, QPixmap> m_pixmaps;

};

#endif // ROOM_H

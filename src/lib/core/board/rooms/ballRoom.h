#ifndef BALLROOM_H
#define BALLROOM_H

#include "room.h"

class BallRoom : public Room
{
public:
    BallRoom();

    // MapElement interface
public:
    virtual std::shared_ptr<Tile> tileAtPos(int x, int y) const override;
    virtual Coordinate getLabelCoordinate() const override;
    virtual QString getRoomText() const override;
};

#endif // BALLROOM_H

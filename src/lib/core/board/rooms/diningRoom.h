#ifndef DININGROOM_H
#define DININGROOM_H

#include "room.h"

class DiningRoom : public Room
{
public:
    DiningRoom();

    // MapElement interface
public:
    virtual std::shared_ptr<Tile> tileAtPos(int x, int y) const override;
    virtual Coordinate getLabelCoordinate() const override;
    virtual QString getRoomText() const override;
};

#endif // DININGROOM_H

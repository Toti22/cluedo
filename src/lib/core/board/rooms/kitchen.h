#ifndef KITCHEN_H
#define KITCHEN_H

#include <QString>
#include <memory>

#include "room.h"
#include "../../coordinate.h"

class Kitchen : public Room
{
public:
    Kitchen();

public:
    virtual std::shared_ptr<Tile> tileAtPos(int x, int y) const override;
    virtual Coordinate getLabelCoordinate() const override;
    virtual QString getRoomText() const override;
};

#endif // KITCHEN_H

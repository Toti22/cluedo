#ifndef STUDY_H
#define STUDY_H

#include "room.h"

class Study : public Room
{
public:
    Study();

    // MapElement interface
public:
    virtual std::shared_ptr<Tile> tileAtPos(int x, int y) const override;
    virtual Coordinate getLabelCoordinate() const override;
    virtual QString getRoomText() const override;
};

#endif // STUDY_H

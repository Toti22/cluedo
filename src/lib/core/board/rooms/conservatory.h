#ifndef CONSERVATORY_H
#define CONSERVATORY_H

#include "room.h"

class Conservatory : public Room
{
public:
    Conservatory();

    // MapElement interface
public:
    virtual std::shared_ptr<Tile> tileAtPos(int x, int y) const override;
    virtual Coordinate getLabelCoordinate() const override;
    virtual QString getRoomText() const override;
};

#endif // CONSERVATORY_H

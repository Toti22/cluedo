#include "library.h"

Library::Library()
{

}

std::shared_ptr<Tile> Library::tileAtPos(int x, int y) const
{
    char collision = 0;
    if (x >= 17 && x <= 23 && y >= 14 && y <= 18)
    {
        // Exclude unused tile
        if ((x == 17 || x == 23) && (y == 14 || y == 18))
        {
            return nullptr;
        }
        // Door
        if (x == 20 && y == 14)
        {
            collision = Tile::generateCollision(false, true, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }
        // Door
        else if (x == 17 && y == 16)
        {
            collision = Tile::generateCollision(true, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(Tile::generateCollision(false, false, false, false))));
        }
        else if ((x == 18 && y == 14) || (x == 17 && y == 15))
        {
            collision = Tile::generateCollision(true, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if ((x == 17 && y == 17) || (x == 18 && y == 18))
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if ((x == 22 && y == 14) || (x == 23 && y == 15))
        {
            collision = Tile::generateCollision(true, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if ((x == 23 && y == 17) || (x == 22 && y == 18))
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 17)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (x == 23)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 14)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else if (y == 18)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
        else
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Room::getPixmap(collision)));
        }
    }

    return nullptr;
}

Coordinate Library::getLabelCoordinate() const
{
    return Coordinate(20, 15);
}

QString Library::getRoomText() const
{
    return "Library";
}

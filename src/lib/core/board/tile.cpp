#include "tile.h"

Tile::Tile(char collision)
{
    setCollision(collision);
}

Tile::Tile(char collision, QPixmap pixmap)
{
    setCollision(collision);
    setPixmap(pixmap);
}

Tile::Tile(Tile const &tileToCopy)
{
    copy(tileToCopy);
}

void Tile::copy(const Tile &tileToCopy)
{
    setCollision(tileToCopy.getCollision());
    setPixmap(tileToCopy.getPixamp());
}

bool Tile::isUpColissionOn() const
{
    return (m_collision & WALL_UP_COLLISION) > 0;
}

bool Tile::isRightColissionOn() const
{
    return (m_collision & WALL_RIGHT_COLLISION) > 0;
}

bool Tile::isDownColissionOn() const
{
    return (m_collision & WALL_DOWN_COLLISION) > 0;
}

bool Tile::isLeftColissionOn() const
{
    return (m_collision & WALL_LEFT_COLLISION) > 0;
}

char Tile::generateCollision(bool upCollision, bool rightCollision, bool downCollision, bool leftCollision)
{
    char collision = 0;
    if (upCollision == true)
    {
        collision = collision | WALL_UP_COLLISION;
    }
    if (rightCollision == true)
    {
        collision = collision | WALL_RIGHT_COLLISION;
    }
    if (downCollision == true)
    {
        collision = collision | WALL_DOWN_COLLISION;
    }
    if (leftCollision == true)
    {
        collision = collision | WALL_LEFT_COLLISION;
    }

    return collision;
}

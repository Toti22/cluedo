#include "floor.h"

#include <QPainter>

#include "boardCaracteristics.h"

QMap<char, QPixmap> Floor::s_pixmaps;

Floor::Floor()
{
    if (s_pixmaps.empty())
    {
        initPixmaps();
    }
}

QPixmap Floor::getPixmap(char collision)
{
    if (s_pixmaps.find(collision) != s_pixmaps.end())
    {
        return *s_pixmaps.find(collision);
    }
    return QPixmap();
}

void Floor::initPixmaps()
{
    char collision = 0;
    QRect rect(0, 0, TILE_SIZE, TILE_SIZE);
    int thicknessWall = 2;
    int thicknessGray = 1;

    QBrush yellowBrush(Qt::GlobalColor::yellow);
    QBrush blackBrush(Qt::GlobalColor::black);
    QBrush lightGrayBrush(Qt::GlobalColor::lightGray);

    QPixmap pixmap(":/onePixel.png");
    pixmap = pixmap.scaled(rect.height(), rect.width());
    QPainter painter(&pixmap);
    painter.fillRect(rect, yellowBrush);
    painter.fillRect(0, 0, thicknessGray, rect.height(), lightGrayBrush);
    painter.fillRect(0, 0, rect.height(), thicknessGray, lightGrayBrush);
    painter.fillRect(rect.height() - thicknessGray, 0, rect.height(), rect.height(), lightGrayBrush);
    painter.fillRect(0,rect.height() - thicknessGray, rect.height(), rect.height(), lightGrayBrush);

    s_pixmaps.insert(collision, pixmap);

    // LEFT AND UP
    {
        collision = Tile::generateCollision(true, false, false, true);
        QPixmap pixmapLeftAndUp(pixmap);
        QPainter painterLeftAndUp(&pixmapLeftAndUp);
        painterLeftAndUp.fillRect(0, 0, thicknessWall, rect.height(), blackBrush);
        painterLeftAndUp.fillRect(0, 0, rect.height(), thicknessWall, blackBrush);
        painterLeftAndUp.end();

        s_pixmaps.insert(collision, pixmapLeftAndUp);
    }
    // UP
    {
        collision = Tile::generateCollision(true, false, false, false);
        QPixmap pixmapUp(pixmap);
        QPainter painterUp(&pixmapUp);
        painterUp.fillRect(0, 0, rect.height(), thicknessWall, blackBrush);
        painterUp.end();

        s_pixmaps.insert(collision, pixmapUp);
    }
    // UP AND RIGHT
    {
        collision = Tile::generateCollision(true, true, false, false);
        QPixmap pixmapUpAndRight(pixmap);
        QPainter painterUpAndRight(&pixmapUpAndRight);
        painterUpAndRight.fillRect(0, 0, rect.height(), thicknessWall, blackBrush);
        painterUpAndRight.fillRect(rect.height() - thicknessWall, 0, rect.height(), rect.height(), blackBrush);
        painterUpAndRight.end();

        s_pixmaps.insert(collision, pixmapUpAndRight);
    }
    // RIGHT
    {
        collision = Tile::generateCollision(false, true, false, false);
        QPixmap pixmapRight(pixmap);
        QPainter painterRight(&pixmapRight);
        painterRight.fillRect(rect.height() - thicknessWall, 0, rect.height(), rect.height(), blackBrush);
        painterRight.end();

        s_pixmaps.insert(collision, pixmapRight);
    }
    // RIGHT AND DOWN
    {
        collision = Tile::generateCollision(false, true, true, false);
        QPixmap pixmapRightAndDown(pixmap);
        QPainter painterRightAndDown(&pixmapRightAndDown);
        painterRightAndDown.fillRect(rect.height() - thicknessWall, 0, rect.height(), rect.height(), blackBrush);
        painterRightAndDown.fillRect(0,rect.height() - thicknessWall, rect.height(), rect.height(), blackBrush);
        painterRightAndDown.end();

        s_pixmaps.insert(collision, pixmapRightAndDown);
    }
    // DOWN
    {
        collision = Tile::generateCollision(false, false, true, false);
        QPixmap pixmapDown(pixmap);
        QPainter painterDown(&pixmapDown);
        painterDown.fillRect(0,rect.height() - thicknessWall, rect.height(), rect.height(), blackBrush);
        painterDown.end();

        s_pixmaps.insert(collision, pixmapDown);
    }
    // DOWN AND LEFT
    {
        collision = Tile::generateCollision(false, false, true, true);
        QPixmap pixmapDownAndLeft(pixmap);
        QPainter painterDownAndLeft(&pixmapDownAndLeft);
        painterDownAndLeft.fillRect(0, 0, thicknessWall, rect.height(), blackBrush);
        painterDownAndLeft.fillRect(0,rect.height() - thicknessWall, rect.height(), rect.height(), blackBrush);
        painterDownAndLeft.end();

        s_pixmaps.insert(collision, pixmapDownAndLeft);
    }
    // LEFT
    {
        collision = Tile::generateCollision(false, false, false, true);
        QPixmap pixmapLeft(pixmap);
        QPainter painterLeft(&pixmapLeft);
        painterLeft.fillRect(0, 0, thicknessWall, rect.height(), blackBrush);
        painterLeft.end();

        s_pixmaps.insert(collision, pixmapLeft);
    }
    // UP AND DOWN
    {
        collision = Tile::generateCollision(true, false, true, false);
        QPixmap pixmapUpAndDown(pixmap);
        QPainter painterUpAndDown(&pixmapUpAndDown);
        painterUpAndDown.fillRect(0, 0, rect.height(), thicknessWall, blackBrush);
        painterUpAndDown.fillRect(0,rect.height() - thicknessWall, rect.height(), rect.height(), blackBrush);
        painterUpAndDown.end();

        s_pixmaps.insert(collision, pixmapUpAndDown);
    }
    // UP AND RIGHT AND LEFT
    {
        collision = Tile::generateCollision(true, true, false, true);
        QPixmap pixmapUpAndRightAndDown(pixmap);
        QPainter painterUpAndRightAndDown(&pixmapUpAndRightAndDown);
        painterUpAndRightAndDown.fillRect(0, 0, rect.height(), thicknessWall, blackBrush);
        painterUpAndRightAndDown.fillRect(rect.height() - thicknessWall, 0, rect.height(), rect.height(), blackBrush);
        painterUpAndRightAndDown.fillRect(0, 0, thicknessWall, rect.height(), blackBrush);
        painterUpAndRightAndDown.end();

        s_pixmaps.insert(collision, pixmapUpAndRightAndDown);
    }
    // UP AND LEFT AND DOWN
    {
        collision = Tile::generateCollision(true, false, true, true);
        QPixmap pixmapUpAndLeftAndDown(pixmap);
        QPainter painterUpAndLeftAndDown(&pixmapUpAndLeftAndDown);
        painterUpAndLeftAndDown.fillRect(0, 0, rect.height(), thicknessWall, blackBrush);
        painterUpAndLeftAndDown.fillRect(0,rect.height() - thicknessWall, rect.height(), rect.height(), blackBrush);
        painterUpAndLeftAndDown.fillRect(0, 0, thicknessWall, rect.height(), blackBrush);
        painterUpAndLeftAndDown.end();

        s_pixmaps.insert(collision, pixmapUpAndLeftAndDown);
    }
    // UP AND RIGHT AND DOWN
    {
        collision = Tile::generateCollision(true, true, true, false);
        QPixmap pixmapUpAndRightAndDown(pixmap);
        QPainter painterUpAndRightAndDown(&pixmapUpAndRightAndDown);
        painterUpAndRightAndDown.fillRect(0, 0, rect.height(), thicknessWall, blackBrush);
        painterUpAndRightAndDown.fillRect(0,rect.height() - thicknessWall, rect.height(), rect.height(), blackBrush);
        painterUpAndRightAndDown.fillRect(rect.height() - thicknessWall, 0, rect.height(), rect.height(), blackBrush);
        painterUpAndRightAndDown.end();

        s_pixmaps.insert(collision, pixmapUpAndRightAndDown);
    }
    // RIGHT AND DOWN AND LEFT
    {
        collision = Tile::generateCollision(false, true, true, true);
        QPixmap pixmapRightAndDownAndLeft(pixmap);
        QPainter painterRightAndDownAndLeft(&pixmapRightAndDownAndLeft);
        painterRightAndDownAndLeft.fillRect(rect.height() - thicknessWall, 0, rect.height(), rect.height(), blackBrush);
        painterRightAndDownAndLeft.fillRect(0,rect.height() - thicknessWall, rect.height(), rect.height(), blackBrush);
        painterRightAndDownAndLeft.fillRect(0, 0, thicknessWall, rect.height(), blackBrush);
        painterRightAndDownAndLeft.end();

        s_pixmaps.insert(collision, pixmapRightAndDownAndLeft);
    }
}

std::shared_ptr<Tile> Floor::tileAtPos(int x, int y) const
{
    QRect rect(0, 0, TILE_SIZE, TILE_SIZE);

    QBrush blackBrush(Qt::GlobalColor::black);
    QPixmap pixmapFullBlack(":/onePixel.png");
    pixmapFullBlack = pixmapFullBlack.scaled(rect.height(), rect.width());
    QPainter painterBlack(&pixmapFullBlack);
    painterBlack.fillRect(rect, blackBrush);
    painterBlack.end();

    char collision = 0;

    // Line Index 0
    if (y == 0)
    {
        if (x == 9)
        {
            collision = Tile::generateCollision(true, true, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 14)
        {
            collision = Tile::generateCollision(true, true, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }
    // Line Index 1
    else if (y == 1)
    {
        if (x == 7)
        {
            collision = Tile::generateCollision(true, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 8 || x == 15)
        {
            collision = Tile::generateCollision(true, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 9)
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 14)
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 16)
        {
            collision = Tile::generateCollision(true, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }

    // Line Index 2
    else if (y == 2)
    {
        if (x == 6)
        {
            collision = Tile::generateCollision(true, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 7)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 16)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 17)
        {
            collision = Tile::generateCollision(true, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }

    // Line Index 3 - 6
    else if (y >= 3 && y <= 6)
    {
        if (y == 6 && x == 23)
        {
            collision = Tile::generateCollision(true, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        //Doors Ball Room
        else if (y == 5 && (x == 7 || x == 16))
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        //Doors Ball Room
        else if (y == 5 && x == 18)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 6 || x == 16)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 7 || (x == 17 && y <= 4))
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (y == 5 && x == 17)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (y == 6 && (x == 17 || x == 18))
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (y == 6 && x >= 19 && x <= 22)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }

    // Line Index 7
    else if (y == 7)
    {
        if (x == 0)
        {
            collision = Tile::generateCollision(true, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        // Kitchen Door
        else if (x == 4)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 1 && x <= 5)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 6 || x== 17)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 7)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 16)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 18 && x <= 21)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 22)
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }

    // Line Index 8
    if (y == 8)
    {
        //Doors Ball Room
        if (x == 9 || x == 14)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 1)
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 17)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 2 && x <= 4)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if ((x >= 5 && x <= 7) || x == 16)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 8 && x <= 15)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }

    // Line Index 9
    if (y == 9)
    {
        // Door Billiard Room
        if (x == 17)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 5)
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 6 || x == 7)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if ((x >= 8 && x <= 9) || (x >= 15 && x <= 16))
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 10 && x <= 14)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }

    //Line Index 10 - 12
    if (y >= 10 && y <= 12)
    {
        // Door Dining Room
        if (x == 8 && y == 12)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 8)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 9 || x == 17)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 15)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 16)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 10 && x <= 14)
        {
            collision = Tile::generateCollision(true, true, true, true);
            return std::make_shared<Tile>(Tile(collision, pixmapFullBlack));
        }
    }

    //Line Index 13
    if (y == 13)
    {
        //Door Library
        if (x == 20)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        //Door Billiard Room
        else if (x == 22)
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 8)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 9)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 15)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 16 || x == 17)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 18 && x <= 21)
        {
            collision = Tile::generateCollision(true, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 10 && x <= 14)
        {
            collision = Tile::generateCollision(true, true, true, true);
            return std::make_shared<Tile>(Tile(collision, pixmapFullBlack));
        }
    }

    //Line Index 14
    if (y == 14)
    {
        if (x == 8)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 9)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 15)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 16)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 17)
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 10 && x <= 14)
        {
            collision = Tile::generateCollision(true, true, true, true);
            return std::make_shared<Tile>(Tile(collision, pixmapFullBlack));
        }
    }

    //Line Index 15
    if (y == 15)
    {
        if (x == 8)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 9)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 15)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 16)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 10 && x <= 14)
        {
            collision = Tile::generateCollision(true, true, true, true);
            return std::make_shared<Tile>(Tile(collision, pixmapFullBlack));
        }
    }

    //Line Index 16
    if (y == 16)
    {
        // Door Dining Room
        if (x == 6)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        //Door Library
        else if (x == 16)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 1)
        {
            collision = Tile::generateCollision(true, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 2 && x <= 7)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 8)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 9)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 15)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 10 && x <= 14)
        {
            collision = Tile::generateCollision(true, true, true, true);
            return std::make_shared<Tile>(Tile(collision, pixmapFullBlack));
        }
    }

    //Line Index 17
    if (y == 17)
    {
        if (x == 0)
        {
            collision = Tile::generateCollision(true, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 11 || x == 12)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 9)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if ((x >= 1 && x <= 8) || x == 15)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 16)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 10 && x <= 14)
        {
            collision = Tile::generateCollision(true, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }

    //Line Index 18
    if (y == 18)
    {
        // Door Lounge
        if (x == 6)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 1)
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 8)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 15)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 17)
        {
            collision = Tile::generateCollision(true, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 2 && x <= 5)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 7 || x == 16)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }

    //Line Index 19 - 23 Left Part
    if (y >= 19 && y <= 23)
    {
        if (y == 19 && x == 23)
        {
            collision = Tile::generateCollision(true, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (y == 23 && x == 8)
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        if (x == 7)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 8)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }

    // Line Index 19 RIGHT PART
    if (y == 19)
    {
        if (x == 15)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 16 || x == 17)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 18 && x <= 22)
        {
            collision = Tile::generateCollision(true, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }

    // Line Index 20 RIGHT PART
    if (y == 20)
    {
        // Door Hall
        if (x == 15 ||x == 17)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 16 || x == 17)
        {
            collision = Tile::generateCollision(false, false, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x >= 18 && x <= 21)
        {
            collision = Tile::generateCollision(false, false, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 22)
        {
            collision = Tile::generateCollision(false, true, true, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }
    if (y >= 21 && y <= 23)
    {
        if (y == 23 && x == 15)
        {
            collision = Tile::generateCollision(false, false, true, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 15)
        {
            collision = Tile::generateCollision(false, false, false, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 16)
        {
            collision = Tile::generateCollision(false, true, false, false);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
    }
    if (y == 24)
    {
        if (x == 7)
        {
            collision = Tile::generateCollision(false, true, true, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }
        else if (x == 16)
        {
            collision = Tile::generateCollision(false, true, true, true);
            return std::make_shared<Tile>(Tile(collision, Floor::getPixmap(collision)));
        }

    }
    return nullptr;
}

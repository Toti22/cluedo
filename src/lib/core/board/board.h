#ifndef BOARD_H
#define BOARD_H

#include <QVector>
#include <memory>

#include "tile.h"
#include "mapelement.h"
#include "floor.h"
#include "rooms/room.h"

class Board
{
public:
    static Board *getBoardInstance();
    static void freeBoardInstance();

    QVector<QVector<std::shared_ptr<Tile>>> getBoard() const;

    QVector<Room *> getRooms() const;

private:
    static Board * s_boardInstance;

    Board();
    void initBoard();

    QVector<QVector<std::shared_ptr<Tile>>> m_board;
    QVector<MapElement *> m_mapElements;
    Floor * m_floor;
    QVector<Room *> m_rooms;
};

inline QVector<QVector<std::shared_ptr<Tile> > > Board::getBoard() const
{
    return m_board;
}

inline QVector<Room *> Board::getRooms() const
{
    return m_rooms;
}

#endif // BOARD_H

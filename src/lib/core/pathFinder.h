#ifndef PATHFINDER_H
#define PATHFINDER_H

enum class Direction {NONE = 0, UP = 1, RIGHT = 2, DOWN = 3, LEFT = 4};

#include <QVector>

#include "board/board.h"
#include "coordinate.h"


class PathFinder
{
public:
    static PathFinder * getPathFinderInstance(Board *board);
    static void freePathFinderInstances();

    QVector<Direction> findShortestPath(const Coordinate &startingPosition, const Coordinate &endingPosition);
    QVector<QVector<int>> getEachTileWeight(const Coordinate &position);
    QVector<Coordinate> getReachableCoordinates(const Coordinate &position, int depth);

private:
    static QMap<Board *, PathFinder *> s_pathFinders;

    PathFinder(Board *board);

    Board *m_board;
    QMap<Coordinate, QVector<QVector<int>>> m_eachTileWeightMap;

    static void recursiveFastestSearch(const Board &board,
                                       Coordinate currentPosition,
                                       const Coordinate &endingPosition,
                                       const QVector<QVector<int>> &alreadyCheckedPos,
                                       QVector<Direction> currentPath,
                                       QVector<Direction> &foundPath);
};

#endif // PATHFINDER_H

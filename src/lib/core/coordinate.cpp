#include "coordinate.h"
#include <math.h>

Coordinate Coordinate::operator=(const Coordinate &coordinateToCopy)
{
    this->x = coordinateToCopy.x;
    this->y = coordinateToCopy.y;
    return *this;
}

bool Coordinate::operator==(const Coordinate &compareTo) const
{
    return equalTo(compareTo);
}

bool Coordinate::operator<(const Coordinate &compareTo) const
{
    if (compareTo.y > this->y)
    {
        return true;
    }
    else if (compareTo.y == this->y && compareTo.x > this->x)
    {
        return true;
    }
    return false;
}

bool Coordinate::operator!=(const Coordinate &compareTo) const
{
    return !equalTo(compareTo);
}

bool Coordinate::equalTo(const Coordinate &compareTo) const
{
    return this->x == compareTo.x && this->y == compareTo.y;
}

int Coordinate::distanceFrom(const Coordinate &compareTo) const
{
    return abs(this->x - compareTo.x) + abs(this->y - compareTo.y);
}

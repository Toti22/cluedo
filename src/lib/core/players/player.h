#ifndef PLAYER_H
#define PLAYER_H

#include <QMap>
#include <QVector>
#include <QObject>
#include "core/gameLogic/gameElement/gameElementFactory.h"
#include "core/gameLogic/card.h"
#include "core/coordinate.h"
#include "core/gameLogic/suggestion.h"
#include "clueSheetData.h"

class Player : public QObject
{
    Q_OBJECT
public:
    static Player *getHumanPlayer(int characterId);

    void resetPlayer();

    virtual void drawCard(Card * card);
    virtual void drawCards(QVector<Card *> cards);

    virtual void sendPlayerStartedHisTurn(int playerId) = 0;
    virtual void playerThrownDice(int playerId, int throwResult);
    virtual void playerMovedTo(int playerId, const Coordinate &newCoordinate);
    virtual void playerTookSecretPassage(int playerId, const Coordinate &newCoordinate);
    virtual void playerMadeAnAccusation(int playerId, bool accussationIsRight, const Suggestion &suggestion) = 0;
    virtual void playerMadeASuggestion(int playerId, const Suggestion &suggestion) = 0;
    virtual void playerIsAskedToShowAClue(int playerIdWhoMadeSuggestion, const Suggestion &suggestion, int playerIdWhoShouldAnswer) = 0;
    virtual void playerAnswerToSuggestion(int elementIdShown, int idOfCharacterWhoAnswered) = 0;
    // If no one Answered because the player who made the suggestion got clue(s) in his hand, his id is set. If he has no clue, then the game is over and id is set to -1
    virtual void suggestionResult(const Suggestion &suggestion, int idOfCharacterWhoAnswered) = 0;
    virtual void playerEndedHisTurn(int playerId, bool allowed = true) = 0;

    // Error in client Reponse
    virtual void wrongThrowDice(const QString &errorMessage);
    virtual void wrongMoveTo(const Coordinate &receivedCoordinate, const Coordinate &playerCoordinateOnServer, const QString & errorMessage);
    virtual void wrongTakeSecretPassage(const Coordinate &playerCoordinateOnServer, const QString &errorMessage);
    virtual void wrongSuggestion(const Suggestion &suggestion, const Coordinate &playerCoordinateOnServer, const QString &errorMessage);
    virtual void wrongAnswerToSuggestion(const Suggestion &suggestion, int elementIdShown, const QString &errorMessage);

    Coordinate getPosition() const;
    void setPosition(const Coordinate &newPosition);

    int getCharacterId() const;

    bool getEleminated() const;
    void setEleminated(bool newEleminated);

    bool isLocalHumanPlayer() const;

    ClueSheetData *getClueSheetData();

    QVector<Card *> getCards() const;

    void setLocalHumanPlayer(bool newLocalHumanPlayer);

public slots:
    virtual void emitThrowDice();
    virtual void emitMoveTo(const Coordinate &coordinate);
    virtual void emitAccusation(const Suggestion &suggestion);
    virtual void emitSuggestion(const Suggestion &suggestion);
    virtual void emitShowClue(int clueShown);
    virtual void emitTakeSecretPassage();
    virtual void emitEndTurn();

protected:
    Player(int characterId);

    bool m_localHumanPlayer;

    static QMap<int, Player *> s_players;

    int m_characterId;
    bool m_eleminated;
    QVector<Card *> m_cards;
    int m_diceResult;
    Coordinate m_position;
    ClueSheetData m_clueSheetData;

signals:
    void throwDice();
    void moveTo(Coordinate);
    void makeAccusation(Suggestion);
    void makeSuggestion(Suggestion);
    void takeSecretPassage();
    void endTurn();
    void showClue(int);

    void playerThrownDiceSignal(int);
    void playerMoveToResultSignal(const Coordinate &positionToGoTo);
    void playerMakeAccusationSignal(bool heIsRight);
    void playerMakeSuggestionSignal(const Suggestion &suggestion);
    void playerTakeSecretPassageSignal();
    void playerEndTurnSignal(int);
    void playerStartedTurnSignal(int);
    void playerShownClue(Suggestion, int, int);
};

inline Coordinate Player::getPosition() const
{
    return m_position;
}

inline void Player::setPosition(const Coordinate &newPosition)
{
    m_position = newPosition;
}

inline int Player::getCharacterId() const
{
    return m_characterId;
}

inline bool Player::getEleminated() const
{
    return m_eleminated;
}

inline void Player::setEleminated(bool newEleminated)
{
    m_eleminated = newEleminated;
}

inline bool Player::isLocalHumanPlayer() const
{
    return m_localHumanPlayer;
}

inline ClueSheetData *Player::getClueSheetData()
{
    return &m_clueSheetData;
}

inline QVector<Card *> Player::getCards() const
{
    return m_cards;
}

inline void Player::setLocalHumanPlayer(bool newLocalHumanPlayer)
{
    m_localHumanPlayer = newLocalHumanPlayer;
}

#endif // PLAYER_H

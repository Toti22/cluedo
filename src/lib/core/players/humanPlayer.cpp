#include "humanPlayer.h"

Player *HumanPlayer::createHumanPlayer(int characterId)
{
    Player *player = nullptr;
    auto iter = s_players.find(characterId);
    if (iter != s_players.end())
    {
        player = iter.value();
    }
    else
    {
        player = new HumanPlayer(characterId);
        s_players.insert(characterId, player);
    }
    return player;
}

HumanPlayer::HumanPlayer(int characterId) : Player(characterId)
{
    m_localHumanPlayer = true;
}

void HumanPlayer::sendPlayerStartedHisTurn(int)
{

}

void HumanPlayer::playerMadeAnAccusation(int, bool, const Suggestion &)
{

}

void HumanPlayer::playerMadeASuggestion(int, const Suggestion &)
{

}

void HumanPlayer::playerIsAskedToShowAClue(int , const Suggestion &, int )
{
}

void HumanPlayer::playerAnswerToSuggestion(int, int)
{

}

void HumanPlayer::suggestionResult(const Suggestion &, int)
{

}

void HumanPlayer::playerEndedHisTurn(int, bool)
{

}

#include "remotePlayer.h"

#include <QDataStream>

RemotePlayer::RemotePlayer(int characterId, QTcpSocket *socket) : Player(characterId), m_socket(socket)
{
}

void RemotePlayer::playerThrownDice(int, int throwResult)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 9;
    dataStream << trameId;

    dataStream << (qint8)throwResult;
    m_socket->write(byteArray);
}

void RemotePlayer::playerMovedTo(int, const Coordinate &newCoordinate)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 11;
    dataStream << trameId;

    dataStream << (qint8)newCoordinate.x;
    dataStream << (qint8)newCoordinate.y;
    m_socket->write(byteArray);
}

void RemotePlayer::playerTookSecretPassage(int, const Coordinate &newCoordinate)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 13;
    dataStream << trameId;

    dataStream << (qint8)newCoordinate.x;
    dataStream << (qint8)newCoordinate.y;
    m_socket->write(byteArray);
}

void RemotePlayer::sendPlayerStartedHisTurn(int playerId)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 6;
    dataStream << trameId;

    qint16 suspectId = (qint16)playerId;
    dataStream << suspectId;

    m_socket->write(byteArray);
}

void RemotePlayer::playerMadeAnAccusation(int, bool accussationIsRight, const Suggestion &suggestion)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 17;
    dataStream << trameId;

    dataStream << (qint8)accussationIsRight;
    dataStream << (qint16)suggestion.murderer;
    dataStream << (qint16)suggestion.weapon;
    dataStream << (qint16)suggestion.room;

    m_socket->write(byteArray);
}

void RemotePlayer::playerMadeASuggestion(int, const Suggestion &suggestion)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 15;
    dataStream << trameId;

    dataStream << (qint16)suggestion.murderer;
    dataStream << (qint16)suggestion.weapon;
    dataStream << (qint16)suggestion.room;
    m_socket->write(byteArray);
}

void RemotePlayer::playerIsAskedToShowAClue(int, const Suggestion &suggestion, int playerIdWhoShouldAnswer)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 23;
    dataStream << trameId;

    dataStream << (qint16)suggestion.murderer;
    dataStream << (qint16)suggestion.weapon;
    dataStream << (qint16)suggestion.room;
    dataStream << (qint16)playerIdWhoShouldAnswer;
    m_socket->write(byteArray);
}

void RemotePlayer::playerAnswerToSuggestion(int elementIdShown, int idOfCharacterWhoAnswered)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 25;
    dataStream << trameId;

    dataStream << (qint16)elementIdShown;
    dataStream << (qint16)idOfCharacterWhoAnswered;
    m_socket->write(byteArray);

}

void RemotePlayer::suggestionResult(const Suggestion &, int idOfCharacterWhoAnswered)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 19;
    dataStream << trameId;

    dataStream << (qint16)idOfCharacterWhoAnswered;
    m_socket->write(byteArray);
}

void RemotePlayer::playerEndedHisTurn(int, bool allowed)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 21;
    dataStream << trameId;

    dataStream << (qint8)allowed;
    m_socket->write(byteArray);

}

void RemotePlayer::sendCharacterIdToClient()
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 0;
    dataStream << trameId;

    qint16 suspectId = (qint16)m_characterId;
    dataStream << suspectId;

    m_socket->write(byteArray);
}

void RemotePlayer::sendSuspectConnectionToClient(int suspectId)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 3;
    dataStream << trameId;

    dataStream << (qint16)suspectId;

    m_socket->write(byteArray);
}

void RemotePlayer::sendSuspectDisconnectionToClient(int suspectId)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 4;
    dataStream << trameId;

    dataStream << (qint16)suspectId;

    m_socket->write(byteArray);
}

void RemotePlayer::sendListOfSuspectToClient(QVector<int> suspectsIds)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 5;
    dataStream << trameId;

    qint8 nbClient;
    nbClient = (qint8)suspectsIds.size();
    dataStream << nbClient;
    for (int suspectId : suspectsIds)
    {
        dataStream << (qint16)suspectId;
    }

    m_socket->write(byteArray);
}

void RemotePlayer::sendCharacterCardsToClient()
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 1;
    dataStream << trameId;

    qint8 nbCard = m_cards.size();
    dataStream << nbCard;
    for (Card *card : m_cards)
    {
        qint16 cardId = (qint16)card->getElementId();
        dataStream << cardId;
    }

    m_socket->write(byteArray);
}

void RemotePlayer::sendGameStartedToClient()
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 2;
    dataStream << trameId;
    m_socket->write(byteArray);
}

void RemotePlayer::emitThrowDice()
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 8;
    dataStream << trameId;

    m_socket->write(byteArray);
}

void RemotePlayer::emitMoveTo(const Coordinate &coordinate)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 10;
    dataStream << trameId;

    dataStream << (qint8)coordinate.x;
    dataStream << (qint8)coordinate.y;

    m_socket->write(byteArray);
}

void RemotePlayer::emitAccusation(const Suggestion &suggestion)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 16;
    dataStream << trameId;

    dataStream << (qint16)suggestion.murderer;
    dataStream << (qint16)suggestion.weapon;
    dataStream << (qint16)suggestion.room;

    m_socket->write(byteArray);
}

void RemotePlayer::emitSuggestion(const Suggestion &suggestion)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 14;
    dataStream << trameId;

    dataStream << (qint16)suggestion.murderer;
    dataStream << (qint16)suggestion.weapon;
    dataStream << (qint16)suggestion.room;

    m_socket->write(byteArray);
}

void RemotePlayer::emitShowClue(int clueShown)
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 18;
    dataStream << trameId;

    dataStream << (qint16)clueShown;

    m_socket->write(byteArray);
}

void RemotePlayer::emitTakeSecretPassage()
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 12;
    dataStream << trameId;

    m_socket->write(byteArray);
}

void RemotePlayer::emitEndTurn()
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 20;
    dataStream << trameId;

    m_socket->write(byteArray);


}

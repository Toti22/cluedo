#ifndef HUMANPLAYER_H
#define HUMANPLAYER_H

#include <QObject>

#include "player.h"

class HumanPlayer : public Player
{
    Q_OBJECT
public:
    static Player *createHumanPlayer(int characterId);
private:
    HumanPlayer(int characterId);

    // Player interface
public:

    // Player interface
public:
    virtual void sendPlayerStartedHisTurn(int playerId) override;
    virtual void playerMadeAnAccusation(int playerId, bool accussationIsRight, const Suggestion &suggestion) override;
    virtual void playerMadeASuggestion(int playerId, const Suggestion &suggestion) override;
    virtual void playerIsAskedToShowAClue(int playerIdWhoMadeSuggestion, const Suggestion &suggestion, int playerIdWhoShouldAnswer) override;
    virtual void playerAnswerToSuggestion(int, int) override;
    virtual void suggestionResult(const Suggestion &suggestion, int idOfCharacterWhoAnswered) override;
    virtual void playerEndedHisTurn(int playerId, bool allowed = true) override;

signals:
    void hasToAnswerSuggestion(int playerIdWhoMadeSuggestion, const Suggestion &suggestion, int playerIdWhoShouldAnswer, QVector<Card *> clues);
};

#endif // HUMANPLAYER_H

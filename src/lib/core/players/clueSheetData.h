#ifndef CLUESHEETDATA_H
#define CLUESHEETDATA_H

#include <QMap>

#include "core/gameLogic/gameElement/gameElement.h"
#include "core/gameLogic/gameElement/gameElementFactory.h"

struct ClueSheetData
{
    QMap<int, bool> gameElementsCheck;

    ClueSheetData()
    {
        gameElementsCheck.insert(static_cast<int>(RoomsEnum::Ballroom), false);
        gameElementsCheck.insert(static_cast<int>(RoomsEnum::BilliardRoom), false);
        gameElementsCheck.insert(static_cast<int>(RoomsEnum::Conservatory), false);
        gameElementsCheck.insert(static_cast<int>(RoomsEnum::DiningRoom), false);
        gameElementsCheck.insert(static_cast<int>(RoomsEnum::Hall), false);
        gameElementsCheck.insert(static_cast<int>(RoomsEnum::Kitchen), false);
        gameElementsCheck.insert(static_cast<int>(RoomsEnum::Library), false);
        gameElementsCheck.insert(static_cast<int>(RoomsEnum::Lounge), false);
        gameElementsCheck.insert(static_cast<int>(RoomsEnum::Study), false);

        gameElementsCheck.insert(static_cast<int>(SuspectsEnum::Green), false);
        gameElementsCheck.insert(static_cast<int>(SuspectsEnum::Mustard), false);
        gameElementsCheck.insert(static_cast<int>(SuspectsEnum::Peacock), false);
        gameElementsCheck.insert(static_cast<int>(SuspectsEnum::Plum), false);
        gameElementsCheck.insert(static_cast<int>(SuspectsEnum::Scarlet), false);
        gameElementsCheck.insert(static_cast<int>(SuspectsEnum::White), false);

        gameElementsCheck.insert(static_cast<int>(WeaponsEnum::Candlestick), false);
        gameElementsCheck.insert(static_cast<int>(WeaponsEnum::Knife), false);
        gameElementsCheck.insert(static_cast<int>(WeaponsEnum::LeadPipe), false);
        gameElementsCheck.insert(static_cast<int>(WeaponsEnum::Poison), false);
        gameElementsCheck.insert(static_cast<int>(WeaponsEnum::Revolver), false);
        gameElementsCheck.insert(static_cast<int>(WeaponsEnum::Rope), false);
        gameElementsCheck.insert(static_cast<int>(WeaponsEnum::Wrench), false);
    }

    void reset()
    {
        QVector<GameElement *>elements = GameElementFactory::getAllGameElements();
        for (GameElement *element : elements)
        {
            gameElementsCheck[element->getId()] = false;
        }
    }
};

#endif // CLUESHEETDATA_H

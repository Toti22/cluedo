#ifndef REMOTEPLAYER_H
#define REMOTEPLAYER_H

#include "player.h"
#include <QObject>
#include <QtNetwork/QtNetwork>

class RemotePlayer : public Player
{
    Q_OBJECT
public:
    RemotePlayer(int characterId, QTcpSocket *socket);
protected:
    QTcpSocket *m_socket;

    // Player interface
public:
    virtual void playerThrownDice(int playerId, int throwResult) override;
    virtual void playerMovedTo(int playerId, const Coordinate &newCoordinate) override;
    virtual void playerTookSecretPassage(int playerId, const Coordinate &newCoordinate) override;
    virtual void sendPlayerStartedHisTurn(int playerId) override;
    virtual void playerMadeAnAccusation(int playerId, bool accussationIsRight, const Suggestion &suggestion) override;
    virtual void playerMadeASuggestion(int playerId, const Suggestion &suggestion) override;
    virtual void playerIsAskedToShowAClue(int playerIdWhoMadeSuggestion, const Suggestion &suggestion, int playerIdWhoShouldAnswer) override;
    virtual void playerAnswerToSuggestion(int elementIdShown, int idOfCharacterWhoAnswered) override;
    virtual void suggestionResult(const Suggestion &suggestion, int idOfCharacterWhoAnswered) override;
    virtual void playerEndedHisTurn(int playerId, bool allowed = true) override;

    void sendCharacterIdToClient();
    void sendSuspectConnectionToClient(int suspectId);
    void sendSuspectDisconnectionToClient(int suspectId);
    void sendListOfSuspectToClient(QVector<int> suspectsIds);
    void sendCharacterCardsToClient();
    void sendGameStartedToClient();

    QTcpSocket *getSocket() const;

public slots:

protected slots:

protected:

    // Player interface
public slots:
    virtual void emitThrowDice() override;
    virtual void emitMoveTo(const Coordinate &coordinate) override;
    virtual void emitAccusation(const Suggestion &suggestion) override;
    virtual void emitSuggestion(const Suggestion &suggestion) override;
    virtual void emitShowClue(int clueShown) override;
    virtual void emitTakeSecretPassage() override;
    virtual void emitEndTurn() override;
};

inline QTcpSocket *RemotePlayer::getSocket() const
{
    return m_socket;
}

#endif // REMOTEPLAYER_H

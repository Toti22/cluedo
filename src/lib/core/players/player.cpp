#include "player.h"

QMap<int, Player *> Player::s_players;

Player::Player(int characterId) : m_characterId(characterId), m_position(0, 0), m_eleminated(false), m_localHumanPlayer(false)
{
    GameElementSuspect *suspect = GameElementFactory::getSuspectById(characterId);
    m_position = suspect->getPosition();
}





Player *Player::getHumanPlayer(int characterId)
{
    Player *player = nullptr;
    auto iter = s_players.find(characterId);
    if (iter != s_players.end())
    {
        player = iter.value();
    }
    return player;
}

void Player::resetPlayer()
{
    m_cards.clear();
    m_clueSheetData.reset();
}

void Player::drawCard(Card *card)
{
    m_cards.push_back(card);

    m_clueSheetData.gameElementsCheck[card->getElementId()] = true;
}

void Player::drawCards(QVector<Card *> cards)
{
    for (Card *card : cards)
    {
        drawCard(card);
    }
}

void Player::playerThrownDice(int playerId, int throwResult)
{
    if (playerId == m_characterId)
    {
        m_diceResult = throwResult;
    }
}

void Player::playerMovedTo(int playerId, const Coordinate &newCoordinate)
{
    if (playerId == m_characterId)
    {
        m_position = newCoordinate;
    }
}

void Player::playerTookSecretPassage(int playerId, const Coordinate &newCoordinate)
{
    if (playerId == m_characterId)
    {
        m_position = newCoordinate;
    }
}

void Player::wrongThrowDice(const QString &)
{
    //TODO Log error
}

void Player::wrongMoveTo(const Coordinate &, const Coordinate &, const QString &)
{
    //TODO Log error
}

void Player::wrongTakeSecretPassage(const Coordinate &, const QString &)
{
    //TODO Log error
}

void Player::wrongSuggestion(const Suggestion &, const Coordinate &, const QString &)
{
    //TODO Log error
}

void Player::wrongAnswerToSuggestion(const Suggestion &, int, const QString &)
{
    //TODO Log error
}

void Player::emitThrowDice()
{
    emit throwDice();
}

void Player::emitMoveTo(const Coordinate &coordinate)
{
    emit moveTo(coordinate);
}

void Player::emitAccusation(const Suggestion &suggestion)
{
    emit makeAccusation(suggestion);
}

void Player::emitSuggestion(const Suggestion &suggestion)
{
    emit makeSuggestion(suggestion);
}

void Player::emitShowClue(int clueShown)
{
    emit showClue(clueShown);
}

void Player::emitTakeSecretPassage()
{
    emit takeSecretPassage();
}

void Player::emitEndTurn()
{
    emit endTurn();
}

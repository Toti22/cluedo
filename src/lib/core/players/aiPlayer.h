#ifndef AIPLAYER_H
#define AIPLAYER_H

#include "player.h"

class AIPlayer : public Player
{
private:
    AIPlayer(int characterId);
};

#endif // AIPLAYER_H

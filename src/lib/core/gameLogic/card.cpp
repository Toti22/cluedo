#include "card.h"

#include <algorithm>

QMap<int, Card*> Card::s_allCard;
QMap<int, Card*> Card::s_roomCard;
QMap<int, Card*> Card::s_weaponCard;
QMap<int, Card*> Card::s_suspectCard;

Card *Card::getCard(int elementIdCard)
{
    auto iter = s_allCard.find(elementIdCard);
    if (iter != s_allCard.end())
    {
        return iter.value();
    }

    return new Card(elementIdCard);
}

Card *Card::getRoomCard(RoomsEnum room)
{
    return getCard(static_cast<int>(room));
}

Card *Card::getWeaponCard(WeaponsEnum weapon)
{
    return getCard(static_cast<int>(weapon));
}

Card *Card::getSuspectCard(SuspectsEnum suspect)
{
    return getCard(static_cast<int>(suspect));
}

QVector<Card *> Card::getAllCards()
{
    QVector<Card *> cards;
    cards += getAllRoomCards();
    cards += getAllWeaponCards();
    cards += getAllSuspectCards();
    return cards;
}

QVector<Card *> Card::getAllRoomCards()
{
    QVector<Card *> cards;
    cards.push_back(getRoomCard(RoomsEnum::Ballroom));
    cards.push_back(getRoomCard(RoomsEnum::BilliardRoom));
    cards.push_back(getRoomCard(RoomsEnum::Conservatory));
    cards.push_back(getRoomCard(RoomsEnum::DiningRoom));
    cards.push_back(getRoomCard(RoomsEnum::Hall));
    cards.push_back(getRoomCard(RoomsEnum::Kitchen));
    cards.push_back(getRoomCard(RoomsEnum::Library));
    cards.push_back(getRoomCard(RoomsEnum::Lounge));
    cards.push_back(getRoomCard(RoomsEnum::Study));
    return cards;
}

QVector<Card *> Card::getAllWeaponCards()
{
    QVector<Card *> cards;
    cards.push_back(getWeaponCard(WeaponsEnum::Candlestick));
    cards.push_back(getWeaponCard(WeaponsEnum::Knife));
    cards.push_back(getWeaponCard(WeaponsEnum::LeadPipe));
    cards.push_back(getWeaponCard(WeaponsEnum::Poison));
    cards.push_back(getWeaponCard(WeaponsEnum::Revolver));
    cards.push_back(getWeaponCard(WeaponsEnum::Rope));
    cards.push_back(getWeaponCard(WeaponsEnum::Wrench));
    return cards;
}

QVector<Card *> Card::getAllSuspectCards()
{
    QVector<Card *> cards;
    cards.push_back(getSuspectCard(SuspectsEnum::Green));
    cards.push_back(getSuspectCard(SuspectsEnum::Mustard));
    cards.push_back(getSuspectCard(SuspectsEnum::Peacock));
    cards.push_back(getSuspectCard(SuspectsEnum::Plum));
    cards.push_back(getSuspectCard(SuspectsEnum::Scarlet));
    cards.push_back(getSuspectCard(SuspectsEnum::White));
    return cards;
}

bool Card::isRoomCard(Card *card)
{
    QVector<Card *> cards = getAllRoomCards();
    return std::find_if(cards.begin(), cards.end(), [card](Card *currentCard) { return card->getElementId() == currentCard->getElementId() ;}) != cards.end();
}

bool Card::isWeaponCard(Card *card)
{
    QVector<Card *> cards = getAllWeaponCards();
    return std::find_if(cards.begin(), cards.end(), [card](Card *currentCard) { return card->getElementId() == currentCard->getElementId() ;}) != cards.end();
}

bool Card::isSuspectCard(Card *card)
{
    QVector<Card *> cards = getAllSuspectCards();
    return std::find_if(cards.begin(), cards.end(), [card](Card *currentCard) { return card->getElementId() == currentCard->getElementId() ;}) != cards.end();
}

Card::Card(int elementId) : m_elementId(elementId)
{
    // ROOMS
    if (elementId == static_cast<int>(RoomsEnum::Ballroom))
    {
        m_pixmap.load(":/ballRoom.png");
    }
    else if (elementId == static_cast<int>(RoomsEnum::BilliardRoom))
    {
        m_pixmap.load(":/billiardRoom.png");
    }
    else if (elementId == static_cast<int>(RoomsEnum::Conservatory))
    {
        m_pixmap.load(":/conservatory.png");
    }
    else if (elementId == static_cast<int>(RoomsEnum::DiningRoom))
    {
        m_pixmap.load(":/diningRoom.png");
    }
    else if (elementId == static_cast<int>(RoomsEnum::Hall))
    {
        m_pixmap.load(":/hall.png");
    }
    else if (elementId == static_cast<int>(RoomsEnum::Kitchen))
    {
        m_pixmap.load(":/kitchen.png");
    }
    else if (elementId == static_cast<int>(RoomsEnum::Library))
    {
        m_pixmap.load(":/library.png");
    }
    else if (elementId == static_cast<int>(RoomsEnum::Lounge))
    {
        m_pixmap.load(":/lounge.png");
    }
    else if (elementId == static_cast<int>(RoomsEnum::Study))
    {
        m_pixmap.load(":/study.png");
    }

    // SUSPECTS
    else if (elementId == static_cast<int>(SuspectsEnum::Green))
    {
        m_pixmap.load(":/mrGreen.png");
    }
    else if (elementId == static_cast<int>(SuspectsEnum::Mustard))
    {
        m_pixmap.load(":/colonelMustard.png");
    }
    else if (elementId == static_cast<int>(SuspectsEnum::Peacock))
    {
        m_pixmap.load(":/mrsPeacock.png");
    }
    else if (elementId == static_cast<int>(SuspectsEnum::Plum))
    {
        m_pixmap.load(":/professorPlum.png");
    }
    else if (elementId == static_cast<int>(SuspectsEnum::Scarlet))
    {
        m_pixmap.load(":/missScarlet.png");
    }
    else if (elementId == static_cast<int>(SuspectsEnum::White))
    {
        m_pixmap.load(":/mrsWhite.png");
    }

    // WEAPONS
    else if (elementId == static_cast<int>(WeaponsEnum::Candlestick))
    {
        m_pixmap.load(":/candlestick.png");
    }
    else if (elementId == static_cast<int>(WeaponsEnum::Knife))
    {
        m_pixmap.load(":/dagger.png");
    }
    else if (elementId == static_cast<int>(WeaponsEnum::LeadPipe))
    {
        m_pixmap.load(":/leadPiping.png");
    }
    else if (elementId == static_cast<int>(WeaponsEnum::Poison))
    {
        m_pixmap.load(":/poison.png");
    }
    else if (elementId == static_cast<int>(WeaponsEnum::Revolver))
    {
        m_pixmap.load(":/revolver.png");
    }
    else if (elementId == static_cast<int>(WeaponsEnum::Rope))
    {
        m_pixmap.load(":/rope.png");
    }
    else if (elementId == static_cast<int>(WeaponsEnum::Wrench))
    {
        m_pixmap.load(":/spanner.png");
    }
}





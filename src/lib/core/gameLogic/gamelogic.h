#ifndef GAMELOGIC_H
#define GAMELOGIC_H

#include <QMap>
#include <QVector>
#include <QObject>
#include <QApplication>
#include <QtNetwork/QtNetwork>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QTimer>

#include "gameSettings.h"
#include "core/players/player.h"
#include "core/players/humanPlayer.h"
#include "core/players/remotePlayer.h"
#include "gameElement/gameElement.h"
#include "card.h"
#include "suggestion.h"

struct CurrentPlayerActions
{
    bool thrownDice = false;
    bool moved = false;
    bool madeSuggestion = false;
};

class GameLogic : public QObject
{
    Q_OBJECT
public:
    GameLogic(QApplication *a, bool serverMod = false);
    ~GameLogic();

    // Use default settings
    void startGame();
    void startGame(const GameSettings &settings);

    void connectToServeur(const QString &ipAdress, int port);
    void cancelLobby();

    QVector<Player *> getPlayers() const;
    void createServer();
    void requestStartGame();

    void startLocalGame();
private:
    void connectPlayersSignals();
    GameElementRoom *getRoomByCoordinate(const Coordinate &coordinate);
    Player *getPlayerById(int id);
    void playerStartHisTurn(Player *player);
    void sendCharacterIdToClient();
    RemotePlayer *getPlayerFromClient(QTcpSocket *socket);
    void gameEnded();

    GameSettings m_settings;

    QVector<Player *> m_players;
    QVector<RemotePlayer *> m_remotePlayers;
    Player *m_currentPlayerPlaying;
    int m_idcurrentPlayerPlaying;
    CurrentPlayerActions m_currentPlayerActions;
    Suggestion m_murderSolution;
    Suggestion m_currentSuggestion;
    Player *m_playerThatHasToShowClue;
    QApplication *m_app;
    QTcpServer *m_server;
    QTimer m_timer;
    QTcpSocket *m_clientSocket;
    QProcess *m_process;

private slots:
    void playerThrowDice(Player *playerPlaying = nullptr);
    void playerMoveTo(const Coordinate &positionToGoTo, Player *playerPlaying = nullptr);
    void playerMakeAccusation(const Suggestion &suggestion, Player *playerPlaying = nullptr);
    void playerMakeSuggestion(const Suggestion &suggestion, Player *playerPlaying = nullptr);
    void playerShowClue(int clue, Player *playerShowingClue = nullptr);
    void playerTakeSecretPassage(Player *playerPlaying = nullptr);
    void playerEndTurn(Player *playerPlaying = nullptr);

    void newConnection();
    void clientDisconnectedFromServer();
    void connectionToServerLostSlot();
    void dataRecievedFromServer();
    void dataRecievedFromClient(QTcpSocket *client = nullptr);

signals:
    void localGameStarted(QVector<Player *>);
    void remoteGameStarted(Player *);
    void playerThrownDiceSignal(int);
    void playerMoveToResultSignal(const Coordinate &positionToGoTo);
    void playerMakeAccusationSignal(bool heIsRight, const Suggestion &);
    void playerMakeSuggestionSignal(const Suggestion &suggestion);
    void playerTakeSecretPassageSignal(const Coordinate &positionToGoTo);
    void playerEndTurnSignal(int);
    void playerStartedTurnSignal(int);
    void playerShownClue(Suggestion, int clue, int playerWhoAnswered);
    void suggestionEnd(int playerWhoAnswered);
    void hasToAnswerSuggestion(int playerIdWhoMadeSuggestion, const Suggestion &suggestion, int playerIdWhoShouldAnswer, QVector<Card *> clues);

    void connectionFailed();
    void connectionSuccess(int suspectId);
    void connectionToServerLost();

    void aClientHasJoinServer(int suspectId);
    void aClientHasLeftServer(int suspectId);
    void clientList(QVector<int> suspectsIds);
};

#endif // GAMELOGIC_H

#ifndef GAMESETTINGS_H
#define GAMESETTINGS_H

#include <QVector>

#include "gameElement/gameElement.h"

enum class AiDifficulty { EASY, NORMAL, HARD};

struct AiPLayerSettings
{
    SuspectsEnum characterPlaying;
    AiDifficulty aiDifficulty;
};

enum class CharacterOrder { RANDOM, SAME_AS_IN_BOARD_CLOCK_WISE};

enum class WhichPlayerStartMod { RANDOM, LESS_CARD_IN_HAND, MOST_CARD_IN_HAND, SPECIFIC_CHARACTER };
// Will determine which player will have more or less card. Adapt mod let player with less card to play first
enum class FirstPlayerToDrawCardMod { ADAPT_TO_FIRST_PLAYER_TO_PLAY, RANDOM, SPECIFIC_CHARACTER };

struct GameSettings
{
    bool localGame = true;
    QVector<SuspectsEnum> charactersInPlay;
    QVector<AiPLayerSettings> aiPlayers;

    CharacterOrder characterOrder = CharacterOrder::SAME_AS_IN_BOARD_CLOCK_WISE;

    WhichPlayerStartMod whoStart = WhichPlayerStartMod::SPECIFIC_CHARACTER; // SPECIFIC_CHARACTER is official rules
    // In official rules, Scarlet always start first. If Character not in play, go random.
    SuspectsEnum specificCharacterToStart = SuspectsEnum::Scarlet;

    FirstPlayerToDrawCardMod firstPlayerToDrawCardMod = FirstPlayerToDrawCardMod::ADAPT_TO_FIRST_PLAYER_TO_PLAY;

    GameSettings()
    {
        localGame = true;
        charactersInPlay.push_back(SuspectsEnum::Scarlet);
        charactersInPlay.push_back(SuspectsEnum::Mustard);
        charactersInPlay.push_back(SuspectsEnum::White);
        charactersInPlay.push_back(SuspectsEnum::Green);
        charactersInPlay.push_back(SuspectsEnum::Peacock);
        charactersInPlay.push_back(SuspectsEnum::Plum);
    }
};

#endif // GAMESETTINGS_H

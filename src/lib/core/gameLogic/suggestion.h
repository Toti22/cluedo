#ifndef SUGGESTION_H
#define SUGGESTION_H

struct Suggestion
{
    int room;
    int weapon;
    int murderer;
};

#endif // SUGGESTION_H

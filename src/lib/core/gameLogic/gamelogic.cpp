#include "gamelogic.h"

#include <QRandomGenerator>
#include <algorithm>
#include <QProcess>
#include <QStringList>
#include <QDataStream>

GameLogic::GameLogic(QApplication *app, bool serverMod) : m_playerThatHasToShowClue(nullptr), m_app(app), m_server(nullptr), m_clientSocket(nullptr), m_process(nullptr)
{
    if (serverMod == true)
    {
        m_server = new QTcpServer(this);
        if (!m_server->listen(QHostAddress::Any, 2332))
        {
            //todo ADD_LOGS tr("Started did not start :<br />") + m_server->errorString());
        }
        else
        {
            connect(m_server, SIGNAL(newConnection()), this, SLOT(newConnection()));
        }
    }
}

GameLogic::~GameLogic()
{
    if (m_clientSocket != nullptr)
    {
        m_clientSocket->abort();
        m_clientSocket->deleteLater();
    }
    if (m_server != nullptr)
    {
        m_server->close();
        m_server->deleteLater();
    }
    if (m_process != nullptr)
    {
        m_process->close();
        m_process->deleteLater();
    }
}

void GameLogic::startGame()
{
    if (m_server != nullptr)
    {
        startGame(GameSettings());
    }
    else
    {
        requestStartGame();
    }
}

void GameLogic::startLocalGame()
{
    startGame(GameSettings());
}

void GameLogic::startGame(const GameSettings &settings)
{
    m_settings = settings;

    if (m_remotePlayers.size() == 0)
    {
        m_players.push_back(HumanPlayer::createHumanPlayer(static_cast<int>(SuspectsEnum::Scarlet)));
        m_players.push_back(HumanPlayer::createHumanPlayer(static_cast<int>(SuspectsEnum::Mustard)));
        m_players.push_back(HumanPlayer::createHumanPlayer(static_cast<int>(SuspectsEnum::White)));
        m_players.push_back(HumanPlayer::createHumanPlayer(static_cast<int>(SuspectsEnum::Green)));
        m_players.push_back(HumanPlayer::createHumanPlayer(static_cast<int>(SuspectsEnum::Peacock)));
        m_players.push_back(HumanPlayer::createHumanPlayer(static_cast<int>(SuspectsEnum::Plum)));
    }
    for (Player *player : m_players)
    {
        if (player != nullptr)
        {
            player->resetPlayer();
        }
    }

    QVector<Card *> roomCards = Card::getAllRoomCards();
    QVector<Card *> weaponCards = Card::getAllWeaponCards();
    QVector<Card *> suspectCards = Card::getAllSuspectCards();

    int randomSuspect = QRandomGenerator::global()->bounded(0, suspectCards.size());
    int randomRoom = QRandomGenerator::global()->bounded(0, roomCards.size());
    int randomWeapon = QRandomGenerator::global()->bounded(0, weaponCards.size());

    m_murderSolution.murderer = suspectCards.at(randomSuspect)->getElementId();
    m_murderSolution.room = roomCards.at(randomRoom)->getElementId();
    m_murderSolution.weapon = weaponCards.at(randomWeapon)->getElementId();

    suspectCards.removeAt(randomSuspect);
    roomCards.removeAt(randomRoom);
    weaponCards.removeAt(randomWeapon);

    QVector<Card *> cardsToDraw;
    for (Card *card : roomCards)
    {
        cardsToDraw.push_back(card);
    }
    for (Card *card : weaponCards)
    {
        cardsToDraw.push_back(card);
    }
    for (Card *card : suspectCards)
    {
        cardsToDraw.push_back(card);
    }

    int cardDrawnCount = 0;
    while (cardsToDraw.size() != 0)
    {
        int cardToDrawIndex = QRandomGenerator::global()->bounded(0, cardsToDraw.size());
        int playerIndex = cardDrawnCount % m_players.size();

        Player *player = m_players.at(playerIndex);
        Card *cardToDraw = cardsToDraw.at(cardToDrawIndex);
        player->drawCard(cardToDraw);

        cardsToDraw.removeAt(cardToDrawIndex);
        cardDrawnCount++;
    }

    connectPlayersSignals();

    if (m_remotePlayers.size() == 0)
    {
        emit localGameStarted(m_players);
    }
    else
    {
        for (RemotePlayer *player : m_remotePlayers)
        {
            player->sendCharacterCardsToClient();
            player->sendGameStartedToClient();
        }
    }

    playerStartHisTurn(m_players[0]);
}

void GameLogic::connectToServeur(const QString &ipAdress, int port)
{
    if (m_clientSocket != nullptr)
    {
        m_clientSocket->deleteLater();
    }
    m_clientSocket = new QTcpSocket(this);
    connect(m_clientSocket, SIGNAL(readyRead()), this, SLOT(dataRecievedFromServer()));
    connect(m_clientSocket, SIGNAL(disconnected()), this, SLOT(connectionToServerLostSlot()));
    m_clientSocket->connectToHost(ipAdress, port);
    if (m_clientSocket->isOpen() != true)
    {
        emit connectionFailed();
    }
}

void GameLogic::cancelLobby()
{
    if (m_clientSocket != nullptr)
    {
        m_clientSocket->deleteLater();
        m_clientSocket = nullptr;
    }
    if (m_process != nullptr)
    {
        m_process->close();
        m_process->deleteLater();
        m_process = nullptr;
    }
    for (Player *player : m_players)
    {
        player->deleteLater();
    }
    m_remotePlayers.clear();
    m_players.clear();
}

void GameLogic::createServer()
{
    if (m_process != nullptr)
    {
        m_process->close();
        m_process->deleteLater();
    }
    m_process = new QProcess(this);
    QStringList arguments;
    arguments << "SERVER";

    m_process->setArguments(arguments);
    m_process->start(m_app->applicationFilePath(), arguments);

    m_clientSocket = new QTcpSocket(this);
    connect(m_clientSocket, SIGNAL(disconnected()), this, SLOT(connectionToServerLostSlot()));
    connect(m_clientSocket, SIGNAL(readyRead()), this, SLOT(dataRecievedFromServer()));

    int nbAttemp = 0;
    connect(&m_timer, &QTimer::timeout, [&nbAttemp, this]()
    {
        m_clientSocket->connectToHost("127.0.0.1", 2332);
        if (!m_clientSocket->isOpen() && nbAttemp > 5)
        {
            m_timer.stop();
            m_clientSocket->deleteLater();
            m_clientSocket = nullptr;
            emit connectionFailed();
        }
        else if (m_clientSocket->isOpen() == true)
        {
            m_timer.stop();
        }
        nbAttemp++;
    });

    m_timer.setInterval(500);
    m_timer.start();
}

void GameLogic::connectPlayersSignals()
{
    for (Player *player : m_players)
    {
        connect(player, SIGNAL(endTurn()), this, SLOT(playerEndTurn()));
        connect(player, SIGNAL(makeAccusation(Suggestion)), this, SLOT(playerMakeAccusation(Suggestion)));
        connect(player, SIGNAL(makeSuggestion(Suggestion)), this, SLOT(playerMakeSuggestion(Suggestion)));
        connect(player, SIGNAL(moveTo(Coordinate)), this, SLOT(playerMoveTo(Coordinate)));
        connect(player, SIGNAL(takeSecretPassage()), this, SLOT(playerTakeSecretPassage()));
        connect(player, SIGNAL(throwDice()), this, SLOT(playerThrowDice()));
        connect(player, SIGNAL(showClue(int)), this, SLOT(playerShowClue(int)));
    }
}

GameElementRoom *GameLogic::getRoomByCoordinate(const Coordinate &coordinate)
{
    for (GameElementRoom *room : GameElementFactory::getRooms())
    {
        if (room->getPosition() == coordinate)
        {
            return room;
        }
    }
    return nullptr;
}

Player *GameLogic::getPlayerById(int id)
{
    Player *player = nullptr;
    auto iter = std::find_if(m_players.begin(), m_players.end(), [id](Player *player) { return id == player->getCharacterId() ;});
    if (iter != m_players.end())
    {
        player = *iter;
    }

    return player;
}

void GameLogic::playerStartHisTurn(Player *player)
{
    m_currentPlayerPlaying = player;
    for (Player *player : m_players)
    {
        player->sendPlayerStartedHisTurn(m_currentPlayerPlaying->getCharacterId());
    }

    emit playerStartedTurnSignal(m_currentPlayerPlaying->getCharacterId());
}

RemotePlayer *GameLogic::getPlayerFromClient(QTcpSocket *socket)
{
    for (RemotePlayer *remotePlayer : m_remotePlayers)
    {
        if (remotePlayer->getSocket() == socket)
        {
            return remotePlayer;
        }
    }
    return nullptr;
}

void GameLogic::gameEnded()
{
    for (Player *player : m_players)
    {
        if (player != nullptr)
        {
            player->resetPlayer();
        }
    }

    m_players.clear();
    m_remotePlayers.clear();

    if (m_clientSocket != nullptr)
    {
        m_clientSocket->abort();
        m_clientSocket->deleteLater();
        m_clientSocket = nullptr;
    }
    if (m_server != nullptr)
    {
        m_server->deleteLater();
        m_server = nullptr;
    }
    if (m_process != nullptr)
    {
        m_process->close();
        m_process->deleteLater();
        m_process = nullptr;
    }
}

void GameLogic::playerThrowDice(Player *playerPlaying)
{
    if (playerPlaying == nullptr)
    {
        playerPlaying = static_cast<Player *>(sender());
    }
    if (playerPlaying != m_currentPlayerPlaying ||
        m_currentPlayerActions.thrownDice == true ||
        m_currentPlayerActions.moved == true ||
        m_currentPlayerActions.madeSuggestion == true)
    {
        return;
    }

    m_currentPlayerActions.thrownDice = true;

    int result = QRandomGenerator::global()->bounded(1, 7) + QRandomGenerator::global()->bounded(1, 7);
    for (Player *player : m_players)
    {
        player->playerThrownDice(playerPlaying->getCharacterId(), result);
    }

    emit playerThrownDiceSignal(result);
}

void GameLogic::playerMoveTo(const Coordinate &positionToGoTo, Player *playerPlaying)
{
    if (playerPlaying == nullptr)
    {
        playerPlaying = static_cast<Player *>(sender());
    }
    if (playerPlaying != m_currentPlayerPlaying || m_currentPlayerActions.thrownDice == false || m_currentPlayerActions.moved == true)
    {
        return;
    }

    Coordinate positionToGoToResponse = positionToGoTo;

    //Todo check if player is allowed to go there
    //Player can't sit on door's position. If he is on one, his pos is set to the room location.
    QVector<GameElementRoom *> rooms = GameElementFactory::getRooms();
    for (GameElementRoom *room : rooms)
    {
        QVector<GameElementDoor> doors = room->getDoors();
        for (GameElementDoor &door : doors)
        {
            if (door.m_position == positionToGoTo)
            {
                positionToGoToResponse = room->getPosition();
            }
        }
    }
    playerPlaying->setPosition(positionToGoToResponse);

    for (Player *player : m_players)
    {
        player->playerMovedTo(playerPlaying->getCharacterId(), positionToGoToResponse);
    }

    emit playerMoveToResultSignal(positionToGoToResponse);
}

void GameLogic::playerMakeAccusation(const Suggestion &suggestion, Player *playerPlaying)
{
    if (playerPlaying == nullptr)
    {
        playerPlaying = static_cast<Player *>(sender());
    }
    if (playerPlaying != m_currentPlayerPlaying || playerPlaying->getEleminated() == true)
    {
        return;
    }

    bool heIsRight = suggestion.murderer == m_murderSolution.murderer &&
                     suggestion.room == m_murderSolution.room &&
                     suggestion.weapon == m_murderSolution.weapon;

    for (Player *player : m_players)
    {
        player->playerMadeAnAccusation(playerPlaying->getCharacterId(), heIsRight, suggestion);
    }

    emit playerMakeAccusationSignal(heIsRight, suggestion);

    if (heIsRight == false)
    {
        playerPlaying->setEleminated(true);
        playerEndTurn(playerPlaying);
    }
    else
    {
        gameEnded();
    }
}

void GameLogic::playerMakeSuggestion(const Suggestion &suggestion, Player *playerPlaying)
{
    if (playerPlaying == nullptr)
    {
        playerPlaying = static_cast<Player *>(sender());
    }
    if (playerPlaying != m_currentPlayerPlaying || m_currentPlayerActions.madeSuggestion == true)
    {
        return;
    }
    m_currentPlayerActions.madeSuggestion = true;
    //Todo check if player is in the room of the suggestion
    GameElementRoom *room = getRoomByCoordinate(playerPlaying->getPosition());

    if (room == nullptr || room->getId() != suggestion.room)
    {
        playerPlaying->wrongSuggestion(suggestion, playerPlaying->getPosition(), "Not in the room of suggestion.");
        return;
    }

    //set player in suggestion, in the room of the suggestion
    Player * playerInSuggestion = getPlayerById(suggestion.murderer);
    if (playerInSuggestion != nullptr)
    {
        playerInSuggestion->setPosition(room->getPosition());
    }

    for (Player *player : m_players)
    {
        player->playerMadeASuggestion(playerPlaying->getCharacterId(), suggestion);
    }

    emit playerMakeSuggestionSignal(suggestion);

    int playerPlayingIndex = 0;
    for (int i = 0 ; i < m_players.size() ; ++i)
    {
        Player *player = m_players.at(i);
        if (player->getCharacterId() == playerPlaying->getCharacterId())
        {
            playerPlayingIndex = i;
        }
    }
    int currentIndex = playerPlayingIndex - 1;
    if (currentIndex < 0)
    {
        currentIndex = m_players.size() - 1;
    }
    Player *playerThatHasAClue = nullptr;
    int compt = 0;
    QVector<Card *> clues;
    while (compt < m_players.size() && playerThatHasAClue == nullptr)
    {
        Player *player = m_players.at(currentIndex);
        QVector<Card *> playerCards = player->getCards();
        bool hasAClue = false;
        for (Card *card : playerCards)
        {
            if (card->getElementId() == suggestion.murderer ||
                card->getElementId() == suggestion.room ||
                card->getElementId() == suggestion.weapon)
            {
                hasAClue = true;
                clues.push_back(card);
            }
        }
        if (hasAClue == true)
        {
            playerThatHasAClue = player;
        }
        ++compt;
        currentIndex = currentIndex - 1;
        if (currentIndex < 0)
        {
            currentIndex = m_players.size() - 1;
        }
    }

    for (Player *player : m_players)
    {
        if (playerThatHasAClue == nullptr)
        {
            // playerShowingclue = -1 means that the game is over. Suggestion is the answer.
            player->suggestionResult(m_currentSuggestion, -1);
        }
        else if (playerThatHasAClue == m_currentPlayerPlaying)
        {
            player->suggestionResult(m_currentSuggestion, playerThatHasAClue->getCharacterId());
        }
        else
        {
            emit hasToAnswerSuggestion(playerPlaying->getCharacterId(), suggestion, playerThatHasAClue->getCharacterId(), clues);
            player->playerIsAskedToShowAClue(playerPlaying->getCharacterId(), suggestion, playerThatHasAClue->getCharacterId());
            m_playerThatHasToShowClue = playerThatHasAClue;
            m_currentSuggestion = suggestion;
        }
    }
    gameEnded();
}

void GameLogic::playerShowClue(int clue, Player *playerShowingClue)
{
    if (playerShowingClue == nullptr)
    {
        playerShowingClue = static_cast<Player *>(sender());
    }
    if (m_playerThatHasToShowClue == nullptr || playerShowingClue != m_playerThatHasToShowClue)
    {
        return;
    }
    bool playerGotClueInHand = false;
    for (Card *card : playerShowingClue->getCards())
    {
        if (card->getElementId() == clue)
        {
            playerGotClueInHand = true;
        }
    }
    if(playerGotClueInHand == false)
    {
        playerShowingClue->wrongAnswerToSuggestion(m_currentSuggestion, clue, "Clue is not in player hand. Smeel like cheat right here");
        return;
    }

    for (Player * player : m_players)
    {
        if (player == m_currentPlayerPlaying)
        {
            player->playerAnswerToSuggestion(clue, playerShowingClue->getCharacterId());
        }
        else
        {
            player->suggestionResult(m_currentSuggestion, playerShowingClue->getCharacterId());
        }
    }

    emit playerShownClue(m_currentSuggestion, clue, m_playerThatHasToShowClue->getCharacterId());

    m_playerThatHasToShowClue = nullptr;
}

void GameLogic::playerTakeSecretPassage(Player *)
{

}

void GameLogic::playerEndTurn(Player *playerPlaying)
{
    if (playerPlaying == nullptr)
    {
        playerPlaying = static_cast<Player *>(sender());
    }
    //TODO don't allow player to end turn if he hasn't moved and made a suggestion if he is inside a room
    if (playerPlaying != m_currentPlayerPlaying)
    {
        return;
    }

    for (Player *player : m_players)
    {
        player->playerEndedHisTurn(playerPlaying->getCharacterId());
    }
    emit playerEndTurnSignal(playerPlaying->getCharacterId());
    m_currentPlayerActions = CurrentPlayerActions();

    int currentPLayerIndex = 0;
    for (int i = 0 ; i < m_players.size() ; ++i)
    {
        Player *player = m_players.at(i);
        if (player->getCharacterId() == m_currentPlayerPlaying->getCharacterId())
        {
            currentPLayerIndex = i;
        }
    }

    Player *nextPlayer = nullptr;
    for (int i = 0 ; i < m_players.size() ; ++i)
    {
        int currentPlayerId = (currentPLayerIndex + 1 + i) % m_players.size();
        Player *currentPlayer = m_players.at(currentPlayerId);
        if (currentPlayer->getEleminated() == false)
        {
            nextPlayer = currentPlayer;
            break;
        }
    }

    if (nextPlayer != nullptr)
    {
        playerStartHisTurn(nextPlayer);
    }
}

void GameLogic::newConnection()
{
    QVector<int> freeCharactersIds;
    QVector<GameElementSuspect *> suspects = GameElementFactory::getSuspects();
    for (GameElementSuspect *suspect : suspects)
    {
        freeCharactersIds.push_back(suspect->getId());
    }

    for (RemotePlayer *remotePlayer : m_remotePlayers)
    {
        freeCharactersIds.removeOne(remotePlayer->getCharacterId());
    }

    QTcpSocket *newClient = m_server->nextPendingConnection();
    connect(newClient, SIGNAL(disconnected()), this, SLOT(clientDisconnectedFromServer()));

    if (freeCharactersIds.size() <= 0)
    {
        newClient->abort();
        return;
    }

    connect(newClient, SIGNAL(readyRead()), this, SLOT(dataRecievedFromClient()));

    int result = QRandomGenerator::global()->bounded(0, freeCharactersIds.size());
    int characterId = freeCharactersIds[result];

    for (RemotePlayer *remotePlayer : m_remotePlayers)
    {
        remotePlayer->sendSuspectConnectionToClient(characterId);
    }

    QVector<int> suspetsIds;
    for (RemotePlayer *remotePlayer : m_remotePlayers)
    {
        suspetsIds.push_back(remotePlayer->getCharacterId());
    }

    RemotePlayer *player = new RemotePlayer(characterId, newClient);
    m_remotePlayers.push_back(player);
    m_players.push_back(player);
    player->sendCharacterIdToClient();
    player->sendListOfSuspectToClient(suspetsIds);

    emit connectionSuccess(characterId);
}

void GameLogic::clientDisconnectedFromServer()
{
    QTcpSocket *disconnectedClient = (QTcpSocket *)sender();

    RemotePlayer *remotePlayer = nullptr;

    for (int i = 0 ; i <  m_remotePlayers.size() ; ++i)
    {
        RemotePlayer *p = m_remotePlayers.at(i);
        if (p->getSocket() == disconnectedClient)
        {
            remotePlayer = p;
            m_remotePlayers.removeAt(i);
            break;
        }
    }

    if (remotePlayer == nullptr)
    {
        return;
    }

    for (int i = 0 ; i < m_players.size() ; ++i)
    {
        Player *p = m_players.at(i);
        if (p == remotePlayer)
        {
            m_players.removeAt(i);
            break;
        }
    }

    emit aClientHasLeftServer(remotePlayer->getCharacterId());
    for (RemotePlayer *p : m_remotePlayers)
    {
        p->sendSuspectDisconnectionToClient(remotePlayer->getCharacterId());
    }
    disconnectedClient->deleteLater();
}

void GameLogic::connectionToServerLostSlot()
{
    emit connectionToServerLost();
}

void GameLogic::dataRecievedFromServer()
{
    QDataStream in(m_clientSocket);
    qint8 idTrame;
    in >> idTrame;
    if (idTrame == 0)
    {
        qint16 suspectId;
        in >> suspectId;
        RemotePlayer *player = new RemotePlayer(suspectId, m_clientSocket);
        player->setLocalHumanPlayer(true);
        m_players.push_back(player);
        m_remotePlayers.push_back(player);
        emit connectionSuccess(suspectId);
    }
    else if (idTrame == 1)
    {
        qint8 nbCard;
        in >> nbCard;
        QVector<Card *> cards;
        for (int i = 0 ; i < (int)nbCard ; ++i)
        {
            qint16 cardId;
            in >> cardId;
            Card *card = Card::getCard(cardId);
            cards.push_back(card);
        }
        m_remotePlayers.last()->drawCards(cards);
    }
    else if (idTrame == 2)
    {
        RemotePlayer *player = m_remotePlayers.last();
        emit remoteGameStarted(player);
    }
    else if (idTrame == 3)
    {
        qint16 suspectId;
        in >> suspectId;

        emit aClientHasJoinServer(suspectId);
    }
    else if (idTrame == 4)
    {
        qint16 suspectId;
        in >> suspectId;

        emit aClientHasLeftServer(suspectId);
    }
    else if (idTrame == 5)
    {
        qint8 nbSuspect;
        in >> nbSuspect;
        QVector<int> suspectsIds;
        for (int i = 0 ; i < (int)nbSuspect ; ++i)
        {
            qint16 suspectId;
            in >> suspectId;
            suspectsIds.push_back((int)suspectId);
        }
        emit clientList(suspectsIds);
    }
    else if (idTrame == 6)
    {
        qint16 suspectId;
        in >> suspectId;

        m_idcurrentPlayerPlaying = (int) suspectId;
        emit playerStartedTurnSignal(m_idcurrentPlayerPlaying);
    }
    else if (idTrame == 9)
    {
        qint8 diceResult;
        in >> diceResult;

        RemotePlayer *remotePlayer = m_remotePlayers.last();
        if (remotePlayer != nullptr && remotePlayer->getCharacterId() == m_idcurrentPlayerPlaying)
        {
            emit playerThrownDiceSignal((int)diceResult);
        }
    }
    else if (idTrame == 11)
    {
        qint8 x;
        in >> x;
        qint8 y;
        in >> y;
        Coordinate coord((int)x, (int)y);

        emit playerMoveToResultSignal(coord);
    }
    else if (idTrame == 13)
    {
        qint8 x;
        in >> x;
        qint8 y;
        in >> y;
        Coordinate coord((int)x, (int)y);

        emit playerTakeSecretPassageSignal(coord);
    }
    else if (idTrame == 15)
    {
        Suggestion suggestion;
        qint16 suspect;
        in >> suspect;
        qint16 weapon;
        in >> weapon;
        qint16 room;
        in >> room;
        suggestion.murderer = (int)suspect;
        suggestion.weapon = (int)weapon;
        suggestion.room = (int)room;

        m_currentSuggestion = suggestion;

        emit playerMakeSuggestionSignal(suggestion);
    }
    else if (idTrame == 17)
    {
        qint8 heIsRight;
        in >> heIsRight;

        qint16 suspect;
        in >> suspect;
        qint16 weapon;
        in >> weapon;
        qint16 room;
        in >> room;

        Suggestion suggestion;
        suggestion.murderer = (int)suspect;
        suggestion.weapon = (int)weapon;
        suggestion.room = (int)room;

        emit playerMakeAccusationSignal((bool)heIsRight, suggestion);

        if ((bool)heIsRight == true)
        {
            gameEnded();
        }
    }
    else if (idTrame == 19)
    {
        qint16 playerWhoAnswered;
        in >> playerWhoAnswered;

        emit suggestionEnd((int)playerWhoAnswered);

        if ((int)playerWhoAnswered < 0)
        {
            gameEnded();
        }
    }
    else if (idTrame == 21)
    {
        qint8 endTurnOk;
        in >> endTurnOk;

        emit playerEndTurnSignal(m_idcurrentPlayerPlaying);
    }
    else if (idTrame == 23)
    {
        Suggestion suggestion;
        qint16 suspect;
        in >> suspect;
        qint16 weapon;
        in >> weapon;
        qint16 room;
        in >> room;
        suggestion.murderer = (int)suspect;
        suggestion.weapon = (int)weapon;
        suggestion.room = (int)room;

        qint16 idPlayer;
        in >> idPlayer;

        QVector<Card *> clues;
        Player *player = getPlayerById((int)idPlayer);
        if (player != nullptr)
        {
            QVector<Card *> playerCards = player->getCards();
            for (Card *card : playerCards)
            {
                if (card->getElementId() == suggestion.murderer ||
                    card->getElementId() == suggestion.room ||
                    card->getElementId() == suggestion.weapon)
                {
                    clues.push_back(card);
                }
            }
        }

        emit hasToAnswerSuggestion(m_idcurrentPlayerPlaying, suggestion, (int)idPlayer, clues);
    }
    else if (idTrame == 25)
    {
        qint16 clueId;
        in >> clueId;

        qint16 playerWhoAnswered;
        in >> playerWhoAnswered;

        emit playerShownClue(m_currentSuggestion, (int)clueId,(int)playerWhoAnswered);
    }


    if (m_clientSocket != nullptr && m_clientSocket->atEnd() != true)
    {
        dataRecievedFromServer();
    }
}

void GameLogic::dataRecievedFromClient(QTcpSocket *client)
{
    if (client == nullptr)
    {
        client = (QTcpSocket *)sender();
    }
    RemotePlayer *remotePlayer = getPlayerFromClient(client);
    if (remotePlayer == nullptr)
    {
        return;
    }

    QDataStream in(client);
    qint8 idTrame;
    in >> idTrame;

    if (idTrame == 7)
    {
        startGame();
    }
    else if (idTrame == 8)
    {
        playerThrowDice(remotePlayer);
    }
    else if (idTrame == 10)
    {
        qint8 x;
        in >> x;
        qint8 y;
        in >> y;
        Coordinate coord((int)x, (int)y);
        playerMoveTo(coord, remotePlayer);
    }
    else if (idTrame == 12)
    {
        playerTakeSecretPassage(remotePlayer);
    }
    else if (idTrame == 14)
    {
        Suggestion suggestion;
        qint16 suspect;
        in >> suspect;
        qint16 weapon;
        in >> weapon;
        qint16 room;
        in >> room;
        suggestion.murderer = (int)suspect;
        suggestion.weapon = (int)weapon;
        suggestion.room = (int)room;

        playerMakeSuggestion(suggestion, remotePlayer);
    }
    else if (idTrame == 16)
    {
        Suggestion suggestion;
        qint16 suspect;
        in >> suspect;
        qint16 weapon;
        in >> weapon;
        qint16 room;
        in >> room;
        suggestion.murderer = (int)suspect;
        suggestion.weapon = (int)weapon;
        suggestion.room = (int)room;

        playerMakeAccusation(suggestion, remotePlayer);
    }
    else if (idTrame == 18)
    {
        qint16 clueId;
        in >> clueId;
        playerShowClue((int)clueId, remotePlayer);
    }
    else if (idTrame == 20)
    {
        playerEndTurn(remotePlayer);
    }
    if (client->atEnd() != true)
    {
        dataRecievedFromClient(client);
    }
}

void GameLogic::requestStartGame()
{
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    qint8 trameId = 7;
    dataStream << trameId;

    m_clientSocket->write(byteArray);
}

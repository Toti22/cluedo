#include "gameElementFactory.h"

QMap<int, GameElementRoom*> GameElementFactory::s_rooms;
QMap<int, GameElementSuspect*> GameElementFactory::s_suspects;
QMap<int, GameElementWeapon*> GameElementFactory::s_weapons;

GameElementRoom *GameElementFactory::getRoomById(int id)
{
    auto iter = s_rooms.find(id);
    if (iter != s_rooms.end())
    {
        return iter.value();
    }

    GameElementRoom *returnValue = nullptr;
    if (id == static_cast<int>(RoomsEnum::Ballroom))
    {
        QVector<GameElementDoor> doors;
        GameElementDoor door;
        door.m_directionToLeaveRoom = Direction::LEFT;
        door.m_position = Coordinate(8, 5);
        doors.push_back(door);
        door.m_directionToLeaveRoom = Direction::RIGHT;
        door.m_position = Coordinate(15, 5);
        doors.push_back(door);
        door.m_directionToLeaveRoom = Direction::DOWN;
        door.m_position = Coordinate(9, 7);
        doors.push_back(door);
        door.m_directionToLeaveRoom = Direction::DOWN;
        door.m_position = Coordinate(14, 7);
        doors.push_back(door);

        QVector<GameElementSpotInside> spots;
        spots.push_back(GameElementSpotInside(Coordinate(10, 3)));
        spots.push_back(GameElementSpotInside(Coordinate(11, 3)));
        spots.push_back(GameElementSpotInside(Coordinate(12, 3)));
        spots.push_back(GameElementSpotInside(Coordinate(13, 3)));
        spots.push_back(GameElementSpotInside(Coordinate(10, 4)));
        spots.push_back(GameElementSpotInside(Coordinate(11, 4)));

        returnValue = new GameElementRoom(id, "Ballroom", "Ballroom", Coordinate(11, 1), spots);
        returnValue->addDoors(doors);
    }
    else if (id == static_cast<int>(RoomsEnum::BilliardRoom))
    {
        QVector<GameElementDoor> doors;
        GameElementDoor door;
        door.m_directionToLeaveRoom = Direction::LEFT;
        door.m_position = Coordinate(18, 9);
        doors.push_back(door);
        door.m_directionToLeaveRoom = Direction::DOWN;
        door.m_position = Coordinate(22, 12);
        doors.push_back(door);

        QVector<GameElementSpotInside> spots;
        spots.push_back(GameElementSpotInside(Coordinate(19, 10)));
        spots.push_back(GameElementSpotInside(Coordinate(20, 10)));
        spots.push_back(GameElementSpotInside(Coordinate(21, 10)));
        spots.push_back(GameElementSpotInside(Coordinate(22, 10)));
        spots.push_back(GameElementSpotInside(Coordinate(20, 11)));
        spots.push_back(GameElementSpotInside(Coordinate(21, 11)));

        returnValue = new GameElementRoom(id, "BilliardRoom", "Billiard Room", Coordinate(23, 10), spots);
        returnValue->addDoors(doors);
    }
    else if (id == static_cast<int>(RoomsEnum::Conservatory))
    {
        GameElementDoor door;
        door.m_directionToLeaveRoom = Direction::DOWN;
        door.m_position = Coordinate(18, 4);

        QVector<GameElementSpotInside> spots;
        spots.push_back(GameElementSpotInside(Coordinate(19, 3)));
        spots.push_back(GameElementSpotInside(Coordinate(20, 3)));
        spots.push_back(GameElementSpotInside(Coordinate(21, 3)));
        spots.push_back(GameElementSpotInside(Coordinate(22, 3)));
        spots.push_back(GameElementSpotInside(Coordinate(20, 4)));
        spots.push_back(GameElementSpotInside(Coordinate(21, 4)));

        returnValue = new GameElementRoom(id, "Conservatory", "Conservatory", Coordinate(23, 3), spots);
        returnValue->addDoor(door);
    }
    else if (id == static_cast<int>(RoomsEnum::DiningRoom))
    {
        QVector<GameElementDoor> doors;
        GameElementDoor door;
        door.m_directionToLeaveRoom = Direction::RIGHT;
        door.m_position = Coordinate(7, 12);
        doors.push_back(door);
        door.m_directionToLeaveRoom = Direction::DOWN;
        door.m_position = Coordinate(6, 15);
        doors.push_back(door);

        QVector<GameElementSpotInside> spots;
        spots.push_back(GameElementSpotInside(Coordinate(2, 12)));
        spots.push_back(GameElementSpotInside(Coordinate(3, 12)));
        spots.push_back(GameElementSpotInside(Coordinate(4, 12)));
        spots.push_back(GameElementSpotInside(Coordinate(5, 12)));
        spots.push_back(GameElementSpotInside(Coordinate(3, 13)));
        spots.push_back(GameElementSpotInside(Coordinate(4, 13)));

        returnValue = new GameElementRoom(id, "DiningRoom", "DiningRoom", Coordinate(0, 11), spots);
        returnValue->addDoors(doors);
    }
    else if (id == static_cast<int>(RoomsEnum::Hall))
    {
        QVector<GameElementDoor> doors;
        GameElementDoor door;
        door.m_directionToLeaveRoom = Direction::UP;
        door.m_position = Coordinate(11, 18);
        doors.push_back(door);
        door.m_directionToLeaveRoom = Direction::UP;
        door.m_position = Coordinate(12, 18);
        doors.push_back(door);
        door.m_directionToLeaveRoom = Direction::RIGHT;
        door.m_position = Coordinate(14, 20);
        doors.push_back(door);

        QVector<GameElementSpotInside> spots;
        spots.push_back(GameElementSpotInside(Coordinate(10, 21)));
        spots.push_back(GameElementSpotInside(Coordinate(11, 21)));
        spots.push_back(GameElementSpotInside(Coordinate(12, 21)));
        spots.push_back(GameElementSpotInside(Coordinate(13, 21)));
        spots.push_back(GameElementSpotInside(Coordinate(11, 22)));
        spots.push_back(GameElementSpotInside(Coordinate(12, 22)));

        returnValue = new GameElementRoom(id, "Hall", "Hall", Coordinate(11, 20), spots);
        returnValue->addDoors(doors);
    }
    else if (id == static_cast<int>(RoomsEnum::Kitchen))
    {
        GameElementDoor door;
        door.m_directionToLeaveRoom = Direction::DOWN;
        door.m_position = Coordinate(4, 6);

        QVector<GameElementSpotInside> spots;
        spots.push_back(GameElementSpotInside(Coordinate(1, 3)));
        spots.push_back(GameElementSpotInside(Coordinate(2, 3)));
        spots.push_back(GameElementSpotInside(Coordinate(3, 3)));
        spots.push_back(GameElementSpotInside(Coordinate(4, 3)));
        spots.push_back(GameElementSpotInside(Coordinate(2, 4)));
        spots.push_back(GameElementSpotInside(Coordinate(3, 4)));

        returnValue = new GameElementRoom(id, "Kitchen", "Kitchen", Coordinate(1, 1), spots);
        returnValue->addDoor(door);
    }
    else if (id == static_cast<int>(RoomsEnum::Library))
    {
        QVector<GameElementDoor> doors;
        GameElementDoor door;
        door.m_directionToLeaveRoom = Direction::LEFT;
        door.m_position = Coordinate(17, 16);
        doors.push_back(door);
        door.m_directionToLeaveRoom = Direction::UP;
        door.m_position = Coordinate(20, 14);
        doors.push_back(door);

        QVector<GameElementSpotInside> spots;
        spots.push_back(GameElementSpotInside(Coordinate(19, 16)));
        spots.push_back(GameElementSpotInside(Coordinate(20, 16)));
        spots.push_back(GameElementSpotInside(Coordinate(21, 16)));
        spots.push_back(GameElementSpotInside(Coordinate(19, 17)));
        spots.push_back(GameElementSpotInside(Coordinate(20, 17)));
        spots.push_back(GameElementSpotInside(Coordinate(21, 17)));

        returnValue = new GameElementRoom(id, "Library", "Library", Coordinate(23, 16), spots);
        returnValue->addDoors(doors);
    }
    else if (id == static_cast<int>(RoomsEnum::Lounge))
    {
        GameElementDoor door;
        door.m_directionToLeaveRoom = Direction::UP;
        door.m_position = Coordinate(6, 19);

        QVector<GameElementSpotInside> spots;
        spots.push_back(GameElementSpotInside(Coordinate(2, 21)));
        spots.push_back(GameElementSpotInside(Coordinate(3, 21)));
        spots.push_back(GameElementSpotInside(Coordinate(4, 21)));
        spots.push_back(GameElementSpotInside(Coordinate(2, 22)));
        spots.push_back(GameElementSpotInside(Coordinate(3, 22)));
        spots.push_back(GameElementSpotInside(Coordinate(4, 22)));

        returnValue = new GameElementRoom(id, "Lounge", "Lounge", Coordinate(0, 24), spots);
        returnValue->addDoor(door);
    }
    else if (id == static_cast<int>(RoomsEnum::Study))
    {
        GameElementDoor door;
        door.m_directionToLeaveRoom = Direction::UP;
        door.m_position = Coordinate(17, 21);

        QVector<GameElementSpotInside> spots;
        spots.push_back(GameElementSpotInside(Coordinate(19, 23)));
        spots.push_back(GameElementSpotInside(Coordinate(20, 23)));
        spots.push_back(GameElementSpotInside(Coordinate(21, 23)));
        spots.push_back(GameElementSpotInside(Coordinate(19, 24)));
        spots.push_back(GameElementSpotInside(Coordinate(20, 24)));
        spots.push_back(GameElementSpotInside(Coordinate(21, 24)));

        returnValue = new GameElementRoom(id, "Study", "Study", Coordinate(23, 24), spots);
        returnValue->addDoor(door);
    }

    s_rooms.insert(id, returnValue);
    return returnValue;
}

GameElementSuspect *GameElementFactory::getSuspectById(int id)
{
    auto iter = s_suspects.find(id);
    if (iter != s_suspects.end())
    {
        return iter.value();
    }

    GameElementSuspect *returnValue = nullptr;
    if (id == static_cast<int>(SuspectsEnum::Green))
    {
        returnValue = new GameElementSuspect(id, "Green", "Mr Green", Coordinate(14, 0), Qt::GlobalColor::green, QPixmap(":/greenPortrait.png"));
    }
    else if (id == static_cast<int>(SuspectsEnum::Mustard))
    {
        returnValue = new GameElementSuspect(id, "Mustard", "Colonel Mustard", Coordinate(0, 17), QColor::fromRgb(255,165,0), QPixmap(":/mustardPortrait.png"));
    }
    else if (id == static_cast<int>(SuspectsEnum::Peacock))
    {
        returnValue = new GameElementSuspect(id, "Peacock", "Mrs Peacock", Coordinate(23, 6), Qt::GlobalColor::blue, QPixmap(":/peacockPortrait.png"));
    }
    else if (id == static_cast<int>(SuspectsEnum::Plum))
    {
        returnValue = new GameElementSuspect(id, "Plum", "Professor Plum", Coordinate(23, 19), QColor::fromRgb(238,130,238), QPixmap(":/plumPortrait.png"));
    }
    else if (id == static_cast<int>(SuspectsEnum::Scarlet))
    {
        returnValue = new GameElementSuspect(id, "Scarlet", "Mrs Scarlet", Coordinate(7, 24), Qt::GlobalColor::red, QPixmap(":/scarletPortrait.png"));
    }
    else if (id == static_cast<int>(SuspectsEnum::White))
    {
        returnValue = new GameElementSuspect(id, "White", "Mrs White", Coordinate(9, 0), Qt::GlobalColor::white, QPixmap(":/whitePortrait.png"));
    }

    s_suspects.insert(id, returnValue);
    return returnValue;
}

GameElementWeapon *GameElementFactory::getWeaponById(int id)
{
    auto iter = s_weapons.find(id);
    if (iter != s_weapons.end())
    {
        return iter.value();
    }

    GameElementWeapon *returnValue = nullptr;
    if (id == static_cast<int>(WeaponsEnum::Candlestick))
    {
        returnValue = new GameElementWeapon(id, "Candlestick", "Candlestick");
    }
    else if (id == static_cast<int>(WeaponsEnum::Knife))
    {
        returnValue = new GameElementWeapon(id, "Knife", "Knife");
    }
    else if (id == static_cast<int>(WeaponsEnum::LeadPipe))
    {
        returnValue = new GameElementWeapon(id, "LeadPipe", "Lead Pipe");
    }
    else if (id == static_cast<int>(WeaponsEnum::Poison))
    {
        returnValue = new GameElementWeapon(id, "Poison", "Poison");
    }
    else if (id == static_cast<int>(WeaponsEnum::Revolver))
    {
        returnValue = new GameElementWeapon(id, "Revolver", "Revolver");
    }
    else if (id == static_cast<int>(WeaponsEnum::Rope))
    {
        returnValue = new GameElementWeapon(id, "Rope", "Rope");
    }
    else if (id == static_cast<int>(WeaponsEnum::Wrench))
    {
        returnValue = new GameElementWeapon(id, "Wrench", "Wrench");
    }

    s_weapons.insert(id, returnValue);
    return returnValue;
}

GameElementRoom *GameElementFactory::getRoom(RoomsEnum room)
{
    return getRoomById(static_cast<int>(room));
}

GameElementSuspect *GameElementFactory::getSuspect(SuspectsEnum suspect)
{
    return getSuspectById(static_cast<int>(suspect));
}

GameElementWeapon *GameElementFactory::getWeapon(WeaponsEnum weapon)
{
    return getWeaponById(static_cast<int>(weapon));
}

QVector<GameElementRoom *> GameElementFactory::getRooms()
{

    QVector<GameElementRoom *> rooms;
    rooms.push_back(getRoom(RoomsEnum::Ballroom));
    rooms.push_back(getRoom(RoomsEnum::BilliardRoom));
    rooms.push_back(getRoom(RoomsEnum::Conservatory));
    rooms.push_back(getRoom(RoomsEnum::DiningRoom));
    rooms.push_back(getRoom(RoomsEnum::Hall));
    rooms.push_back(getRoom(RoomsEnum::Kitchen));
    rooms.push_back(getRoom(RoomsEnum::Library));
    rooms.push_back(getRoom(RoomsEnum::Lounge));
    rooms.push_back(getRoom(RoomsEnum::Study));
    return rooms;
}

QVector<GameElementSuspect *> GameElementFactory::getSuspects()
{
    QVector<GameElementSuspect *> suspects;
    suspects.push_back(getSuspect(SuspectsEnum::Green));
    suspects.push_back(getSuspect(SuspectsEnum::Mustard));
    suspects.push_back(getSuspect(SuspectsEnum::Peacock));
    suspects.push_back(getSuspect(SuspectsEnum::Plum));
    suspects.push_back(getSuspect(SuspectsEnum::Scarlet));
    suspects.push_back(getSuspect(SuspectsEnum::White));
    return suspects;
}

QVector<GameElementWeapon *> GameElementFactory::getWeapons()
{
    QVector<GameElementWeapon *> weapons;
    weapons.push_back(getWeapon(WeaponsEnum::Candlestick));
    weapons.push_back(getWeapon(WeaponsEnum::Knife));
    weapons.push_back(getWeapon(WeaponsEnum::LeadPipe));
    weapons.push_back(getWeapon(WeaponsEnum::Poison));
    weapons.push_back(getWeapon(WeaponsEnum::Revolver));
    weapons.push_back(getWeapon(WeaponsEnum::Rope));
    weapons.push_back(getWeapon(WeaponsEnum::Wrench));
    return weapons;
}

QVector<GameElement *> GameElementFactory::getAllGameElements()
{
    QVector<GameElement *> gameElements;
    for (GameElement *gameElement : getSuspects())
    {
        gameElements.push_back(gameElement);
    }
    for (GameElement *gameElement : getWeapons())
    {
        gameElements.push_back(gameElement);
    }
    for (GameElement *gameElement : getRooms())
    {
        gameElements.push_back(gameElement);
    }
    return gameElements;
}

GameElement *GameElementFactory::getGameElementById(int id)
{
    GameElement *gameElement = nullptr;
    gameElement = getRoomById(id);
    if (gameElement != nullptr)
    {
        return gameElement;
    }
    gameElement = getWeaponById(id);
    if (gameElement != nullptr)
    {
        return gameElement;
    }
    gameElement = getSuspectById(id);

    return gameElement;
}

GameElementFactory::GameElementFactory()
{

}

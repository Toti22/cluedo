#ifndef GAMEELEMENTONBOARD_H
#define GAMEELEMENTONBOARD_H

#include "core/coordinate.h"
#include "gameElement.h"


class GameElementOnBoard : public GameElement
{
    friend class GameElementFactory;
public:

    Coordinate getPosition() const;

protected:
    GameElementOnBoard(int id, const QString & name, const QString &visibleName, const Coordinate & position) : GameElement(id, name, visibleName), m_position(position) {}

    Coordinate m_position;
};

inline Coordinate GameElementOnBoard::getPosition() const
{
    return m_position;
}

#endif // GAMEELEMENTONBOARD_H

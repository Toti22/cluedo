#include "gameElementRoom.h"

bool GameElementRoom::isCoordSameAsADoorCoord(const Coordinate &coord)
{
    bool sameAsADoor = false;
    for (GameElementDoor &door : getDoors())
    {
        if (door.m_position == coord)
        {
            sameAsADoor = true;
        }
    }

    return sameAsADoor;
}

Coordinate GameElementRoom::getSpot(int characterId)
{
    int spotIndex = -1;
    for (int i = 0 ; i < m_spots.size() ; ++i)
    {
        if (m_spots[i].m_idCharacterInSpot == characterId)
        {
            spotIndex = i;
            break;
        }
        if (spotIndex == -1 && m_spots[i].m_idCharacterInSpot == -1)
        {
            spotIndex = i;
        }
    }
    if (spotIndex!= -1)
    {
        m_spots[spotIndex].m_idCharacterInSpot = characterId;
        return m_spots[spotIndex].m_position;
    }
    else
    {
        return m_position;
    }
}

void GameElementRoom::relaseSpot(int characterId)
{
    for (int i = 0 ; i < m_spots.size() ; ++i)
    {
        if (m_spots[i].m_idCharacterInSpot == characterId)
        {
            m_spots[i].m_idCharacterInSpot = -1;
            break;
        }
    }
}

GameElementRoom::GameElementRoom(int id, const QString &name, const QString &visibleName, const Coordinate &position, const QVector<GameElementSpotInside> &spots) :
    GameElementOnBoard(id, name, visibleName, position), m_spots(spots)
{
    for (GameElementSpotInside &spot : m_spots)
    {
        spot.m_idCharacterInSpot = -1;
    }
}

void GameElementRoom::addDoor(const GameElementDoor &door)
{
    m_doors.append(door);
}

void GameElementRoom::addDoors(const QVector<GameElementDoor> &doors)
{
    for (const GameElementDoor &door : doors)
    {
        addDoor(door);
    }
}

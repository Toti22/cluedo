#ifndef GAMEELEMENTFACTORY_H
#define GAMEELEMENTFACTORY_H

#include <QMap>

#include "gameElementRoom.h"
#include "gameElementSuspect.h"
#include "gameElementWeapon.h"
#include "gameElement.h"

class GameElementFactory
{
public:
    static GameElementRoom *getRoomById(int id);
    static GameElementSuspect *getSuspectById(int id);
    static GameElementWeapon *getWeaponById(int id);

    static GameElementRoom *getRoom(RoomsEnum room);
    static GameElementSuspect *getSuspect(SuspectsEnum suspect);
    static GameElementWeapon *getWeapon(WeaponsEnum weapon);

    static QVector<GameElementRoom *> getRooms();
    static QVector<GameElementSuspect *> getSuspects();
    static QVector<GameElementWeapon *> getWeapons();
    static QVector<GameElement *> getAllGameElements();
    static GameElement *getGameElementById(int id);

    static QMap<int, GameElementRoom*> s_rooms;
    static QMap<int, GameElementSuspect*> s_suspects;
    static QMap<int, GameElementWeapon*> s_weapons;
private:
    GameElementFactory();
};

#endif // GAMEELEMENTFACTORY_H

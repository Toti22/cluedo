#ifndef GAMEELEMENTSUSPECT_H
#define GAMEELEMENTSUSPECT_H

#include <QColor>
#include <QPixmap>

#include "core/coordinate.h"
#include "gameElementOnBoard.h"


class GameElementSuspect : public GameElementOnBoard
{
    friend class GameElementFactory;
public:
    QColor getColor() const;

    QPixmap getPortrait() const;

private:
    GameElementSuspect(int id, const QString &name, const QString & visibleName, const Coordinate &position, const QColor &color, const QPixmap &portrait);

    QColor m_color;
    QPixmap m_portrait;
};

inline QColor GameElementSuspect::getColor() const
{
    return m_color;
}

inline QPixmap GameElementSuspect::getPortrait() const
{
    return m_portrait;
}

#endif // GAMEELEMENTSUSPECT_H

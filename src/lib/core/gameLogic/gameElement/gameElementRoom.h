#ifndef GAMEELEMENTROOM_H
#define GAMEELEMENTROOM_H

#include <QVector>
#include <QMap>

#include "core/coordinate.h"
#include "core/pathFinder.h"
#include "gameElementOnBoard.h"

struct GameElementDoor
{
    Coordinate m_position = Coordinate(-1, -1); // Position inside room
    Direction m_directionToLeaveRoom;
};

struct GameElementSpotInside
{
    Coordinate m_position = Coordinate(-1, -1); // Position inside room
    int m_idCharacterInSpot;

    GameElementSpotInside(const Coordinate &position) : m_position(position), m_idCharacterInSpot(-1){}

};

class GameElementRoom : public GameElementOnBoard
{
    friend class GameElementFactory;
public:
    QVector<GameElementDoor> getDoors();
    bool isCoordSameAsADoorCoord(const Coordinate &coord);
    Coordinate getSpot(int characterId);
    void relaseSpot(int characterId);

protected:
    GameElementRoom(int id, const QString & name, const QString & visibleName, const Coordinate & position, const QVector<GameElementSpotInside> &spots);

private:
    void addDoor(const GameElementDoor &door);
    void addDoors(const QVector<GameElementDoor> &doors);

    QVector<GameElementDoor> m_doors;
    QVector<GameElementSpotInside> m_spots;
};

inline QVector<GameElementDoor> GameElementRoom::getDoors()
{
    return m_doors;
}

#endif // GAMEELEMENTROOM_H

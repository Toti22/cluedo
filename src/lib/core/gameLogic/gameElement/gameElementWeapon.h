#ifndef GAMEELEMENTWEAPON_H
#define GAMEELEMENTWEAPON_H

#include "gameElement.h"


class GameElementWeapon : public GameElement
{
    friend class GameElementFactory;
private:
    GameElementWeapon(int id, const QString &name, const QString &visibleName);
};

#endif // GAMEELEMENTWEAPON_H

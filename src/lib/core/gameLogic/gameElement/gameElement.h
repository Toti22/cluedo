#ifndef GAMEELEMENT_H
#define GAMEELEMENT_H

#include <QString>

enum class RoomsEnum { Hall = 0, Lounge = 1, DiningRoom = 2, Kitchen = 3, Ballroom = 4, Conservatory = 5, BilliardRoom = 6, Library = 7, Study = 8 };
enum class WeaponsEnum { Knife = 100, Candlestick = 101, Revolver = 102, Rope = 103, LeadPipe = 104, Wrench = 105, Poison = 106 };
enum class SuspectsEnum { Mustard = 200, Plum = 201, Green = 202, Peacock = 203, Scarlet = 204, White = 205 };

class GameElement
{
public:
    QString getName() const;

    int getId() const;

    QString getVisibleName() const;
    void setVisibleName(const QString &newVisibleName);

protected:
    GameElement(int id,const QString &m_name, const QString &visibleName) :
        m_id(id), m_name(m_name), m_visibleName(visibleName){}

    QString m_name;
    QString m_visibleName;
    int m_id;
};

inline QString GameElement::getName() const
{
    return m_name;
}

inline int GameElement::getId() const
{
    return m_id;
}

inline QString GameElement::getVisibleName() const
{
    return m_visibleName;
}

inline void GameElement::setVisibleName(const QString &newVisibleName)
{
    m_visibleName = newVisibleName;
}

#endif // GAMEELEMENT_H

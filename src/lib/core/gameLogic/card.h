#ifndef CARD_H
#define CARD_H

#include <QMap>
#include <QPixmap>

#include "gameElement/gameElement.h"

class Card
{
public:
    static Card *getCard(int elementIdCard);
    static Card *getRoomCard(RoomsEnum room);
    static Card *getWeaponCard(WeaponsEnum weapon);
    static Card *getSuspectCard(SuspectsEnum suspect);
    static QVector<Card *> getAllCards();
    static QVector<Card *>  getAllRoomCards();
    static QVector<Card *>  getAllWeaponCards();
    static QVector<Card *>  getAllSuspectCards();

    static bool  isRoomCard(Card *card);
    static bool  isWeaponCard(Card *card);
    static bool  isSuspectCard(Card *card);

    int getElementId() const;

    QPixmap getPixmap() const;

private:
    Card(int elementId);

    static QMap<int, Card*> s_allCard;
    static QMap<int, Card*> s_roomCard;
    static QMap<int, Card*> s_weaponCard;
    static QMap<int, Card*> s_suspectCard;

    int m_elementId;
    QPixmap m_pixmap;
};

inline int Card::getElementId() const
{
    return m_elementId;
}

inline QPixmap Card::getPixmap() const
{
    return m_pixmap;
}

#endif // CARD_H

include($$(CLUEDODIR)/src/lib/lib.pri)

QT += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += \
    board/board.cpp \
    board/floor.cpp \
    board/mapelement.cpp \
    board/rooms/ballRoom.cpp \
    board/rooms/billiardRoom.cpp \
    board/rooms/conservatory.cpp \
    board/rooms/diningRoom.cpp \
    board/rooms/hall.cpp \
    board/rooms/kitchen.cpp \
    board/rooms/library.cpp \
    board/rooms/lounge.cpp \
    board/rooms/room.cpp \
    board/rooms/study.cpp \
    board/tile.cpp \
    coordinate.cpp \
    gameLogic/trameHelper.cpp \
    players/aiPlayer.cpp \
    gameLogic/card.cpp \
    gameLogic/gameElement/gameElementFactory.cpp \
    gameLogic/gameElement/gameElementRoom.cpp \
    gameLogic/gameElement/gameElementSuspect.cpp \
    gameLogic/gameElement/gameElementWeapon.cpp \
    gameLogic/gamelogic.cpp \
    players/humanPlayer.cpp \
    players/player.cpp \
    pathFinder.cpp \
    players/remotePlayer.cpp

HEADERS += \
    board/boardCaracteristics.h \
    board/board.h \
    board/floor.h \
    board/mapelement.h \
    board/rooms/ballRoom.h \
    board/rooms/billiardRoom.h \
    board/rooms/conservatory.h \
    board/rooms/diningRoom.h \
    board/rooms/hall.h \
    board/rooms/kitchen.h \
    board/rooms/library.h \
    board/rooms/lounge.h \
    board/rooms/room.h \
    board/rooms/study.h \
    board/tile.h \
    coordinate.h \
    gameLogic/gameSettings.h \
    gameLogic/trameHelper.h \
    players/aiPlayer.h \
    gameLogic/card.h \
    gameLogic/gameElement/gameElement.h \
    gameLogic/gameElement/gameElementFactory.h \
    gameLogic/gameElement/gameElementOnBoard.h \
    gameLogic/gameElement/gameElementRoom.h \
    gameLogic/gameElement/gameElementSuspect.h \
    gameLogic/gameElement/gameElementWeapon.h \
    gameLogic/gamelogic.h \
    players/humanPlayer.h \
    players/player.h \
    players/clueSheetData.h \
    gameLogic/suggestion.h \
    pathFinder.h \
    players/remotePlayer.h

FORMS += \

INCLUDEPATH += $$(CLUEDODIR)/src/lib

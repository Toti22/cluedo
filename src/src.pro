TEMPLATE = subdirs

SUBDIRS += \
    lib \
    app

app.depends = lib

include($$(CLUEDODIR)/deploy.pri)

QMAKE_EXTRA_TARGETS += checks
checks.target = checks

#ifndef PATHFINDERTEST_H
#define PATHFINDERTEST_H

#include <QTest>

#include "core/pathFinder.h"
#include "core/board/board.h"

class PathFinderTest : public QObject
{
    Q_OBJECT
public:

private slots:
    void toDo();
};

void PathFinderTest::toDo()
{
}

QTEST_MAIN(PathFinderTest)
#include "pathFinderTest.moc"

#endif // PATHFINDERTEST_H

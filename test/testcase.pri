CONFIG += qt warn_on console testcase depend_includepath c++17
CONFIG -= app_bundle

DESTDIR = $$(CLUEDODIR)/bin

QT += testlib
QT += gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app

QMAKE_EXTRA_TARGETS += checks
checks.target = checks
checks.commands = cd /d \"$${DESTDIR}\" && call \"$${OUT_PWD}/target_wrapper.bat\" \".\\$${TARGET}.exe\" -xml > \"%CLUEDODIR%/test/results/qtestlib/$${TARGET}.xml\" $$escape_expand(\\n\\t)

QMAKE_EXTRA_TARGETS += deploy
deploy.target = deploy
deploy.commands = windeployqt $${DESTDIR}/$${TARGET}.exe $$escape_expand(\\n\\t)